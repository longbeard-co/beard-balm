<?php

/**
 * The template for displaying single Degree Program custom post type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while (have_posts()) :
      the_post();

      $name = get_field('display_name') ?: get_the_title();
      $img  = has_post_thumbnail() ? get_the_post_thumbnail(null, 'large') : wp_get_attachment_image(lb_get_person_placeholder_id(), 'large');

      $phone = get_field('contact_phone') ?: get_field('contact_phone', 'option');
      $email = get_field('contact_email') ?: get_field('contact_email', 'option');
      $address_title = get_field('contact_address_title', 'option');
      $address_link  = get_field('contact_address_link', 'option');
      $address_text  = get_field('contact_address_text', 'option');

    ?>

      <section class="container row page-m-t section-m-b--sm">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <div class="entry-breadcrumbs">
            <p class="breadcrumbs">
              <a href="<?php echo home_url('/faculty/'); ?>">Faculty & Staff</a> - <span><?php echo $name; ?></span>
            </p>
          </div>
        </div>
      </section>
      <section class="container row section-m-t--sm section-m-b--sm staff">
        <div class="staff__bg xs-hide md-show">
          <?php echo wp_get_attachment_image(84, 'large'); ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 col-lg-offset-1">
          <div class="profile">
            <div class="profile__title">
              <div class="profile__img">
                <?php echo $img; ?>
              </div>
              <h2 class="profile__name sm-hide"><?php echo $name; ?></h2>
            </div>
            <div class="profile__contact">
              <h2 class="h3">Contact</h2>
              <div class="contact-info-wrapper">
                <div class="contact__row">
                  <div class="contact__col">
                    <div class="contact-info">
                      <div class="contact-info__inner">
                        <?php the_svg('phone'); ?>
                        <a href="<?php echo phone_to_url($phone); ?>" target="_blank" rel="noopener noreferrer"><?php echo $phone; ?></a>
                      </div>
                    </div>
                    <div class="contact-info">
                      <div class="contact-info__inner">
                        <?php the_svg('email'); ?>
                        <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                      </div>
                    </div>
                  </div>
                  <div class="contact__col">
                    <div class="contact-info">
                      <div class="contact-info__inner">
                        <?php the_svg('location'); ?>
                        <div class="contact-info__text">
                          <h3 class="h5"><a href="<?php echo $address_link; ?>" target="_blank" rel="noopener noreferrer"><strong><?php echo $address_title; ?></strong></a></h3>
                          <p><?php echo $address_text; ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-md-7">
          <div class="bio">
            <h2 class="bio__name xs-hide sm-show"><?php echo $name; ?></h2>
            <div class="bio__text">
              <?php the_content(); ?>
            </div>
            <div class="bio__details">
              <?php
              // 1. Build available data into array
              $data = array();

              if (get_field('cv')) {
                $data['cv'] = array(
                  'title'   => 'Curriculum Vitae',
                  'content' => get_field('cv'),
                );
              }

              if (have_rows('published_works')) {
                $data['published_works'] = array(
                  'title'   => 'Published Works',
                  'content' => get_field('published_works')
                );
              }

              if (have_rows('video_lectures')) {
                $data['video_lectures'] = array(
                  'title'   => 'Video Lectures',
                  'content' => get_field('video_lectures')
                );
              }

              $tabs = '';
              $tab_panels = '';
              $i = 0;
              foreach ($data as $key => $value) {
                $title   = $value['title'];
                $content = $value['content'];

                $active = $i == 0 ? 'active' : '';
                $selected = $i == 0 ? 'true' : '';

                $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$title</span></a>";
                $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";

                if ($key == 'cv') {
                  // CV
                  $tab_panels .= "<div class='text-block'>$content</div>";
                } else if ($key == 'published_works') {
                  // Published Works
                  $post_id    = get_the_ID();
                  $tab_panels .= "<div id='published_works' data-post_id='$post_id' data-field='published_works' class='published-works'>";
                  $tab_panels .= lb_load_template_part('template-parts/published-works-list', '', array(
                    'paged'   => 1,
                    'field'   => 'published_works',
                  ));
                  $tab_panels .= "</div>"; #published_works

                } else if ($key == 'video_lectures') {
                  // Video Lectures
                  $ids = array_map(function ($video) {
                    return $video['youtube_video_id'];
                  }, $content);
                  // $posts = lb_get_youtube_video(implode(',', $ids));
                  $ids_string = implode(',', $ids);

                  $tab_panels .= '<div id="post-youtube-playlist" class="post-youtube-playlist" data-video-id="' . $ids_string . '">';
                  $tab_panels .= lb_load_template_part('template-parts/youtube-videos', '', array('video_id' => $ids_string));
                  $tab_panels .= '</div>';
                }

                $tab_panels .= "</div>";

                $i++;
              }
              wp_reset_postdata();
              ?>

              <div class="lb-tabs">
                <div class="tabs tabs--small" role="tablist"><?php echo $tabs; ?></div>
                <div class="tab-panels"><?php echo $tab_panels; ?></div>
              </div>
            </div>
          </div>
        </div>
      </section>



    <?php endwhile; // End of the loop.
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
