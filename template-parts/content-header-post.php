<?php

?>

<header class="entry-header">
  <div class="entry-breadcrumbs">
    <p class="breadcrumbs">
      <?php
      // $parent = has_category('research') ? 'Research' : 'Media';
      $parent = 'Media';
      $parent_link = $parent == 'Research' ? home_url('/research') : home_url('media');
      ?>
      <a href="<?php echo esc_url($parent_link); ?>"><?php echo $parent; ?></a> - <span><?php the_title(); ?></span>
    </p>
  </div>

  <?php
  the_title('<h2 class="entry-title">', '</h2>');
  ?>

  <div class="entry-meta">
    <div class="entry-meta__info">
      <ul>
        <li>
          <?php echo lb_get_the_date(); ?>
        </li>
        <li>
          <?php echo lb_get_the_category(); ?>
        </li>
      </ul>
    </div>
    <div class="entry-meta__social">
      <?php get_template_part('template-parts/social-share'); ?>
    </div>
  </div>


</header><!-- .entry-header -->