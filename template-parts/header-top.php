<div class="header__top">
  <div class="container">
    <div class="header__top__inner">
      <div class="header__top__left">
        <div class="header__logo">
          <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
            <?php
            $logo = get_field('header_logo', 'option');
            $size = 'full';
            if ($logo) {
              echo wp_get_attachment_image($logo, $size);
            } else {
              echo '<img src="' . get_the_theme_image_src('logo.png') . '" alt="Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies" width="460" height="82" />';
            }
            ?>
          </a>
        </div>
      </div>
      <div class="header__top__right">
        <div class="header__top-nav xs-hide col-md-show">
          <div class="header__top-nav__inner push-right">
            <nav id="secondary-navigation" class="header__top-nav__nav">
              <?php wp_nav_menu([
                'theme_location' => 'menu-2',
                'menu_id'        => 'secondary-menu',
                'menu_class'     => 'header__menu menu col-xs-hide col-md-show',
                'link_before'    => '<span>',
                'link_after'     => '</span>',
              ]); ?>
              <div id="site-search" class="header__search xs-hide md-show">
                <button id="site-search-trigger" class="header__search__button menu-item">
                  <?php the_svg('search', __('Search', 'beardbalm')); ?>
                </button>
                <div id="site-search-box" class="header__search-box">
                  <?php get_search_form(); ?>
                  <div id="site-search-results" class="header__search-box__results"></div>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <nav id="site-navigation" class="header__nav main-navigation end-xs middle-xs">
          <?php wp_nav_menu([
            'theme_location' => 'menu-1',
            'menu_id'        => 'primary-menu',
            'menu_class'     => 'header__menu menu col-xs-hide col-md-show',
            'link_before'    => '<span>',
            'link_after'     => '</span>',
          ]); ?>
          <div id="site-search" class="header__search md-hide">
            <button id="site-search-trigger" class="header__search__button menu-item">
              <?php the_svg('search', __('Search', 'beardbalm')); ?>
            </button>
            <div id="site-search-box" class="header__search-box">
              <?php get_search_form(); ?>
              <div id="site-search-results" class="header__search-box__results"></div>
            </div>
          </div>
          <button id="mobile-menu-toggle" class="menu-toggle col-xs-show col-md-hide" aria-controls="primary-menu" aria-expanded="false">
            <span class="screen-reader-text"><?php esc_html_e('Toggle Menu', 'beardbalm'); ?></span>
            <span class="menu-hamburger">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
            </span>
          </button>
        </nav>
      </div>
    </div>
  </div>
</div>