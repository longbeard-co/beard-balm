<?php
$paged       = $args && isset($args['paged']) ? $args['paged'] : (get_query_var('paged') ?: 1);
$category    = $args && isset($args['category']) && $args['category'] != 'all' ? $args['category'] : '';
$search      = $args && isset($args['search']) ? $args['search'] : '';
$is_deceased = $args && isset($args['is_deceased']) ? $args['is_deceased'] : false;

$posts_per_page = 10;

if ($category == 'academic-collaborators') {
  $args = array(
    'post_type'      => 'staff',
    'posts_per_page' => $posts_per_page,
    'paged'          => $paged,
    'order'          => 'ASC',
    'orderby'        => 'post_title',
    'tax_query'      => array(
      array(
        'taxonomy'   => 'position',
        'field'      => 'slug',
        'terms'      => $category,
      )
    ),
    'meta_key'       => 'is_deceased',
    'meta_value'     => true,
    'meta_compare'   => $is_deceased ? '=' : 'NOT EXISTS'
  );
} else {
  $args = array(
    'post_type'      => 'staff',
    'posts_per_page' => $posts_per_page,
    'paged'          => $paged,
    'order'          => 'ASC',
    'orderby'        => 'post_title',
    'tax_query'      => array(
      array(
        'taxonomy'   => 'position',
        'field'      => 'slug',
        'terms'      => $category,
      )
    ),
    // 'meta_key'       => 'is_deceased',
    // 'meta_value'     => array(true,false),
    // 'meta_compare'   => $is_deceased ? '=' : 'NOT EXISTS'
  );
}

if ($search) {
  $args['s'] = $search;
}

$the_query = new WP_Query($args);

if ($search) {
  $the_query->parse_query($args);
  relevanssi_do_query($the_query);
}

?>

<?php if ($the_query->have_posts()) { ?>
  <div class="posts-grid">
    <div class="posts-grid__inner">
      <?php
      while ($the_query->have_posts()) {
        $the_query->the_post();
        get_template_part('template-parts/post-entry', 'staff', array());
      }
      ?>
    </div>
  </div>
  <?php
  get_template_part('template-parts/pagination', '', array(
    'style'        => 'bottom',
    'show_results' => false,
    'query'        => $the_query,
    'base'         => home_url('/faculty'),
    'paged'        => $paged,
  ));
  ?>
<?php } else { ?>
  <div class="text-block narrow">
    <?php if ($search) { ?>
      <h6>Your search for <strong>"<?php echo $search; ?>"</strong> did not match any result under this category. Please check out other categories, or try adjusting your search. </h6>
      <p class="align-right"><a href="<?php echo home_url('/faculty'); ?>">&times; Clear Search</a></p>
    <?php } else { ?>
      <h6>No faculty / staff members found under this category.</h6>
    <?php } ?>
  </div>
<?php } ?>