<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package beardbalm
 */

$is_video = has_category('video') && get_field('video_youtube_id');

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php if (has_post_thumbnail() || $is_video) { ?>
    <header class="entry-header">
      <div class="entry-thumbnail">
        <div class="post-thumbnail">
          <?php if ($is_video) { ?>
            <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/<?php the_field('video_youtube_id'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          <?php } else { ?>
            <?php the_post_thumbnail('large'); ?>
          <?php } ?>
        </div>
      </div>

      <?php if ($is_video) { ?>
        <div class="youtube-view-more">
          <div class="youtube-view-more__inner">
            <span>View More on</span>
            <a href="https://www.youtube.com/sheptytskyinstitute" target="_blank" rel="noopener noreferrer">
              <img src="<?php the_theme_image_src('youtube.svg'); ?>" alt="YouTube" width="84" height="19" />
            </a>
            <a href="https://www.youtube.com/sheptytskyinstitute?sub_confirmation=1" target="_blank" rel="noopener noreferrer">
              <img src="<?php the_theme_image_src('youtube-subscribe.svg'); ?>" alt="Subscribe" width="73" height="23" />
            </a>
          </div>
        </div>
      <?php } ?>
    </header>
  <?php } ?>

  <div class="entry-content text-block">
    <?php
    the_content(
      sprintf(
        wp_kses(
          /* translators: %s: Name of current post. Only visible to screen readers */
          __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'beardbalm'),
          array(
            'span' => array(
              'class' => array(),
            ),
          )
        ),
        wp_kses_post(get_the_title())
      )
    );
    ?>
  </div><!-- .entry-content -->

  <?php
  $post_gallery = get_field('post_gallery');
  $post_videos = get_field('post_videos');
  $youtube_playlist_id = $post_videos && $post_videos['selection'] == 'playlist' && isset($post_videos['youtube_playlist_id']) ? $post_videos['youtube_playlist_id'] : null;
  $youtube_video_ids   = $post_videos && $post_videos['selection'] == 'videos' && isset($post_videos['youtube_video_ids']) ? $post_videos['youtube_video_ids'] : null;

  $post_media_html = '';

  if ($post_gallery || $youtube_playlist_id || $youtube_video_ids) {

    $post_gallery_html = '';
    $post_videos_html  = '';

    if ($post_gallery) {
      // This is so that JS can load in the template without passing the entire $post_gallery object
      $gallery_data = array(
        'post_id' => get_the_ID(),
        'field'   => 'post_gallery',
      );
      $gallery_data_json = json_encode($gallery_data);

      $post_gallery_html .= '<div id="post-gallery" class="post-gallery-wrapper">';
      $post_gallery_html .= lb_load_template_part('template-parts/gallery', '', array('gallery' => $post_gallery));
      $post_gallery_html .= '</div>'; // #post-gallery
      $post_gallery_html .= "<script>window._gallery = {'post-gallery': $gallery_data_json}</script>";
    }

    if ($youtube_playlist_id) {
      $post_videos_html .= '<div id="post-youtube-playlist" class="post-youtube-playlist" data-playlist-id="' . $youtube_playlist_id . '">';
      $post_videos_html .= lb_load_template_part('template-parts/youtube-videos', '', array('playlist_id' => $youtube_playlist_id));
      $post_videos_html .= '</div>';
    } else if ($youtube_video_ids) {
      $youtube_video_id = implode(',', array_map(function ($v) {
        return $v['video_id'];
      }, $youtube_video_ids));

      $post_videos_html .= '<div id="post-youtube-playlist" class="post-youtube-playlist" data-video-id="' . $youtube_video_id . '">';
      $post_videos_html .= lb_load_template_part('template-parts/youtube-videos', '', array('video_id' => $youtube_video_id));
      $post_videos_html .= '</div>';
    }

    if ($post_gallery_html && $post_videos_html) {
      // Make this into tabs
      $data = array(
        array(
          'title' => 'Videos',
          'content' => $post_videos_html,
        ),
        array(
          'title' => 'Photo Gallery',
          'content' => $post_gallery_html,
        ),
      );

      $tabs = '';
      $tab_panels = '';

      $i = 0;
      foreach ($data as $section) {
        $section_title    = $section['title'];
        $section_content  = $section['content'];

        $key           = sanitize_key($section_title);
        $active        = $i == 0 ? 'active' : '';
        $selected      = $i == 0 ? 'true' : '';

        $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$section_title</span></a>";
        $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>$section_content</div>";

        $i++;
      }

      $post_media_html .= '<div class="lb-tabs">';
      $post_media_html .= '<div class="tabs tabs--small" role="tablist">' . $tabs . '</div>';
      $post_media_html .= '<div class="tab-panels">' . $tab_panels . '</div>';
      $post_media_html .= '</div>'; // lb-tabs
    } else if ($post_gallery_html) {
      $post_media_html .= $post_gallery_html;
    } else if ($post_videos_html) {
      $post_media_html .= $post_videos_html;
    }

  ?>
    <div class="">
      <div class="post-media section-m-t--sm section-m-b--sm">
        <?php echo $post_media_html; ?>
      </div>
    </div>
  <?php } ?>

  <footer class="entry-footer">
    <?php get_template_part('template-parts/social-share'); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->