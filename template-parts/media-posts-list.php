<?php
$paged    = $args && isset($args['paged']) ? $args['paged'] : (get_query_var('paged') ?: 1);
$category = $args && isset($args['category']) ? $args['category'] : (isset($_GET['category']) && $_GET['category'] ? $_GET['category'] : '');
$search   = $args && isset($args['search']) ? $args['search'] : (isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '');
$year     = $args && isset($args['year']) ? $args['year'] : (get_query_var('year') ?: (isset($_GET['year']) && $_GET['year'] ? $_GET['year'] : ''));
$month    = $args && isset($args['month']) ? $args['month'] : (get_query_var('monthnum') ? get_query_var('monthnum') : (isset($_GET['month']) && $_GET['month'] ? $_GET['month'] : ''));
$post_type = 'post';

if (!$category) {
  $post_type = ['post'];
}

// Exception when $category passed is "events"
// This is actually post_type of tribe_events, not post category of "events"
// So we need to transform the variables here to avoid confusion
if ($category == 'events') {
  $category = '';
  $post_type = 'tribe_events';
}


// Query posts based on args

$posts_per_page = 9;

if ($post_type != 'tribe_events') {
  $query_args = array(
    'post_type'       => $post_type,
    'post_status'     => 'publish',
    'posts_per_page'  => $posts_per_page,
    'paged'           => $paged,
    'category_name'   => $category,
  );

  if ($year && $month) {
    $query_args['date_query'] = array(
      'year'  => $year,
      'month' => $month,
    );
  }

  if ($search) {
    $query_args['search'] = $search;
  }

  $the_query = new WP_Query($query_args);
} else if ($post_type == 'tribe_events') {
  $query_args = array(
    'post_status'    => 'publish',
    'posts_per_page' => $posts_per_page,
    'paged'          => $paged,
    'end_date'       => 'now',
  );

  if ($year && $month) {
    $query_args['date_query'] = array(
      'start_date'   => $year, '-' . $month . '-01 00:01',
      'end_date'     => $year, '-' . ($month + 1) . '-01 00:00',
    );
  }

  if ($search) {
    $query_args['s'] = $search;
  }

  $the_query = tribe_get_events($query_args, true);
}


if ($search) {
  $the_query->parse_query($query_args);
  relevanssi_do_query($the_query);
}

if ($the_query->have_posts()) {
  $i = 0;
  $featured_html   = '';
  $featured_2_html = '';
  $standard_html   = '';

  while ($the_query->have_posts()) {
    $the_query->the_post();
    $is_featured = $i < 3 && $paged == 1;

    if ($paged == 1 && $i == 0) {
      $featured_html .= lb_load_template_part('template-parts/post-entry', '', array(
        'show_thumbnail' => true,
        'show_meta'      => true,
        'show_excerpt'   => false,
        'show_icon'      => true,
      ));
    } else if ($paged == 1 && $i < 3) {
      $featured_2_html .= lb_load_template_part('template-parts/post-entry', '', array(
        'show_thumbnail' => true,
        'show_meta'      => true,
        'show_excerpt'   => false,
        'show_icon'      => true,
        'class'          => 'xs-hide md-show'
      ));
      $standard_html .= lb_load_template_part('template-parts/post-entry', '', array(
        'show_thumbnail' => false,
        'show_meta'      => true,
        'show_excerpt'   => false,
        'show_icon'      => true,
        'class'          => 'md-hide'
      ));
    } else {
      $standard_html .= lb_load_template_part('template-parts/post-entry', '', array(
        'show_thumbnail' => false,
        'show_meta'      => true,
        'show_excerpt'   => false,
        'show_icon'      => true,
      ));
    }

    $i++;
  }

?>

  <div class="posts-list-header">
    <div class="posts-list-header__search xs-hide md-show">
      <?php echo lb_get_search_form('media-posts-searchform', 'media-posts-searchform', '/media', 'Search Posts', 'search', $query_search); ?>
    </div>
    <div class="posts-list-header__pagination">
      <?php
      get_template_part('template-parts/pagination', '', array(
        'style' => 'top',
        'show_results' => true,
        'query' => $the_query,
        'base' => home_url('/media'),
        'paged' => $paged,
      ));
      ?>
    </div>
  </div>

  <div class="posts-list-wrapper">
    <?php if ($featured_html) { ?>
      <div class="posts-list posts-list--featured">
        <div class="posts-list--featured--1">
          <?php echo $featured_html; ?>
        </div>
        <div class="posts-list--featured--2">
          <?php echo $featured_2_html; ?>
        </div>
      </div>
    <?php } ?>
    <?php if ($standard_html) { ?>
      <div class="posts-list posts-list--standard">
        <?php echo $standard_html; ?>
      </div>
    <?php } ?>
  </div>

  <?php
  get_template_part('template-parts/pagination', '', array(
    'style' => 'bottom',
    'show_results' => true,
    'query' => $the_query,
    'base' => home_url('/media'),
    'paged' => $paged,
  ));
  ?>
<?php
} else {
  // No posts found
?>
  <div class="posts-list-wrapper">
    <h4>No posts are found.</h4>
  </div>
<?php
}
