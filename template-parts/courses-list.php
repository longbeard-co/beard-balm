<?php
$paged     = $args && isset($args['paged']) ? $args['paged'] : (get_query_var('paged') ?: 1);
$semester  = $args && isset($args['semester']) ? $args['semester'] : (isset($_GET['semester']) ? $_GET['semester'] : array());
$category  = $args && isset($args['category']) ? $args['category'] : (isset($_GET['category']) ? $_GET['category'] : array());
$search    = $args && isset($args['search']) ? $args['search'] : (isset($_GET['search']) ? $_GET['search'] : '');

$the_query = lb_query_courses(
  array(
    'semester' => $semester,
    'category' => $category,
    'paged'    => $paged,
    'search'   => $search,
    // 'posts_per_page' => 1,
  )
);
$courses       = $the_query['items'];
$has_next_page = $the_query['has_next_page'];
$has_prev_page = $the_query['has_prev_page'];

if (empty($courses)) {
?>
  <div class="courses__list">
    <?php if ($search || !empty($semester) || !empty($category)) { ?>
      <h5>No course can be found based on your search parameters. Please adjust your search.</h5>
    <?php } else { ?>
      <h5>No course can be found. Please check back again later.</h5>
    <?php } ?>
  </div>
<?php
  return;
}

?>
<div class="courses__list">
  <ul>
    <?php foreach ($courses as $course) {
      $title       = $course['title'];
      $semester    = $course['semester'];
      $year        = $course['year'];
      $description = $course['description'];
      $date        = $course['date'];
      $time        = $course['time'];
      $instructor  = $course['instructor'];
      $location    = $course['location'];
      $link        = $course['link'];
    ?>
      <li class="course">
        <div class="course__card card">
          <div class="course__header">
            <div class="course__title">
              <?php if ($link) { ?>
                <a href="<?php echo esc_url($link); ?>" class="button-icon" target="_blank" rel="noopener noreferrer">
                  <h2 class="h3"><?php echo $title; ?></h2>
                </a>
              <?php } else { ?>
                <h2 class="h3"><?php echo $title; ?></h2>
              <?php } ?>
            </div>
            <div class="course__semester">
              <h3 class="h5"><?php echo $semester['label'] . ' ' . $year; ?></h3>
            </div>
          </div>
          <div class="course__desc">
            <p><?php echo $description; ?></p>
            <ul class="course__desc__list">
              <?php if ($date) { ?>
                <li><strong>Date:</strong> <?php echo $date; ?></li>
              <?php } ?>
              <?php if ($time) { ?>
                <li><strong>Time:</strong> <?php echo $time; ?></li>
              <?php } ?>
              <?php if ($instructor) { ?>
                <li><strong>Instructor:</strong> <?php echo $instructor; ?></li>
              <?php } ?>
              <?php if ($location) { ?>
                <li><strong>Location:</strong> <?php echo $location; ?></li>
              <?php } ?>
            </ul>
            <?php if ($link) { ?>
              <p class="align-right">
                <a href="<?php echo esc_url($link); ?>" class="button-icon" target="_blank" rel="noopener noreferrer">Learn More<?php the_svg('external-link'); ?></a>
              </p>
            <?php } ?>
          </div>
        </div>
      </li>
    <?php } ?>
  </ul>
</div>

<?php if ($has_next_page || $has_prev_page) { ?>
  <div class="page-numbers-wrapper page-numbers-wrapper--bottom">
    <div class="page-numbers-wrapper__inner">
      <div class="page-numbers-list">
        <ul class="page-numbers">
          <li>
            <button class="prev page-numbers" <?php echo !$has_prev_page ? 'disabled' : ''; ?> <?php echo $has_prev_page ? ' data-page="' . strval($paged - 1) . '"' : ''; ?>>
              <?php the_svg('arrow-prev', 'Previous Page'); ?>
            </button>
          </li>
          <li>
            <button class="next page-numbers" <?php echo !$has_next_page ? 'disabled' : ''; ?> <?php echo $has_next_page ? ' data-page="' . strval($paged + 1) . '"' : ''; ?>>
              <?php the_svg('arrow-next', 'Next Page'); ?>
            </button>
          </li>
        </ul>
      </div>
    </div>
  </div>
<?php } ?>