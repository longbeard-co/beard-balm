<?php
$articles = $args && isset($args['articles']) ? $args['articles'] : array();
$per_page = $args && isset($args['per_page']) ? $args['per_page'] : 10;
$paged    = $args && isset($args['paged']) ? $args['paged'] : 1;
$search   = $args && isset($args['search']) ? $args['search'] : '';

if (empty($articles)) {
  return;
}

if ($search) {
  $articles = array_filter($articles, function ($article) use ($search) {
    $string = implode(' ', array_values($article));
    $searched = strpos(strtolower($string), strtolower($search)) !== false;
    return $searched;
  });
}

$paged_articles = array_slice($articles, ($paged - 1) * $per_page, $per_page);
$total_pages = ceil(count($articles) / $per_page);
$has_next_page = $total_pages > $paged;
$has_prev_page = $paged > 1;
?>

<div class="articles__wrapper">
  <?php if (empty($paged_articles) && $search) { ?>
    <h3 class="h6">No results found based on your search. Please try adjusting your search query.</h3>
  <?php } else { ?>
    <ul>
      <?php foreach ($paged_articles as $article) {
        $title  = $article['title'];
        $author = isset($article['author']) ? $article['author'] : '';
        $meta   = isset($article['meta']) ? $article['meta'] : array();
        $link   = isset($article['link']) ? $article['link'] : '';

        $meta_arr   = array();

        if (!empty($meta)) {
          foreach ($meta as $m) {
            array_push($meta_arr, $m);
          }
        }

        if ($author) {
          array_push($meta_arr, $author);
        }
      ?>
        <li>
          <article class="article">
            <h3 class="article__title h6">
              <?php if ($link) { ?>
                <a href="<?php echo $link; ?>">
                  <?php echo $title; ?>
                </a>
              <?php } else { ?>
                <?php echo $title; ?>
              <?php } ?>
            </h3>
            <div class="article__meta <?php echo $link ? 'article__meta--link' : ''; ?>">
              <div class="article__meta__list">
                <?php if (!empty($meta_arr)) { ?>
                  <ul>
                    <?php foreach ($meta_arr as $m) { ?>
                      <li><?php echo $m; ?></li>
                    <?php } ?>
                  </ul>
                <?php } ?>
              </div>
              <!-- <?php if ($link) { ?>
              <div class="article__meta__link">
                <a href="<?php echo $link; ?>" class="button-icon" target="_blank" rel="noopener noreferrer">PDF<?php the_svg('external-link'); ?></a>
              </div>
            <?php } ?> -->
            </div>
          </article>
        </li>
      <?php } ?>
    </ul>
  <?php } ?>
</div>

<?php if ($total_pages > 1) { ?>
  <div class="page-numbers-wrapper">
    <div class="page-numbers-wrapper__inner">
      <ul class="page-numbers">
        <?php if ($has_prev_page) { ?>
          <li>
            <button type="button" class="prev page-numbers" data-page="<?php echo $paged - 1; ?>"><?php the_svg('arrow-prev', 'Previous Page'); ?></button>
          </li>
        <?php } ?>
        <?php for ($i = 1; $i <= $total_pages; $i++) { ?>
          <li>
            <?php if ($i == $paged) { ?>
              <span aria-current="page" class="page-numbers current"><?php echo $i; ?></span>
            <?php } else { ?>
              <button type="button" class="page-numbers" data-page="<?php echo $i; ?>"><?php echo $i; ?></button>
            <?php } ?>
          </li>
        <?php } ?>
        <?php if ($has_next_page) { ?>
          <li>
            <button type="button" class="next page-numbers" data-page="<?php echo $paged + 1; ?>"><?php the_svg('arrow-next', 'Next Page'); ?></button>
          </li>
        <?php } ?>
      </ul>
    </div>
  </div>
<?php } ?>