<?php
$paged         = $args && isset($args['paged']) ? $args['paged'] : 1;
$per_page      = $args && isset($args['per_page']) ? $args['per_page'] : 4;
$field         = $args && isset($args['field']) ? $args['field'] : '';
$post_id       = $args && isset($args['post_id']) ? $args['post_id'] : null;

// $before_loop   = $args && isset($args['before_loop']) ? $args['before_loop'] : '';
// $after_loop    = $args && isset($args['after_loop']) ? $args['after_loop'] : '';

if (!$field) {
  return;
}

$all_data      = get_field($field, $post_id);

if (!$all_data) {
  return;
}

$paged_data    = array_slice($all_data, ($paged - 1) * $per_page, $per_page);

$has_next_page = $paged * $per_page < count($all_data);
$has_prev_page = $paged > 1;

?>

<div class="posts-grid">
  <div class="posts-grid__inner">
    <?php
    foreach ($paged_data as $data) {
      if ($data['is_bookstore_product']) {
        $post_id = $data['bookstore_product'];
        $post = get_post($post_id);
        setup_postdata($post);
        get_template_part('template-parts/post-entry', 'book', array(
          'show_thumbnail'  => true,
          'show_meta'       => true,
        ));
        wp_reset_postdata();
      } else {
        get_template_part('template-parts/post-entry', 'book', array(
          'show_thumbnail'  => true,
          'show_meta'       => true,
          'post' => array(
            'title'     => $data['title'],
            'thumbnail' => $data['image'] ? wp_get_attachment_image($data['image'], 'large') : null,
            'desc'      => array(
              $data['meta'],
              $data['date']
            ),
            'link'      => array(
              'url'     => $data['link'] ?: null,
              'target'  => '_blank'
            )
          )
        ));
      }
    }
    ?>
  </div>
</div>

<?php if ($has_next_page || $has_prev_page) { ?>
  <div class="page-numbers-wrapper page-numbers-wrapper--bottom">
    <div class="page-numbers-wrapper__inner">
      <div class="page-numbers-list">
        <ul class="page-numbers">
          <li>
            <button class="prev page-numbers" <?php echo !$has_prev_page ? 'disabled' : ''; ?> <?php echo $has_prev_page ? ' data-page="' . strval($paged - 1) . '"' : ''; ?>>
              <?php the_svg('arrow-prev', 'Previous Page'); ?>
            </button>
          </li>
          <li>
            <button class="next page-numbers" <?php echo !$has_next_page ? 'disabled' : ''; ?> <?php echo $has_next_page ? ' data-page="' . strval($paged + 1) . '"' : ''; ?>>
              <?php the_svg('arrow-next', 'Next Page'); ?>
            </button>
          </li>
        </ul>
      </div>
    </div>
  </div>
<?php } ?>