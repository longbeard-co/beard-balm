<?php

$search = $args && isset($args['search']) ? $args['search'] : '';
$char = $args && isset($args['char']) ? $args['char'] : ($search ? '' : 'A');

$args = array(
  'taxonomy' => 'product_author',
  'hide_empty' => false,
);

if ($char) {
  $args['meta_query'] = array(
    array(
      'key'     => 'last_name',
      'value'   => '^' . $char . '.*',
      'compare' => 'REGEXP'
    )
  );
}

if ($search) {
  $args['search'] = $search;
}

$terms = get_terms($args);

if ($terms && !is_wp_error($terms)) {
  // $grouped_terms = array();

  // if (!$search) {
  //   $grouped_terms = array(
  //     'char' => $char,
  //     'terms' => $terms,
  //   );
  // } else {

  // }

?>
  <div class="bookstore-authors">
    <ul class="bookstore-authors__chars">
      <li>
        <h3 class="bookstore-authors__chars__title"><?php echo $char; ?></h3>
        <ul class="bookstore-authors__list">
          <?php foreach ($terms as $term) {
            $name = '';
            $last_name = get_field('last_name', 'product_author_' . $term->term_id);
            $first_name = get_field('first_name', 'product_author_' . $term->term_id);
            if ($first_name && $last_name) {
              $name = $last_name . ', ' . $first_name;
            } else if ($first_name || $last_name) {
              $name = $last_name ?: $first_name;
            } else {
              $name = $term->name;
            }
          ?>
            <li>
              <a href="<?php echo get_term_link($term, 'product_author'); ?>"><?php echo $name; ?></a>
            </li>
          <?php } ?>
        </ul>
      </li>
    </ul>
  </div>
<?php } else { ?>
  <div class="bookstore-authors">
    <h3 class="h5">No authors found. <?php echo $search ? 'Please adjust your search' : ''; ?></h3>
  </div>
<?php } ?>