<?php

$style  = $args && isset($args['style']) ? $args['style'] : 'bottom'; // e.g. top
$show_results = $args && isset($args['show_results']) ? $args['show_results'] : false;
$query  = $args && isset($args['query']) ? $args['query'] : null;
$base   = $args && isset($args['base']) ? $args['base'] : null;
$paged   = $args && isset($args['paged']) ? $args['paged'] : (get_query_var('paged') ?: 1);

// if (!$query) {
//   global $wp_query;
//   $query = $wp_query;
// }

if (!$query) {
  return;
}

$max_num_pages = $query->max_num_pages;

if ($max_num_pages > 1) :
  $big = 999999999; // need an unlikely integer

  $nav_args = [
    'base'       => $base ? $base . '/page/%#%/' : str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format'     => '?paged=%#%',
    'current'   => max(1, $paged),
    'total'     => $max_num_pages,
    'prev_text' => get_the_svg('arrow-prev', 'Previous Page'),
    'next_text' => get_the_svg('arrow-next', 'Next Page'),
    'type'       => 'list',
  ];

  $nav = '<div class="page-numbers-list">' . paginate_links($nav_args) . '</div>';
  $results = '';

  if ($show_results) {
    $from = (($paged - 1) * $query->query_vars['posts_per_page']) + 1;
    $to = $from + $query->post_count - 1;
    $results = '<div class="page-numbers-results"><p>Showing ' . $from . ' - ' . $to . ' of ' . $query->found_posts . ' results</p></div>';
  }

  echo '<div class="page-numbers-wrapper page-numbers-wrapper--' . $style . '"><div class="page-numbers-wrapper__inner">' . $results . $nav . '</div></div>';
endif;
