<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package beardbalm
 */

$is_video = has_category('video') && get_field('video_youtube_id');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header--top">
		<div class="entry-breadcrumbs">
			<p class="breadcrumbs">
				<a href="/media">Media</a> - <span><?php the_title(); ?></span>
			</p>
		</div>

		<?php
		the_title('<h2 class="entry-title">', '</h2>');
		?>

		<div class="entry-meta">
			<div class="entry-meta__info">
				<ul>
					<li>
						By <?php echo get_the_author(); ?>
					</li>
					<li>
						<?php echo get_the_date(); ?>
					</li>
					<li>
						<?php echo the_category(', '); ?>
					</li>
				</ul>
			</div>
			<div class="entry-meta__social">
				<?php get_template_part('template-parts/social-share'); ?>
			</div>
		</div>

		<?php if (has_post_thumbnail() || $is_video) { ?>
			<div class="entry-thumbnail">
				<div class="post-thumbnail">
					<?php if ($is_video) { ?>
						<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/<?php the_field('video_youtube_id'); ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<?php } else { ?>
						<?php the_post_thumbnail('large'); ?>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

		<?php if ($is_video) { ?>
			<div class="youtube-view-more">
				<div class="youtube-view-more__inner">
					<span>View More on</span>
					<a href="https://www.youtube.com/sheptytskyinstitute" target="_blank" rel="noopener noreferrer">
						<img src="<?php the_theme_image_src('youtube.svg'); ?>" alt="YouTube" width="84" height="19" />
					</a>
					<a href="https://www.youtube.com/sheptytskyinstitute?sub_confirmation=1" target="_blank" rel="noopener noreferrer">
						<img src="<?php the_theme_image_src('youtube-subscribe.svg'); ?>" alt="Subscribe" width="73" height="23" />
					</a>
				</div>
			</div>
		<?php } ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'beardbalm'),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post(get_the_title())
			)
		);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php get_template_part('template-parts/social-share'); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->