<?php
$category = $args && isset($args['category']) ? $args['category'] : '';
?>

<?php
$featured = '';
$standard = '';
$args = array(
  'post_type'           => 'product',
  'post_status'         => 'publish',
  'posts_per_page'      => 4,
  'no_found_rows'       => true,
  'ignore_sticky_posts' => true,
  'product_cat'         => $category,
  'orderby'             => 'date',
  'order'               => 'DESC',
);
$the_query = new WP_Query($args);
$the_posts = $the_query->get_posts();
$featured_posts = array();
$standard_posts = array();
if (!empty($the_posts)) {
  $featured_posts = array($the_posts[0]);
  $standard_posts = array_slice($the_posts, 1);
}

wp_reset_postdata();
?>
<div class="featured-products__wrapper">
  <div class="featured-products__featured">
    <div class="featured-products__featured__box card">
      <?php
      foreach ($featured_posts as $pid) {
        $post = get_post($pid);
        setup_postdata($post);
        get_template_part('template-parts/post-entry', 'book', array(
          'show_thumbnail' => true,
          'show_meta'       => true,
        ));
        wp_reset_postdata();
      }
      ?>
    </div>
  </div>
  <div class="featured-products__standard posts-grid">
    <div class="posts-grid__inner">
      <?php
      foreach ($standard_posts as $pid) {
        $post = get_post($pid);
        setup_postdata($post);
        get_template_part('template-parts/post-entry', 'book', array(
          'show_thumbnail' => false,
          'show_meta'       => false,
        ));
        wp_reset_postdata();
      }
      ?>
    </div>
  </div>
</div>