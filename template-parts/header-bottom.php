<nav id="mobile-navigation" class="header__nav-mobile md-hide">
  <div class="header__nav-mobile__inner  container">
    <?php
      wp_nav_menu([
        'theme_location' => 'menu-mobile',
        'menu_id'        => 'primary-menu-mobile',
        'menu_class'     => 'header__nav-mobile__menu menu',
        'link_before'    => '<span>',
        'link_after'     => '</span>',
      ]);
    // wp_nav_menu([
    //   'theme_location' => 'menu-1',
    //   'menu_id'        => 'primary-menu-mobile',
    //   'menu_class'     => 'header__nav-mobile__menu menu',
    //   'link_before'    => '<span>',
    //   'link_after'     => '</span>',
    // ]);
    // wp_nav_menu([
    //   'theme_location' => 'menu-2',
    //   'menu_id'        => 'secondary-menu-mobile',
    //   'menu_class'     => 'header__nav-mobile__menu menu',
    //   'link_before'    => '<span>',
    //   'link_after'     => '</span>',
    // ]);
    ?>
    <div class="header__social--mobile">
      <?php get_template_part('template-parts/social-media'); ?>
    </div>
  </div>
</nav>