<?php

$show_thumbnail = $args && isset($args['show_thumbnail']) ? $args['show_thumbnail'] : false;
$show_meta      = $args && isset($args['show_meta']) ? $args['show_meta'] : false;
$show_excerpt   = $args && isset($args['show_excerpt']) ? $args['show_excerpt'] : false;
$show_icon      = $args && isset($args['show_icon']) ? $args['show_icon'] : false;
$class          = $args && isset($args['class']) ? $args['class'] : '';

$custom_post    = $args && isset($args['post']) ? $args['post'] : '';

if ($custom_post) {
  // Useful for custom / external data, e.g. YouTube videos
  $title       = isset($custom_post['title']) ? $custom_post['title'] : null;
  $excerpt     = isset($custom_post['excerpt']) ? $custom_post['excerpt'] : null;
  $link        = isset($custom_post['link']) ? $custom_post['link'] : null;
  $link_url    = isset($link['url']) ? $link['url'] : '/';
  $link_target = isset($link['target']) ? $link['target'] : '_self';
  $thumbnail   = isset($custom_post['thumbnail']) ? $custom_post['thumbnail'] : null;
  $is_video    = false !== strpos($link_url, 'youtube');
  $is_popup    = isset($custom_post['is_popup']) ? $custom_post['is_popup'] : false;
  $meta        = isset($custom_post['meta']) ? $custom_post['meta'] : array();
  $categories  = array();
} else {
  // Inside loop
  $title       = get_the_title();
  $excerpt     = get_the_excerpt();
  $link_url    = get_permalink();
  $link_target = '_self';

  $thumbnail   = get_the_post_thumbnail(null, 'large');

  $youtube_id  = get_field('video') && isset(get_field('video')['youtube_id']) ? get_field('video')['youtube_id'] : null;
  $is_video    = $youtube_id ? true : false;
  $is_popup    = $is_video && !get_the_content() ? true : false;

  if (!$thumbnail && $youtube_id) {
    $thumbnail = '<img src="https://img.youtube.com/vi/' . $youtube_id . '/0.jpg" alt="' . $title . '" width="480" height="360" />';
  }



  $categories = get_the_category();
  $meta = array();

  if (get_post_type() == 'tribe_events') {
    $event_time = lb_get_tribe_events_datetime();
    $meta[] = $event_time;
  } else {
    // Author
    $author = get_field('author_name');
    if ($author) {
      $meta[] = $author;
    }
    // Date
    $date = lb_get_the_date();
    if ($date) {
      $meta[] = $date;
    }
    // Categories
    if ($categories) {
      $primary_category = yoast_get_primary_term();
      $meta[] = $primary_category ? $primary_category : $categories[0]->name;
    }
  }
}



$icon = '';

if ($show_icon) {
  if ($is_video) {
    $icon .= '<div class="post-entry__icon">';
    $icon .= lb_get_post_category_icon('video');
    $icon .= '</div>';
  } else if ($categories) {
    $icon .= '<div class="post-entry__icon">';
    $icon .= lb_get_post_category_icon($categories[0]->slug);
    $icon .= '</div>';
  }
}

?>

<article class="post-entry <?php echo $class; ?> <?php echo !$show_thumbnail && $show_icon ? 'post-entry--icon' : ''; ?>">
  <div class="post-entry__inner">
    <?php if ($show_thumbnail) { ?>
      <div class="post-entry__img">
        <?php
        echo '<div class="post-thumbnail">';
        echo $thumbnail ?: wp_get_attachment_image(379, 'large');
        echo '</div>';
        ?>
        <?php if ($show_icon) {
          echo $icon;
        } ?>
      </div>
    <?php } ?>
    <?php if (!$show_thumbnail && $show_icon) {
      echo $icon;
    } ?>
    <div class="post-entry__details">
      <div class="post-entry__text">
        <a href="<?php echo esc_url($link_url); ?>" class="post-entry__link-overlay" target="<?php echo $link_target; ?>" rel="<?php echo $link_target == '_blank' ? 'noopener noreferrer' : ''; ?>" <?php echo $is_popup ? 'data-lity' : ''; ?>>
          <h3 class="post-entry__title h6"><?php echo $title; ?></h3>
        </a>
        <?php if ($show_excerpt) { ?>
          <div class="post-entry__excerpt"><?php echo $excerpt; ?></div>
        <?php } ?>
        <?php if ($show_meta) { ?>
          <div class="post-entry__meta small"><?php echo implode('<span class="separator">&nbsp;|&nbsp;</span>', $meta); ?></div>
        <?php } ?>
      </div>
    </div>
  </div>
</article>