<?php
$per_page = 9;

$gallery       = $args && isset($args['gallery']) ? $args['gallery'] : '';
$gallery_data  = $args && isset($args['gallery_data']) ? $args['gallery_data'] : '';

if (!$gallery) {
  $acf_field = isset($gallery_data['field']) ? $gallery_data['field'] : '';
  $post_id   = isset($gallery_data['post_id']) ? $gallery_data['post_id'] : '';
  if ($acf_field && $post_id) {
    $gallery = get_field($acf_field, $post_id);
  }
}

if (!$gallery) {
  return;
}

$paged    = $args && isset($args['paged']) ? $args['paged'] : 1;

$paged_start   = ($paged - 1) * $per_page;
$paged_gallery = array_slice($gallery, ($paged - 1) * $per_page, $per_page);

$has_next_page = $paged * $per_page < count($gallery);
$has_prev_page = $paged > 1;
?>

<div class="post-gallery lightgallery">
  <ul>
    <?php
    $i = 0;
    foreach ($gallery as $img) {
      $full   = $img['url'];
      $thumb  = $img['sizes']['medium'];
      $width  = $img['width'];
      $height = $img['height'];
      $alt    = $img['alt'];
      $style  = $i >= $paged_start && $i < $paged_start + $per_page ? '' : 'display: none';

    ?>
      <li style="<?php echo $style; ?>">
        <a href="<?php echo $img['url']; ?>" class="post-gallery__item" data-lg-size="<?php echo $width; ?>-<?php echo $height; ?>">
          <img alt="<?php echo $alt; ?>" src="<?php echo $thumb; ?>" />
        </a>
      </li>
    <?php
      $i++;
    }
    ?>
  </ul>
</div>

<?php if ($has_next_page || $has_prev_page) { ?>
  <div class="page-numbers-wrapper page-numbers-wrapper--bottom">
    <div class="page-numbers-wrapper__inner">
      <div class="page-numbers-list">
        <ul class="page-numbers">
          <li>
            <button class="prev page-numbers" <?php echo !$has_prev_page ? 'disabled' : ''; ?> <?php echo $has_prev_page ? ' data-page="' . strval($paged - 1) . '"' : ''; ?>>
              <?php the_svg('arrow-prev', 'Previous Page'); ?>
            </button>
          </li>
          <li>
            <button class="next page-numbers" <?php echo !$has_next_page ? 'disabled' : ''; ?> <?php echo $has_next_page ? ' data-page="' . strval($paged + 1) . '"' : ''; ?>>
              <?php the_svg('arrow-next', 'Next Page'); ?>
            </button>
          </li>
        </ul>
      </div>
    </div>
  </div>
<?php } ?>