<?php
$paged     = $args && isset($args['paged']) ? $args['paged'] : (get_query_var('paged') ?: 1);
$post_type = $args && isset($args['post_type']) ? $args['post_type'] : 'post';
$category  = $args && isset($args['category']) ? $args['category'] : '';
$search    = $args && isset($args['search']) ? $args['search'] : '';

$posts_per_page = 6;

$args = array(
  'post_type'      => $post_type,
  'posts_per_page' => $posts_per_page,
  'paged'          => $paged,

);

if ($search) {
  $args['s'] = $search;
}

if ($post_type == 'tribe_events') {
  $args['start_date'] = 'now';
  $args['tax_query']  = array(
    array(
      'taxonomy' => 'tribe_events_cat',
      'field' => 'slug',
      'terms' => $category
    )
  );
  $the_query = tribe_get_events($args, true);
} else {
  $args['category_name']  = $category;
  $the_query = new WP_Query($args);
}

if ($search) {
  $the_query->parse_query($args);
  relevanssi_do_query($the_query);
}

?>

<?php if ($the_query->have_posts()) { ?>
  <div class="posts-grid">
    <div class="posts-grid__inner">
      <?php
      while ($the_query->have_posts()) {
        $the_query->the_post();
        get_template_part('template-parts/post-entry', '', array(
          'class'          => 'post-entry--list',
          'show_thumbnail' => true,
          'show_meta'      => true,
          'show_excerpt'   => false,
          'show_icon'      => true,
        ));
      }
      ?>
    </div>
  </div>
  <?php
  get_template_part('template-parts/pagination', '', array(
    'style'        => 'bottom',
    'show_results' => false,
    'query'        => $the_query,
    'base'         => home_url('/research'),
    'paged'        => $paged,
  ));
  ?>
<?php } else { ?>
  <div class="text-block narrow">
    <?php if ($search) { ?>
      <h6>Your search for <strong>"<?php echo $search; ?>"</strong> did not match any result under this category. Please check out other categories, or try adjusting your search. </h6>
      <p class="align-right"><a href="<?php echo home_url('/research'); ?>">&times; Clear Search</a></p>
    <?php } else { ?>
      <h6>No posts found under this category. Please check back again later.</h6>
    <?php } ?>
  </div>
<?php } ?>