<?php
$person = $args && isset($args['person']) ? $args['person'] : null;

if (!$person) {
  return null;
}

$name  = $person['name'];
$img   = $person['img'];
$role  = isset($person['role']) ? $person['role'] : '';
$role2 = isset($person['role2']) ? $person['role2'] : '';
$phone = isset($person['phone']) ? $person['phone'] : '';
$email = isset($person['email']) ? $person['email'] : '';

$contact_info = '';
if ($email || $phone) {
  $contact_info .= '<div class="contact-info-wrapper">';
  if ($email) {
    $contact_info .= "<div class='contact-info'>";
    $contact_info .= "<div class='contact-info__inner'>";
    $contact_info .= get_the_svg('email');
    $contact_info .= "<a href='mailto:$email' target='_blank' rel='noopener noreferrer'>$email</a>";
    $contact_info .= "</div>";
    $contact_info .= "</div>";
  }
  if ($phone) {
    $phone_url = phone_to_url($phone);
    $contact_info .= "<div class='contact-info'>";
    $contact_info .= "<div class='contact-info__inner'>";
    $contact_info .= get_the_svg('phone');
    $contact_info .= "<a href='$phone_url' target='_blank' rel='noopener noreferrer'>$phone</a>";
    $contact_info .= "</div>";
    $contact_info .= "</div>";
  }
  $contact_info .= "</div>";
}
?>

<div class='contact-person'>
  <div class='contact-person__img'>
    <?php echo wp_get_attachment_image($img, 'large'); ?>
  </div>
  <div class='contact-person__details'>
    <div class="contact-person__details__body">
      <h3 class='h6 contact-person__name'><?php echo $name; ?></h3>
      <?php if ($role) { ?>
        <h4 class='small contact-person__role'><?php echo $role; ?></h4>
      <?php } ?>
      <?php if ($role2) { ?>
        <h4 class='small contact-person__role contact-person__role--2'><?php echo $role2; ?></h4>
      <?php } ?>
    </div>
    <div class='contact-person__contact xs-hide sm-show'>
      <?php echo $contact_info; ?>
    </div>
  </div>
  <div class='contact-person__contact sm-hide'>
    <?php echo $contact_info; ?>
  </div>
</div>