<?php
$posts = $args && isset($args['posts']) ? $args['posts'] : []; // Array of post ids
$link  = $args && isset($args['link']) ? $args['link'] : null; // Read more link
$post_type  = $args && isset($args['post_type']) ? $args['post_type'] : null; // Post Type, mostly for the "nothing found" message

$featured_posts = array();
$standard_posts = array();

if (empty($posts)) {
  $unit = $post_type == 'tribe_events' ? 'upcoming event scheduled' : 'news found';
?>
  <h5>There is currently no <?php echo $unit; ?>. Please check again later.</h5>
<?php
  return;
}

if (!empty($posts)) {
  $featured_posts = array($posts[0]);
  $standard_posts = array_slice($posts, 1);
}

?>

<div class="featured-posts__wrapper">
  <div class="featured-posts__featured posts-grid">
    <div class="posts-grid__inner">
      <?php
      foreach ($featured_posts as $pid) {
        $post = get_post($pid);
        setup_postdata($post);
        get_template_part('template-parts/post-entry', '', array(
          'show_thumbnail' => true,
          'show_meta'       => true,
          'show_excerpt'   => true,
        ));
        wp_reset_postdata();
      }
      ?>
    </div>
  </div>
  <div class="featured-posts__standard posts-grid">
    <div class="posts-grid__inner">
      <?php
      foreach ($standard_posts as $pid) {
        $post = get_post($pid);
        setup_postdata($post);
        get_template_part('template-parts/post-entry', '', array(
          'show_thumbnail' => false,
          'show_meta'       => true,
          'show_excerpt'   => false,
        ));
        wp_reset_postdata();
      }
      ?>
    </div>
  </div>
</div>

<?php if ($link) { ?>
  <div class="end-xs">
    <a href="<?php echo esc_url($link); ?>" class="button m-t"><span></span>View More</a>
  </div>
<?php } ?>