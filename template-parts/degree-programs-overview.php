<?php
$img = $args && isset($args['img']) ? $args['img'] : 83;
$subtitle = $args && isset($args['subtitle']) ? $args['subtitle'] : null;

$programs = array(
  array(
    'title' => 'Conjoint Certificate in Eastern Christian Studies',
    'body'  => '<p>The Faculty of Theology at the University of St Michael’s College (USMC) offers a conjoint certificate in Eastern Christian Studies.</p>
										<p>This program is open to all basic degree students at any TST college; the program is a separate certificate granted by the USMC in addition to one’s MDiv/MTS program.</p><p class="align-right"><a class="button" href="/academics/degree-programs/conjoint-certificate-in-eastern-christian-studies"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Master of Theological Studies Degree (MTS)',
    'body'  => '<p>This program is designed to provide the student with a general theological understanding as well as the opportunity for in-depth study.</p><p>A typical program length of 2 years full-time study consists of twenty units of study. The program can also be completed on a part-time basis.</p><p class="align-right"><a class="button" href="/academics/degree-programs/master-of-theological-studies-degree-mts/"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Master of Divinity (MDiv)',
    'body'  => '<p>This professional program is designed for the theological education of men and women for leadership in the Church in both lay and ordained ministerial roles.</p><p>The typical program length is three years and consists of a thirty (30) credit program. The program can be completed part-time.</p><p class="align-right"><a class="button" href="/academics/degree-programs/master-of-divinity-mdiv"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Master of Arts in Theological Studies (MA)',
    'body'  => '<p>The MA in Theology prepares students for doctoral work. The program can only be completed as a full-time student, with a normal program length of 1 year.</p><p class="align-right"><a class="button" href="/academics/degree-programs/master-of-arts-in-theological-studies-ma/"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Master of Theology (ThM)',
    'body'  => '<p>The ThM is designed to deepen a student’s knowledge of theology and further preparation for pastoral ministry or doctoral study. There is no requirement for full-time residence.</p><p class="align-right"><a class="button" href="/academics/degree-programs/master-of-theology-thm/"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Doctor of Philosophy in Theological Studies (PhD)',
    'body'  => '<p>The PhD program will enable students to make an original contribution to scholarship in a chosen area of specialization as a full-time student, with a normal program length of 4 years.</p><p class="align-right"><a class="button" href="/academics/degree-programs/doctor-of-philosophy-in-theological-studies-phd/"><span></span>Learn More</a></p>',
  ),
  array(
    'title' => 'Doctor of Ministry (DMin)',
    'body'  => '<p>The DMin program is designed to develop excellence in the practice of ministry through a creative coordination of theological reflection, professional development, social analysis, and personal integration.</p><p class="align-right"><a class="button" href="/academics/degree-programs/doctor-of-ministry-dmin/"><span></span>Learn More</a></p>',
  ),
);
?>

<section class="container row section-m-t section-m-b degree-programs-overview">
  <?php if ($img) { ?>
    <div class="degree-programs-overview__bg xs-hide sm-show">
      <?php echo wp_get_attachment_image($img, 'large'); ?>
    </div>
  <?php } ?>
  <div class="col-xs-12 col-md-10 col-lg-9 col-lg-offset-1">
    <h2 class="h1">Degree Programs</h2>
    <?php if ($subtitle) { ?>
      <div class="degree-programs-overview__subtitle narrow">
        <?php echo $subtitle; ?>
      </div>
    <?php } ?>
    <?php get_template_part('template-parts/vertical-tabs', '', array('data' => $programs)); ?>
  </div>
  </div>
</section>