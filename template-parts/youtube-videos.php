<?php
$playlist_id  = $args && isset($args['playlist_id']) ? $args['playlist_id'] : '';
$video_id     = $args && isset($args['video_id']) ? $args['video_id'] : '';
$page_token   = $args && isset($args['page_token']) ? $args['page_token'] : '';
$per_page     = 4;

// Youtube Data API v3 - https://console.cloud.google.com/
$API_KEY   = YOUTUBE_API_KEY;


if ($video_id) {
  // https://developers.google.com/youtube/v3/docs/videos/list
  // Note that maxResults param is not supported when used in conjunction with id parameter
  // And thus, page_token will not be available, so we're going to fake page_token system
  $page_number = $page_token ? intval($page_token) : 1;
  $page_token  = null; // So that this is not included on the URL query string

  // Parse video_id into an array, paginate it, and switch back to string
  $video_id_array = explode(',', $video_id);
  $total_pages    = ceil(count($video_id_array) / $per_page);
  $page_offset    = ($page_number - 1) * $per_page;
  $video_id       = implode(',', array_slice($video_id_array, $page_offset, $per_page));
}

if ($playlist_id) {
  $url     = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=$per_page&playlistId=$playlist_id&key=$API_KEY";
} else if ($video_id) {
  $url     = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=$video_id&key=$API_KEY";
} else {
  return;
}

if ($page_token) {
  $url .= "&pageToken=$page_token";
}

$response = wp_remote_get($url);
$body = wp_remote_retrieve_body($response);
$body = json_decode($body, true);

$next_page_token = isset($body['nextPageToken']) ? $body['nextPageToken'] : null;
$prev_page_token = isset($body['prevPageToken']) ? $body['prevPageToken'] : null;

$has_next_page   = $next_page_token || (isset($page_number) && isset($total_pages) && $page_number < $total_pages) ? true : false;
$has_prev_page   = $prev_page_token || (isset($page_number) && isset($total_pages) && $page_number > 1) ? true : false;

$items = $body['items'];
if ($items) {
?>
  <div class="posts-grid">
    <div class="posts-grid__inner">
      <?php
      foreach ($items as $item) {
        $video = $item['snippet'];
        $title = $video['title'];
        $thumbnail = isset($video['thumbnails']['standard']) ? $video['thumbnails']['standard'] : $video['thumbnails']['default'];
        $thumbnail_img = '<img src="' . $thumbnail['url'] . '" alt="' . $title . '" width="' . $thumbnail['width'] . '" height="' . $thumbnail['height'] . '" loading="lazy" />';
        $meta = array();
        $date = $video['publishedAt'];
        if ($date) {
          $date_format = date(get_option('date_format'), strtotime($date));
          $meta[] = $date_format;
        }
        $video_id = $item['kind'] == 'youtube#video' && isset($item['id']) && $item['id'] ? $item['id'] : $video['resourceId']['videoId'];


        get_template_part('template-parts/post-entry', '', array(
          'class'          => 'post-entry--list',
          'show_thumbnail' => true,
          'show_meta'      => true,
          'show_icon'      => true,
          'post'           => array(
            'title'     => $title,
            'thumbnail' => $thumbnail_img,
            'meta'      => $meta,
            'link'      => array(
              'url'     => 'https://www.youtube.com/watch?v=' . $video_id,
              'target'  => '_blank',
            ),
            'is_popup'  => true,
          ),
        ));
      }
      ?>
    </div>
  </div>

  <?php if ($has_next_page || $has_prev_page) { ?>
    <div class="page-numbers-wrapper page-numbers-wrapper--bottom">
      <div class="page-numbers-wrapper__inner">
        <div class="page-numbers-list">
          <ul class="page-numbers">
            <li>
              <button class="prev page-numbers" <?php echo !$has_prev_page ? 'disabled' : ''; ?> data-token="<?php echo $prev_page_token ?: (isset($page_number) ? $page_number - 1 : 0); ?>">
                <?php the_svg('arrow-prev', 'Previous Page'); ?>
              </button>
            </li>
            <li>
              <button class="next page-numbers" <?php echo !$has_next_page ? 'disabled' : ''; ?> data-token="<?php echo $next_page_token ?: (isset($page_number) ? $page_number + 1 : 0); ?>">
                <?php the_svg('arrow-next', 'Next Page'); ?>
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  <?php } ?>
<?php
}
