<?php
$class = $args && isset($args['class']) ? $args['class'] : '';
$data = $args && isset($args['data']) ? $args['data'] : '';

if (!$data) {
  return null;
}
?>

<div class="vertical-tabs__accordions <?php echo $class ? $class . '__accordions' : ''; ?> md-hide">
  <?php get_template_part('template-parts/accordions', '', ['data' => $data]); ?>
</div>
<div class="vertical-tabs__tabs <?php echo $class ? $class . '__tabs' : ''; ?> xs-hide md-show">
  <div class="lb-tabs lb-tabs--vertical no-scroll">
    <?php
    $tabs = '';
    $tab_panels = '';
    $i = 0;
    foreach ($data as $tab) {
      $title = $tab['title'];
      $body = $tab['body'];
      $key = sanitize_title($title);
      $active = $i == 0 ? 'active' : '';
      $selected = $i == 0 ? 'true' : '';
      $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$title</span></a>";
      $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'><h3 class='color-primary'>$title</h3>$body</div>";
      $i++;
    }
    ?>
    <div class="tabs" role="tablist"><?php echo $tabs; ?></div>
    <div class="tab-panels">
      <div class="card">
        <?php echo $tab_panels; ?>
      </div>
    </div>
  </div>
</div>