<?php
$query_year     = isset($_GET['year']) && $_GET['year'] ? $_GET['year'] : '';
$query_month    = isset($_GET['month']) && $_GET['month'] ? $_GET['month'] : '';

// Reference: wp_get_archive()
// Reference URL: https://developer.wordpress.org/reference/functions/wp_get_archives/
global $wpdb, $wp_locale;

$defaults = [
  'type'            => 'monthly',
  'limit'           => '',
  'format'          => 'html',
  'before'          => '',
  'after'           => '',
  'show_post_count' => false,
  'echo'            => 1,
  'order'           => 'DESC',
  'post_type'       => 'post',
  'year'            => get_query_var('year'),
  'monthnum'        => get_query_var('monthnum'),
  'day'             => get_query_var('day'),
  'w'               => get_query_var('w'),
];

$parsed_args  = wp_parse_args($args, $defaults);
$sql_where    = $wpdb->prepare("WHERE post_type = %s AND post_status = 'publish'", $parsed_args['post_type']);
$where        = apply_filters('getarchives_where', $sql_where, $parsed_args);
$join         = apply_filters('getarchives_join', '', $parsed_args);

$output   = '';
$archive  = [];

$last_changed = wp_cache_get_last_changed('posts');

$limit = $parsed_args['limit'];

if ('monthly' === $parsed_args['type']) {
  $query   = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts $join $where GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date $order $limit";
  $key     = md5($query);
  $key     = "wp_get_archives:$key:$last_changed";
  $results = wp_cache_get($key, 'posts');
  if (!$results) {
    $results = $wpdb->get_results($query);
    wp_cache_set($key, $results, 'posts');
  }
  if ($results) {
    $after = $parsed_args['after'];
    foreach ((array) $results as $result) {
      $url        = get_month_link($result->year, $result->month);
      $month_name = $wp_locale->get_month($result->month);
      $selected   = (string) $parsed_args['year'] === $result->year && (string) $parsed_args['monthnum'] === $result->month;

      if (!$selected) {
        $selected   = (string) $query_year === $result->year && (string) ($query_month - 1) === $result->month;
      }

      if (!isset($archive[$result->year][$month_name])) :
        $archive[$result->year][$month_name] = ['url' => $url, 'selected' => $selected];
      endif;
    }
  }
}

$i = 0;
$col1 = '';
$col2 = '';
if ($archive) :
  foreach ($archive as $year => $months) :
    $controls = 'archive-' . $year;
    $i++;
    $icon = get_the_svg('angle-down');
    $year_class = !empty(array_filter($months, function ($month) {
      return $month['selected'];
    })) ? 'active' : null;
    $output = "";
    $output .= "<li class='accordion'><button id='$controls-control' type='button' aria-controls='$controls' aria-expanded='false' class='$year_class'>$year$icon</button>";
    $output .= "<ul id='$controls' class='accordion__body' aria-labelledby='$controls-control' aria-expanded='false'>";
    foreach ($months as $month_name => $month) :
      $class  = $month['selected'] ? "active" : null;
      $link   = $month['url'];
      $output .= "<li><a href='$link' class='$class'>$month_name</a></li>";
    endforeach;
    $output .= "</ul>"; // accordion__body
    $output .= "</li>"; // accordion
    if ($i > ceil(count($archive) / 2)) :
      $col2 .= $output;
    else :
      $col1 .= $output;
    endif;
  endforeach;
?>
  <section class="widget widget-year">
    <h2 class="h4 widget-title">Archives</h2>
    <ul class="accordions">
      <li class="col">
        <ul class="archives-list"><?php echo $col1; ?></ul>
      </li>
      <li class="col">
        <ul class="archives-list"><?php echo $col2; ?></ul>
      </li>
    </ul>
  </section>
<?php endif; ?>