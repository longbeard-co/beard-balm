<?php
$show_search      = $args && isset($args['show_search']) ? $args['show_search'] : true;
$show_recent      = $args && isset($args['show_recent']) ? $args['show_recent'] : true;
$show_archive     = $args && isset($args['show_archive']) ? $args['show_archive'] : true;

$search_placeholder = $args && isset($args['search_placeholder']) ? $args['search_placeholder'] : 'Search Posts';
?>

<aside class="sidebar widget-area">
  <?php if ($show_search) { ?>
    <section class="widget widget-search">
      <?php echo lb_get_search_form('sidebar-search', '', '/media', $search_placeholder, 'search'); ?>
    </section>
  <?php } ?>
  <?php if ($show_recent) { ?>
    <section class="widget widget-recent">
      <h2 class="h4 widget-title">Recent Posts</h2>
      <div class="posts-grid">
        <div class="posts-grid__inner">
          <?php
          $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 3,
            'no_found_rows' => true,
            'ignore_sticky_posts' => true,
          );
          $the_query = new WP_Query($args);
          if ($the_query->have_posts()) {
            while ($the_query->have_posts()) {
              $the_query->the_post();
              get_template_part('template-parts/post-entry', '', [
                'show_thumbnail' => false,
                'show_meta' => true,
                'show_excerpt' => false,
              ]);
            }
          }
          wp_reset_postdata();
          ?>
        </div>
      </div>
    </section>
  <?php } ?>
  <?php if ($show_archive) { ?>
    <?php get_template_part('template-parts/widget-year-archive'); ?>
  <?php } ?>

  <?php
  // Custom CTAs

  if (have_rows('sidebar_cta')) {
    while (have_rows('sidebar_cta')) {
      the_row();
      $title = get_sub_field('title');
      $text  = get_sub_field('text');
      $link  = get_sub_field('link');

  ?>
      <section class="sidebar-cta">
        <div class="sidebar-cta__inner">
          <h2 class="h3 color-primary"><?php echo $title; ?></h2>
          <?php if ($text) { ?>
            <div class="sidebar-cta__text text-block">
              <?php echo $text; ?>
            </div>
          <?php } ?>
          <?php if ($link) {
            $link_title = $link['title'];
            $link_url = $link['url'];
            $link_target = $link['target'];
          ?>
            <p class="align-right"><a href="<?php echo $link_url; ?>" class="button alt" target="<?php echo $link_target; ?>"><span></span><?php echo $link_title; ?></a></p>
          <?php } ?>
        </div>
      </section>
  <?php
    }
  }
  ?>
</aside>