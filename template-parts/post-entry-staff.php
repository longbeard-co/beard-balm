<?php
$class          = $args && isset($args['class']) ? $args['class'] : '';

$name     = get_field('display_name') ?: get_the_title();
$img      = has_post_thumbnail() ? get_the_post_thumbnail(null, 'large') : wp_get_attachment_image(lb_get_person_placeholder_id(), 'large');
$bio      = get_the_excerpt();
$link     = get_permalink();

?>
<article class="post-entry post-entry--staff <?php echo $class; ?>">
  <div class="post-entry__inner">
    <div class="post-entry__img">
      <div class="post-thumbnail post-thumbnail--staff">
        <?php echo $img; ?>
      </div>
    </div>
    <div class="post-entry__details">
      <div class="post-entry__text">
        <a href="<?php echo esc_url($link); ?>" class="post-entry__link-overlay" target="_self" rel="">
          <h3 class="post-entry__title h5"><?php echo $name; ?></h3>
        </a>
        <div class="post-entry__excerpt">
          <?php echo $bio; ?>
        </div>
        <p class="post-entry__text__more align-right">
          <a href="<?php the_permalink(); ?>" class="button-more">View Bio</a>
        </p>
      </div>
    </div>
  </div>
</article>