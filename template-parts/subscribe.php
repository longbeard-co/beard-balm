<?php
$class = $args && isset($args['class']) ? $args['class'] : '';
$type = $args && isset($args['type']) ? $args['type'] : 'default';
$col_class = $args && isset($args['col_class']) ? $args['col_class'] : 'col-xs-12 col-lg-10 col-lg-offset-1';

switch ($type) {
  case 'about':
    $main_img = 115;
    $bg_img = 117;
    break;
  case 'contact':
    $main_img = 155;
    $bg_img = 0;
    break;
  case 'alumni':
    $main_img = 204;
    $bg_img = 205;
    break;
  case 'media':
    $main_img = 273;
    $bg_img = 117;
    break;
  case 'bookstore':
    $main_img = 24;
    $bg_img = 23;
    break;
  default:
    $main_img = 303;
    $bg_img = 307;
}

?>

<section class="container row subscribe subscribe--<?php echo $type; ?> <?php echo $class; ?>">
  <div class="<?php echo $col_class; ?>">
    <div class="subscribe__inner">
      <?php if ($bg_img) { ?>
        <div class="subscribe__bg xs-hide sm-show">
          <?php echo wp_get_attachment_image($bg_img, 'large'); ?>
        </div>
      <?php } ?>
      <div class="subscribe__text">
        <h2 class="h1 color-primary">Subscribe to Our Newsletter</h2>
        <p>Stay updated on all offerings from the Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies.</p>
        <div class="subscribe__form">
          <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
        </div>
      </div>
      <div class="subscribe__img xs-hide sm-show">
        <div class="subscribe__img__inner">
          <?php echo wp_get_attachment_image($main_img, 'large'); ?>
        </div>
      </div>
    </div>
  </div>
</section>