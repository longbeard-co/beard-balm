<?php

$author_id      = isset($args) && isset($args['author_id']) ? $args['author_id'] : false;
$class          = isset($args) && isset($args['class']) ? $args['class'] : '';
$show_thumbnail = isset($args) && isset($args['show_thumbnail']) ? $args['show_thumbnail'] : false;
$show_meta      = isset($args) && isset($args['show_meta']) ? $args['show_meta'] : false;

$custom_post    = $args && isset($args['post']) ? $args['post'] : '';

if ($custom_post) {
  // Useful for custom / external data
  $title       = isset($custom_post['title']) ? $custom_post['title'] : null;
  $subtitle    = isset($custom_post['subtitle']) ? $custom_post['subtitle'] : null;
  $desc        = isset($custom_post['desc']) ? $custom_post['desc'] : null;
  $format      = '';
  $price       = null;

  $link        = isset($custom_post['link']) ? $custom_post['link'] : null;
  $link_url    = $link && isset($link['url']) ? $link['url'] : null;
  $link_target = $link && isset($link['target']) ? $link['target'] : '_self';

  $thumbnail   = isset($custom_post['thumbnail']) ? $custom_post['thumbnail'] : null;
  $meta        = isset($custom_post['meta']) ? $custom_post['meta'] : array();

  $is_popup    = isset($custom_post['is_popup']) ? $custom_post['is_popup'] : false;
} else {
  $product     = wc_get_product(get_the_ID());

  $title       = get_the_title();
  $subtitle    = get_field('byline');

  $attributes  = array_map(function ($attr) {
    return implode(', ', $attr['options']);
  }, $product->get_attributes());

  $desc        = array(
    implode('. ', $attributes),
  );
  $price       = $product->get_price();
  $link        = $product->get_permalink();

  $link_url    = get_the_permalink();
  $link_target = '_self';

  $thumbnail   = get_the_post_thumbnail(null, 'large');

  $is_popup    = false;
}


if ($author_id) {
  $articles = get_field('article_categories');
  if (!empty($articles)) {
    $author_article = lb_product_article_search($articles, $author_id);

    if ($author_article) {
      $subtitle = $title;
      $title    = $author_article['title'];
    }
  }
}

?>
<article class="post-entry post-entry--book <?php echo $class; ?>">
  <div class="post-entry__inner">
    <?php if ($show_thumbnail) { ?>
      <div class="post-entry__img">
        <?php if ($thumbnail) {
          echo '<div class="post-thumbnail post-thumbnail--book">';
          echo $thumbnail;
          echo '</div>';
        } ?>
      </div>
    <?php } ?>
    <div class="post-entry__details">
      <div class="post-entry__text">
        <?php if ($link_url) { ?>
          <a href="<?php echo esc_url($link_url); ?>" class="post-entry__link-overlay" target="<?php echo $link_target; ?>" rel="<?php echo $link_target == '_blank' ? 'noopener noreferrer' : ''; ?>" <?php echo $is_popup ? 'data-lity' : ''; ?>>
            <h3 class="post-entry__title h5"><?php echo $title; ?></h3>
            <?php if ($subtitle) { ?>
              <h4 class="post-entry__subtitle h6"><?php echo $subtitle; ?></h4>
            <?php } ?>
          </a>
        <?php } else { ?>
          <h3 class="post-entry__title h5"><?php echo $title; ?></h3>
          <?php if ($subtitle) { ?>
            <h4 class="post-entry__subtitle h6"><?php echo $subtitle; ?></h4>
          <?php } ?>
        <?php } ?>
        <?php if ($show_meta) { ?>
          <div class="post-entry__meta-group">
            <ul>
              <?php foreach ($desc as $desc_item) { ?>
                <li class="post-entry__meta-group__desc"><?php echo $desc_item; ?></li>
              <?php } ?>
              <?php if ($price) { ?>
                <li class="post-entry__meta-group__price"><?php echo wc_price($price); ?></li>
              <?php } ?>
            </ul>
          </div>
        <?php } ?>

      </div>
    </div>
  </div>
</article>