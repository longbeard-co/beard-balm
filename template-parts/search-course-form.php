<?php

$checkbox   = $args && isset($args['checkbox']) ? $args['checkbox'] : false;

$categories = array(
  ''             => 'All',
  'history'      => 'History',
  'liturgy'      => 'Liturgy',
  'spirituality' => 'Spirituality',
  'theology'     => 'Theology'
);
$semesters = array(
  ''       => 'All',
  'fall'   => 'Fall',
  'winter' => 'Winter',
);

$default_semester = isset($_GET['semester']) && $_GET['semester'] ? array_filter($_GET['semester']) : array();
$default_category = isset($_GET['category']) && $_GET['category'] ? array_filter($_GET['category']) : array();
$default_search   = isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '';

$default_semester_string = '';
if (!empty($default_semester)) {
  $default_semester_string = array_filter($default_semester, function ($s) use ($semesters) {
    return isset($semesters[$s]) && $semesters[$s];
  });
  if (!empty($default_semester_string)) {
    $default_semester_string = array_map(function ($s) use ($semesters) {
      return $semesters[$s];
    }, $default_semester);
    $default_semester_string = implode(', ', $default_semester_string);
  }
}

$default_category_string = '';
if (!empty($default_category)) {
  $default_category_string = array_filter($default_category, function ($s) use ($categories) {
    return isset($categories[$s]) && $categories[$s];
  });
  if (!empty($default_category_string)) {
    $default_category_string = array_map(function ($s) use ($categories) {
      return $categories[$s];
    }, $default_category);
    $default_category_string = implode(', ', $default_category_string);
  }
}

?>

<form id="search-course-form" class="search-course" action="/academics/courses/" method="GET">
  <div class="search-course__field">
    <h3 class="search-course__field__label h5">Search Courses</h3>
    <div class="search-course__fields box-shadow">
      <div class="search-course__fields__select search-course__fields__select--semester">
        <div class="select-wrapper">
          <button type="button" data-dropdown="semester-dropdown" data-event="semesterUpdated" data-placeholder="Semester" style="width:7.75em;">
            <?php echo $default_semester_string ?: 'Semester'; ?>
          </button>
        </div>
        <div id="semester-dropdown" class="search-course__fields__select__dropdown">
          <fieldset class="search-course__fields__checkboxes">
            <legend class="screen-reader-text">Choose Semester</legend>
            <ul>
              <?php foreach ($semesters as $key => $value) { ?>
                <li>
                  <input type="checkbox" name="semester[]" id="semester-<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo in_array($key, $default_semester) || (!$key && empty($default_semester)) ? 'checked' : ''; ?> />
                  <label for="semester-<?php echo $key; ?>">
                    <?php echo $value; ?>
                  </label>
                </li>
              <?php } ?>
            </ul>
          </fieldset>
        </div>
      </div>
      <?php if (!$checkbox) { ?>
        <div class="search-course__fields__select search-course__fields__select--category">
          <div class="select-wrapper">
            <button type="button" data-dropdown="category-dropdown" data-event="categoryUpdated" data-placeholder="Course Category" style="width:11em;">
              <?php echo $default_category_string ?: 'Course Category'; ?>
            </button>
          </div>
          <div id="category-dropdown" class="search-course__fields__select__dropdown">
            <fieldset class="search-course__fields__checkboxes">
              <legend class="screen-reader-text">Choose Course Category</legend>
              <ul>
                <?php foreach ($categories as $key => $value) { ?>
                  <li>
                    <input type="checkbox" name="category[]" id="category-<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo in_array($key, $default_category) || (!$key && empty($default_category)) ? 'checked' : ''; ?> />
                    <label for="category-<?php echo $key; ?>">
                      <?php echo $value; ?>
                    </label>
                  </li>
                <?php } ?>
              </ul>
            </fieldset>
          </div>
        </div>
      <?php } ?>
      <div class="search-course__fields__input">
        <label for="search-course-keyword">
          <span class="screen-reader-text">Search by Keyword</span>
          <input type="search" name="search" id="search-course-keyword" placeholder="Search by Keyword" value="<?php echo $default_search; ?>">
        </label>
        <button type="submit" form="search-course-form"><?php the_svg('search', 'Search Courses'); ?></button>
      </div>
    </div>
  </div>
  <?php if ($checkbox) { ?>
    <fieldset class="search-course__field">
      <legend class="screen-reader-text">Course Category</legend>
      <h3 class="search-course__field__label h5">Course Category</h3>
      <div class="search-course__checkboxes">
        <?php foreach ($categories as $key => $category) { ?>
          <div class="search-course__checkbox">
            <input type="checkbox" name="category[]" id="search-course-categories-<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo in_array($key, $default_category) || (!$key && empty($default_category)) ? 'checked' : ''; ?>>
            <label for="search-course-categories-<?php echo $key; ?>">
              <?php echo $category; ?>
            </label>
          </div>
        <?php } ?>
      </div>
    </fieldset>
  <?php } ?>
</form>