module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    postcss: {
      options: {
        map: {
          inline: false, // disable option to save all sourcemaps as separate files
        },

        processors: [
          require("autoprefixer")({ grid: true }), // add vendor prefixes and IE grid support, see package.json for Browserslist config
          require("cssnano")(), // minify the result
          require("postcss-flexibility"), // add vendor prefix for flex-fallback
        ],
      },
      dist: {
        src: "*.css",
      },
    },
    prettier: {
      options: {
        progress: true, // Show progress bar
      },
      files: {
        src: [
          "scss/**.scss",
          "**/**/*.scss",
          "!scss/utilities/_breakpoint.scss",
          "!scss/utilities/breakpoint/*",
          "!scss/utilities/breakpoint/**/**",
        ],
      },
    },
    watch: {
      css: {
        files: ["scss/*.scss", "**/**/*.scss"],
        tasks: ["sass"],
      },
      js: {
        files: ["js/theme.js"],
        tasks: ["babel"],
      },
      // scripts: {
      //  files: ['**/*.css'],
      //  tasks: ['postcss'],
      // },
    },
    sass: {
      dist: {
        files: {
          "style.css": "scss/style.scss",
          "login.css": "scss/login.scss",
        },
      },
    },
    babel: {
      options: {
        sourceMap: true,
        presets: ["@babel/preset-env"],
      },
      dist: {
        files: {
          "js/dist/theme.js": "js/theme.js",
        },
      },
    },
  });

  grunt.loadNpmTasks("@lodder/grunt-postcss");
  grunt.loadNpmTasks("grunt-prettier");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-babel");
  grunt.registerTask("default", ["postcss"]);
  grunt.registerTask("grunt-babel");
};
