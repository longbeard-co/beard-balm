<?php

/**
 * Footer Template
 *
 * Default footer template. Everything after <div id="content">. Adapted from Underscores.
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

?>

</div><!-- #content -->

<?php
function get_policy_links($class = '') {
?>
  <div class="footer__links <?php echo $class; ?>">
    <ul>
      <li>
        <a href="/privacy-policy/">Privacy Policy</a>
      </li>
      <li>
        <a href="/cookie-policy/">Cookie Policy</a>
      </li>
    </ul>
  </div>
<?php
}
?>

<footer id="footer" class="footer">
  <div class="container">
    <div class="footer__inner">
      <div class="footer__logo">
        <?php echo '<img src="' . get_the_theme_image_src('logo-W.png') . '" alt="Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies" width="460" height="82" />'; ?>
        <?php get_policy_links('xs-hide md-show'); ?>
      </div>
      <div class="footer__menus">
        <ul>
          <li>
            <h3>About</h3>
            <?php wp_nav_menu([
              'theme_location' => 'menu-footer-1',
              'menu_class'     => 'footer__menu',
              'link_before'    => '<span>',
              'link_after'     => '</span>',
            ]); ?>
          </li>
          <li>
            <h3>Academics</h3>
            <?php wp_nav_menu([
              'theme_location' => 'menu-footer-2',
              'menu_class'     => 'footer__menu',
              'link_before'    => '<span>',
              'link_after'     => '</span>',
            ]); ?>
          </li>
          <li>
            <h3>Resources</h3>
            <?php wp_nav_menu([
              'theme_location' => 'menu-footer-3',
              'menu_class'     => 'footer__menu',
              'link_before'    => '<span>',
              'link_after'     => '</span>',
            ]); ?>
          </li>
        </ul>
      </div>
      <?php get_policy_links('md-hide'); ?>
      <div class="footer__side">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 180 270">
          <mask id="a" width="180" height="270" x="0" y="0" maskUnits="userSpaceOnUse" style="mask-type:alpha">
            <path fill="#fff" d="m100.624 197.639 23.945 19.957v36.46l-23.945-19.961v35.13H79.376v-52.836l-23.944-19.961v-36.455l23.944 19.956V82.98H0V61.73h79.376V35.866h-24.42V14.614h24.42V0h21.248v14.614h24.425v21.252h-24.425v25.863H180V82.98h-79.376V197.64Z" />
          </mask>
          <g mask="url(#a)">
            <path fill="#fff" d="M-18-55h568v355H-18z" />
          </g>
        </svg>
      </div>
      <a class="lb-colophon" href="https://www.longbeard.com" target="_blank" rel="noopener noreferrer">
        <?php echo '<img src="' . get_the_theme_image_src('lb-colophon-white.svg') . '" alt="Built by Longbeard" width="30" height="15" />'; ?>
      </a>
    </div>
  </div>
</footer><!-- #footer -->
</div><!-- #page -->
</div><!-- .site-wrapper -->

<?php wp_footer(); ?>
<?php if (get_field('slick', 'option')) : ?>
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<?php endif; ?>
<?php if (get_field('lightgallery', 'option')) : ?>
  <script src="/wp-content/themes/beardbalm/vendor/lightgallery/js/lightgallery.js"></script>
<?php endif; ?>
<?php if (get_field('lity', 'option')) : ?>
  <link href="/wp-content/themes/beardbalm/vendor/lity/dist/lity.min.css" rel="stylesheet">
  <script src="/wp-content/themes/beardbalm/vendor/lity/dist/lity.min.js"></script>
<?php endif; ?>
<?php if (get_field('aos', 'option')) : ?>
  <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
<?php endif; ?>
<?php if (get_field('instant_page', 'option') && !is_admin() && is_admin_bar_showing()) : ?>
  <script src="//instant.page/5.1.0" type="module" integrity="sha384-by67kQnR+pyfy8yWP4kPO12fHKRLHZPfEsiSXR8u2IKcTdxD805MGUXBzVPnkLHw"></script>
<?php endif; ?>
</body>

</html>