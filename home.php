<?php

/**
 * Template name: Homepage
 *
 * Boilerplate template for the homepage.
 *
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section class="container page-m-t section-m-b--sm hero">
			<div class="hero__inner">
				<div class="hero__text">
					<div class="hero__text__inner">
						<h1 class="h2">
							Over <span class="h1 big">35 years</span> of<br />
							<span class="h1 color-primary">Eastern Christian Studies</span><br />
							in Canada
						</h1>
						<div class="end-xs">
							<a href="/about" class="button">
								<span></span>
								<span>Learn More</span>
							</a>
						</div>
					</div>
				</div>
				<div class="hero__img">
					<div class="hero__img__inner">
						<?php echo wp_get_attachment_image(1197,  'full', '', ['class' => 'xs-hide sm-show']); ?>
						<?php echo wp_get_attachment_image(386,  'full', '', ['class' => 'sm-hide']); ?>
					</div>
				</div>
			</div>
		</section>

		<section class="container row section-m-b about">
			<div class="col-xs-12 col-lg-10 col-lg-offset-1">
				<div class="about__inner">
					<div class="about__img xs-hide sm-show">
						<?php echo wp_get_attachment_image(13, 'large'); ?>
					</div>
					<div class="about__text">
						<h2 class="h1">About Us</h2>
						<div class="text-col">
							<p>The Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies (MASI) is an autonomous academic unit of the Faculty of Theology of the University of St. Michael’s College in the University of Toronto.</p>
							<blockquote>
								It specializes in the theology, spirituality, liturgy, history,
								and ecclesial polity of the Eastern Christian Churches,
								both Orthodox and Catholic.
							</blockquote>
							<p>Since July 1, 2017, MASI has been an autonomous academic unit within USMC’s Faculty of Theology and part of the Toronto School of Theology, an ecumenical consortium of 7 colleges.</p>
							<p>The Sheptytsky Institute publishes a peer-reviewed journal, LOGOS: A Journal of Eastern Christian Studies.</p>
							<p class="align-right"><a href="/about" class="button"><span></span>Learn More</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/degree-programs-overview'); ?>

		<?php
		$faculty = array(
			array(
				'image' => 314,
				'name' => 'Fr. Alexander Laschuk',
				'role' => 'Executive Director',
				'link' => '/staff/alexander-laschuk/'
			),
			array(
				'image' => 1200,
				'name' => 'Fr. Andrew Summerson',
				'role' => 'Assistant Professor of Greek Patristics',
				'link' => '/staff/fr-andrew-summerson/'
			),
			array(
				'image' => 1198,
				'name' => 'Rev. Dr. Andriy Chirovsky',
				'role' => 'Professor',
				'link' => '/staff/andriy-chirovsky/'
			),
			array(
				'image' => 312,
				'name' => 'Rev. Dr. Peter Galadza',
				'role' => 'Professor Emeritus',
				'link' => '/staff/peter-galadza/'
			),
		);
		?>

		<section class="container row section-m-t section-m-b faculty">
			<div class="faculty__bg xs-hide sm-show">
				<?php echo wp_get_attachment_image(84, 'large'); ?>
			</div>
			<div class="col-xs-12 col-md-11 col-lg-9 col-lg-offset-1">
				<h2 class="h1">Faculty</h2>
				<p class="narrow">Our institute hosts leading academics who contribute to a truly unique educational experience in Eastern Christian Studies.</p>
				<div class="faculty__list person-list">
					<ul>
						<?php
						if (!empty($faculty)) {
							foreach ($faculty as $person) {
						?>
								<li>
									<div class="person">
										<div class="person__img">
											<div class="person__img__inner">
												<?php echo wp_get_attachment_image($person['image'], 'large'); ?>
											</div>
										</div>
										<div class="person__text">
											<h3 class="h6 person__name"><?php echo $person['name']; ?></h3>
											<p class="small person__meta"><?php echo $person['role']; ?></p>
											<p class="align-right">
												<a href="<?php echo $person['link']; ?>"><button class="button-more">Learn More</button></a>
											</p>
										</div>
									</div>
								</li>
						<?php
							}
						}
						?>

					</ul>
				</div>
				<div class="end-xs">
					<a href="/faculty/" class="button"><span></span>View More</a>
				</div>
			</div>
		</section>

		<section class="container section-m-t section-m-b news-events">
			<div class="news-events__bg xs-hide md-show">
				<?php echo wp_get_attachment_image(302, 'large'); ?>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
					<h2 class="h1">News & Events</h2>
					<div class="news-events__tabs">
						<div class="lb-tabs">
							<div class="tabs" role="tablist">
								<a href="#news" id="tab-news" class="tab active" aria-selected="true" aria-controls="news" role="tab"><span>News</span></a>
								<a href="#events" id="tab-events" class="tab" aria-selected="false" aria-controls="events" role="tab"><span>Events</span></a>
							</div>
							<div class="tab-panels">
								<div id="news" class="tab-panel active" aria-expanded="true" aria-labelledby="tab-news" role="tabpanel">
									<?php
									$args = array(
										'post_type' 					=> 'post',
										'post_status' 				=> 'publish',
										'posts_per_page' 			=> 4,
										'no_found_rows' 			=> true,
										'ignore_sticky_posts' => true,
										'fields' 							=> 'ids',
										'category_name'			  => 'news',
									);
									$the_query = new WP_Query($args);
									$the_posts = $the_query->get_posts();
									get_template_part('template-parts/featured-posts', '', ['posts' => $the_posts, 'link' => '/media/', 'post_type' => 'post']);
									?>
								</div>
								<div id="events" class="tab-panel" aria-expanded="false" aria-labelledby="tab-events" role="tabpanel">
									<?php
									$events_query = tribe_get_events([
										'posts_per_page' => 4,
										'start_date'     => 'now',
									], true);
									$event_ids = $events_query->get_posts();

									get_template_part('template-parts/featured-posts', '', ['posts' => $event_ids, 'link' => '/media?category=events', 'post_type' => 'tribe_events']);
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="container row section-m-t section-m-b bookstore">
			<div class="col-xs-12 col-md-10 col-lg-9 col-lg-offset-1">
				<h2 class="h1">Bookstore</h2>
				<p class="narrow">MASI’s bookstore sells publications in Eastern Christian Studies published by our own Institute as well as other publishing houses.</p>
				<div class="lb-tabs">
					<div class="tabs" role="tablist">
						<a href="#books" id="tab-books" class="tab active" aria-selected="true" aria-controls="books" role="tab"><span>Books</span></a>
						<a href="#journals" id="tab-journals" class="tab" aria-selected="false" aria-controls="journals" role="tab"><span>Journals</span></a>
						<a href="#media" id="tab-media" class="tab" aria-selected="false" aria-controls="media" role="tab"><span>Media</span></a>
					</div>
					<div class="tab-panels">
						<div id="books" class="tab-panel active" aria-expanded="true" aria-labelledby="tab-books" role="tabpanel">
							<?php get_template_part('template-parts/featured-products', '', array('category' => 'books')); ?>
						</div>
						<div id="journals" class="tab-panel" aria-expanded="false" aria-labelledby="tab-journals" role="tabpanel">
							<?php get_template_part('template-parts/featured-products', '', array('category' => 'journals')); ?>
						</div>
						<div id="media" class="tab-panel" aria-expanded="false" aria-labelledby="tab-media" role="tabpanel">
							<?php get_template_part('template-parts/featured-products', '', array('category' => 'cds-dvds-posters')); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php get_template_part('template-parts/subscribe', '', array('class' => 'section-m-t section-m-b')); ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
