<?php

/**
 * LB - Custom Post Types
 *
 * Houses all the CPTs for this project. Includes a starter template to work off.
 *
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

// Register Custom Post Type
function lb_cpt_degree_program() {

  $labels = array(
    'name'                  => _x('Degree Programs', 'Post Type General Name', 'beardbalm'),
    'singular_name'         => _x('Degree Program', 'Post Type Singular Name', 'beardbalm'),
    'menu_name'             => __('Degree Programs', 'beardbalm'),
    'name_admin_bar'        => __('Degree Program', 'beardbalm'),
    'archives'              => __('Degree Program Archives', 'beardbalm'),
    'attributes'            => __('Degree Program Attributes', 'beardbalm'),
    'parent_item_colon'     => __('Parent Degree Program:', 'beardbalm'),
    'all_items'             => __('All Degree Programs', 'beardbalm'),
    'add_new_item'          => __('Add New Degree Program', 'beardbalm'),
    'add_new'               => __('Add New', 'beardbalm'),
    'new_item'              => __('New Degree Program', 'beardbalm'),
    'edit_item'             => __('Edit Degree Program', 'beardbalm'),
    'update_item'           => __('Update Degree Program', 'beardbalm'),
    'view_item'             => __('View Degree Program', 'beardbalm'),
    'view_items'            => __('View Degree Programs', 'beardbalm'),
    'search_items'          => __('Search Degree Programs', 'beardbalm'),
    'not_found'             => __('Not found', 'beardbalm'),
    'not_found_in_trash'    => __('Not found in Trash', 'beardbalm'),
    'featured_image'        => __('Featured Image', 'beardbalm'),
    'set_featured_image'    => __('Set featured image', 'beardbalm'),
    'remove_featured_image' => __('Remove featured image', 'beardbalm'),
    'use_featured_image'    => __('Use as featured image', 'beardbalm'),
    'insert_into_item'      => __('Insert into Degree Program', 'beardbalm'),
    'uploaded_to_this_item' => __('Uploaded to this Degree Program', 'beardbalm'),
    'items_list'            => __('Degree Programs list', 'beardbalm'),
    'items_list_navigation' => __('Degree Programs list navigation', 'beardbalm'),
    'filter_items_list'     => __('Filter Degree Programs list', 'beardbalm'),
  );
  $args = array(
    'label'                 => __('Degree Programs', 'beardbalm'),
    'description'           => __('Degree Programs at the Sheptytsky Institute'),
    'labels'                => $labels,
    'supports'              => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'            => array(),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    // 'menu_position'         => 1,
    'menu_icon'             => 'dashicons-welcome-learn-more',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'rewrite'               => array('slug' => 'academics/degree-programs', 'with_front' => false),
  );

  register_post_type('degree_program', $args);
}

add_action('init', 'lb_cpt_degree_program', 0);


/**
 * Custom Taxonomy - Product Authors
 */

function lb_tax_product_authors() {

  $labels = array(
    'name'              => _x('Authors', 'taxonomy general name', 'beardbalm'),
    'singular_name'     => _x('Author', 'taxonomy singular name', 'beardbalm'),
    'search_items'      => __('Search Authors', 'beardbalm'),
    'all_items'         => __('All Authors', 'beardbalm'),
    'parent_item'       => __('Parent Author', 'beardbalm'),
    'parent_item_colon' => __('Parent Author:', 'beardbalm'),
    'edit_item'         => __('Edit Author', 'beardbalm'),
    'update_item'       => __('Update Author', 'beardbalm'),
    'add_new_item'      => __('Add New Author', 'beardbalm'),
    'new_item_name'     => __('New Author Name', 'beardbalm'),
    'menu_name'         => __('Authors', 'beardbalm'),
  );

  register_taxonomy('product_author', array('product'), array(
    'hierarchical'      => false,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_rest'      => true,
    'query_var'         => true,
    'rewrite'           => array('slug' => 'bookstore/authors'),
  ));
}

add_action('init', 'lb_tax_product_authors', 0);

/**
 * Faculty / Staff
 */

function lb_cpt_staff() {

  $labels = array(
    'name'                  => _x('Faculty / Staff Members', 'Post Type General Name', 'beardbalm'),
    'singular_name'         => _x('Faculty / Staff Member', 'Post Type Singular Name', 'beardbalm'),
    'menu_name'             => __('Faculty / Staff', 'beardbalm'),
    'name_admin_bar'        => __('Faculty / Staff', 'beardbalm'),
    'archives'              => __('Faculty / Staff Member Archives', 'beardbalm'),
    'attributes'            => __('Faculty / Staff Member Attributes', 'beardbalm'),
    'parent_item_colon'     => __('Parent Faculty / Staff Member:', 'beardbalm'),
    'all_items'             => __('All Faculty / Staff Members', 'beardbalm'),
    'add_new_item'          => __('Add New Faculty / Staff Member', 'beardbalm'),
    'add_new'               => __('Add New', 'beardbalm'),
    'new_item'              => __('New Faculty / Staff Member', 'beardbalm'),
    'edit_item'             => __('Edit Faculty / Staff Member', 'beardbalm'),
    'update_item'           => __('Update Faculty / Staff Member', 'beardbalm'),
    'view_item'             => __('View Faculty / Staff Member', 'beardbalm'),
    'view_items'            => __('View Faculty / Staff Members', 'beardbalm'),
    'search_items'          => __('Search Faculty / Staff Members', 'beardbalm'),
    'not_found'             => __('Not found', 'beardbalm'),
    'not_found_in_trash'    => __('Not found in Trash', 'beardbalm'),
    'featured_image'        => __('Featured Image', 'beardbalm'),
    'set_featured_image'    => __('Set featured image', 'beardbalm'),
    'remove_featured_image' => __('Remove featured image', 'beardbalm'),
    'use_featured_image'    => __('Use as featured image', 'beardbalm'),
    'insert_into_item'      => __('Insert into Faculty / Staff Member', 'beardbalm'),
    'uploaded_to_this_item' => __('Uploaded to this Faculty / Staff Member', 'beardbalm'),
    'items_list'            => __('Faculty / Staff Members list', 'beardbalm'),
    'items_list_navigation' => __('Faculty / Staff Members list navigation', 'beardbalm'),
    'filter_items_list'     => __('Filter Faculty / Staff Members list', 'beardbalm'),
  );
  $args = array(
    'label'                 => __('Faculty / Staff Members', 'beardbalm'),
    'description'           => __('Faculty / Staff Members at the Sheptytsky Institute'),
    'labels'                => $labels,
    'supports'              => array('title', 'editor', 'thumbnail', 'excerpt'),
    'taxonomies'            => array('position'),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    // 'menu_position'         => 1,
    'menu_icon'             => 'dashicons-admin-users',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    // 'rewrite'               => array('slug' => 'academics/degree-programs', 'with_front' => false),
  );

  register_post_type('staff', $args);
}

add_action('init', 'lb_cpt_staff', 5);

/**
 * Custom Taxonomy - Product Authors
 */

function lb_tax_position() {

  $labels = array(
    'name'              => _x('Positions', 'taxonomy general name', 'beardbalm'),
    'singular_name'     => _x('Position', 'taxonomy singular name', 'beardbalm'),
    'search_items'      => __('Search Positions', 'beardbalm'),
    'all_items'         => __('All Positions', 'beardbalm'),
    'parent_item'       => __('Parent Position', 'beardbalm'),
    'parent_item_colon' => __('Parent Position:', 'beardbalm'),
    'edit_item'         => __('Edit Position', 'beardbalm'),
    'update_item'       => __('Update Position', 'beardbalm'),
    'add_new_item'      => __('Add New Position', 'beardbalm'),
    'new_item_name'     => __('New Position Name', 'beardbalm'),
    'menu_name'         => __('Positions', 'beardbalm'),
  );

  register_taxonomy('position', array('staff'), array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_in_rest'      => true,
    // 'query_var'         => true,
    // 'rewrite'           => array('slug' => 'bookstore/authors'),
  ));
}

add_action('init', 'lb_tax_position', 0);
