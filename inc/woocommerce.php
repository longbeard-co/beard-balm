<?php


/**
 * WooCommerce
 */

// Remove WooCommerce CSS
add_filter('woocommerce_enqueue_styles', '__return_empty_array');


/**
 * Change number of related products output
 */

function lb_related_products_args($args) {
  $args['posts_per_page'] = 3; // 4 related products
  return $args;
}

add_filter('woocommerce_output_related_products_args', 'lb_related_products_args', 20);

/**
 * Change number of products that are displayed per page (shop page)
 */

// function lb_loop_shop_per_page($cols) {
//   // $cols contains the current number of products per page based on the value stored on Options –> Reading
//   // Return the number of products you wanna show per page.
//   $cols = get_query_var('per_page') ?: 12;
//   return $cols;
// }
// add_filter('loop_shop_per_page', 'lb_loop_shop_per_page', 99);

// function lb_change_product_query($query) {
//   if (!is_admin() && $query->query_vars['wc_query'] == 'product_query') {
//     $query->set('posts_per_page', get_query_var('per_page') ?: 9);
//   }
// }
// add_action('pre_get_posts', 'lb_change_product_query');

/**
 * Woocomerce Templates Adjustments
 */

function lb_get_woocommerce_top_header() {
  return '<div class="col-xs-12 xs-hide md-show"><div class="woocommerce-page-top-header">' . lb_get_woocommerce_header_links() . '</div></div>';
}

function lb_get_woocommerce_header_links() {
  $account_text = is_user_logged_in() ? 'My Account' : 'Login / Register';

  $html = '';
  $html .= '<div class="woocommerce-links">';
  $html .= '<ul>';
  $html .= '<li><a href="' . esc_url(wc_get_account_endpoint_url('my-account')) . '"><img src="' . get_template_directory_uri() . '/assets/icons/profile.svg" alt="Profile" width="15" height="15">' . $account_text . '</a></li>';
  if (!is_cart() && !is_checkout()) {
    $html .= '<li><a href="' . esc_url(wc_get_account_endpoint_url('cart')) . '"><img src="' . get_template_directory_uri() . '/assets/icons/cart.svg" alt="Cart" width="15" height="15">Cart</a></li>';
  }
  $html .= '</ul>';
  $html .= '</div>';
  return $html;
}

add_action('woocommerce_before_main_content', 'lb_woocommerce_nav_open', 15);
function lb_woocommerce_nav_open() {
  if (is_shop() || is_tax() || is_single() || is_page_template('page-templates/bookstore-authors.php')) {
    echo '<div class="woocommerce-page-header">';
    echo '<div class="woocommerce-page-header__inner">';

    if (is_single()) {
      ob_start();
      woocommerce_breadcrumb();
      $breadcrumbs = ob_get_clean();
      echo '<div class="woocommerce-page-header__breadcrumb">' . $breadcrumbs . '</div>';
    } else if (is_tax('product_author')) {
      $breadcrumbs = '<nav class="woocommerce-breadcrumb">';
      $breadcrumbs .= '<span><a href="' . wc_get_page_permalink('shop') . '">' . __('Bookstore', 'beardbalm') . '</a></span>';
      $breadcrumbs .= ' - ';
      $breadcrumbs .= '<span><a href="' . home_url('/bookstore/authors/') . '">' . __('Authors', 'beardbalm') . '</a></span>';
      $breadcrumbs .= ' - ';
      $breadcrumbs .= '<span>' . get_queried_object()->name . '</span>';
      $breadcrumbs .= '</nav>';
      echo '<div class="woocommerce-page-header__breadcrumb">' . $breadcrumbs . '</div>';
    } else if (is_shop() || is_tax() || is_page_template('page-templates/bookstore-authors.php')) {
      $title = is_page_template('page-templates/bookstore-authors.php') ? __('Authors', 'beardbalm') : __('Bookstore', 'beardbalm');
      echo '<div class="woocommerce-page-header__title"><h1>' . $title . '</h1></div>';
    }

    echo '<div class="woocommerce-page-header__links md-hide"> ' . lb_get_woocommerce_header_links() . '</div>';

    if (is_page_template('page-templates/bookstore-authors.php')) {
      $searchform = lb_get_search_form('authors-searchform', '', home_url('/bookstore/authors'), 'Search Authors', 'search', isset($_GET['search']) ? $_GET['search'] : '');
    } else {
      $searchform = lb_get_search_form('product-searchform', '', wc_get_page_permalink('shop'), 'Search Bookstore', '_sf_s', isset($_GET['_sf_s']) ? $_GET['_sf_s'] : '');
    }
    echo '<div class="woocommerce-page-header__search">' . $searchform . '</div>';

    if (!is_tax('product_author') && !is_page_template('page-templates/bookstore-authors.php')) {
      echo '<div class="woocommerce-page-header__filter md-hide">';
      echo '<button id="shop-filter-button" class="shop-filter-button" type="button">Filter</button>';
      echo '</div>';
    }

    echo '</div>';
    echo '</div>';


    // if (get_queried_object() && isset(get_queried_object()->taxonomy) && get_queried_object()->taxonomy == 'product_tag') {
    //   echo '<div class="filter-header">';
    //   echo '<div class="filter-header__inner">';
    //   echo '<a href="' . wc_get_page_permalink('shop') . '">&times;</a>';
    //   echo '<h1 class="h4">' . get_queried_object()->name . '</h1>';
    //   echo '</div>';
    //   echo '</div>';
    // }
  }
}
// add_action('woocommerce_before_main_content', 'lb_woocommerce_nav_close', 25);
// function lb_woocommerce_nav_close() {

// }

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/**
 * Wrap in Containers
 */
add_action('woocommerce_before_main_content', 'lb_woocommerce_before_main_content', 9);
function lb_woocommerce_before_main_content() {
  echo '<div class="container row page-m-t section-m-b">';
  if (is_shop() || is_tax() || is_singular()) {
    echo lb_get_woocommerce_top_header();
  }
  echo '<div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">';
}

add_action('woocommerce_before_shop_loop', 'lb_woocommerce_before_shop_loop', 9);
function lb_woocommerce_before_shop_loop() {
  echo '<div class="woocommerce columns-3">';
}

add_action('woocommerce_after_shop_loop', 'lb_woocommerce_after_shop_loop', 11);
function lb_woocommerce_after_shop_loop() {
  echo '</div>'; // woocommerce
}

add_action('woocommerce_after_main_content', 'lb_woocommerce_after_main_content', 7);
function lb_woocommerce_after_main_content() {
  echo '</div>'; // col
}

add_action('woocommerce_sidebar', 'lb_woocommerce_before_sidebar', 9);
function lb_woocommerce_before_sidebar() {
  echo '<div class="col-xs-12 col-md-3">';
}

add_action('woocommerce_sidebar', 'lb_woocommerce_after_sidebar', 11);
function lb_woocommerce_after_sidebar() {
  echo '</div></div>'; // col row-container
}

add_action('woocommerce_sidebar', 'lb_woocommerce_after_sidebar_cta', 25);
function lb_woocommerce_after_sidebar_cta() {
  get_template_part('template-parts/subscribe', '', array('type' => 'bookstore', 'class' => 'section-m-t section-m-b'));
}

add_action('woocommerce_shop_loop_item_title', 'lb_woocommerce_shop_loop_item_title', 5);
function lb_woocommerce_shop_loop_item_title() {
  echo '<div class="woocommerce-LoopProduct-text woocommerce-loop-product__text">';
}

add_action('woocommerce_shop_after_loop_item_title', 'lb_woocommerce_shop_after_loop_item_title', 5);
function lb_woocommerce_shop_after_loop_item_title() {
  echo '</div>';
}

remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');


/**
 * Navigation
 */

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 30);
add_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 20);
add_action('woocommerce_before_shop_loop', 'woocommerce_pagination', 40);
add_action('woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 8);
add_action('woocommerce_after_shop_loop', 'woocommerce_result_count', 9);

add_filter('woocommerce_pagination_args', 'filter_woocommerce_pagination_args', 10, 1);
function filter_woocommerce_pagination_args($array) {
  $array['prev_text'] = get_the_svg('arrow-prev', 'Previous Page');
  $array['next_text'] = get_the_svg('arrow-next', 'Next Page');
  $array['end_size'] = 1;
  $array['mid_size'] = 2;
  return $array;
};


/**
 * Product Search widget
 */

function lb_product_search_form($form) {
  $form = '<form role="search" method="get" class="woocommerce-product-search searchform" action="' . home_url('/') . '" >
    <label class="screen-reader-text" for="woocommerce-product-search-field-0">' . __('Search for:') . '</label>
    <input type="search" value="" placeholder="Search products" name="s" id="woocommerce-product-search-field-0" />
    <button type="submit" value="Search">' . get_the_svg('search') . '</button>
    <input type="hidden" name="post_type" value="product">
  </form>';

  return $form;
}
add_filter('get_product_search_form', 'lb_product_search_form');

/**
 * Quantiy Field
 */

function woocommerce_quantity_input($args = array(), $product = null, $echo = true) {
  if (is_null($product)) {
    $product = $GLOBALS['product'];
  }
  $defaults = array(
    'input_id'     => uniqid('quantity_'),
    'input_name'   => 'quantity',
    'input_value'  => '1',
    'classes'      => apply_filters('woocommerce_quantity_input_classes', array('input-text', 'qty', 'text'), $product),
    'max_value'    => apply_filters('woocommerce_quantity_input_max', -1, $product),
    'min_value'    => apply_filters('woocommerce_quantity_input_min', 1, $product),
    'step'         => apply_filters('woocommerce_quantity_input_step', 1, $product),
    'pattern'      => apply_filters('woocommerce_quantity_input_pattern', has_filter('woocommerce_stock_amount', 'intval') ? '[0-9]*' : ''),
    'inputmode'    => apply_filters('woocommerce_quantity_input_inputmode', has_filter('woocommerce_stock_amount', 'intval') ? 'numeric' : ''),
    'product_name' => $product ? $product->get_title() : '',
  );
  $args = apply_filters('woocommerce_quantity_input_args', wp_parse_args($args, $defaults), $product);
  // Apply sanity to min/max args - min cannot be lower than 0.
  $args['min_value'] = max($args['min_value'], 1);
  $args['max_value'] = 1 < $args['max_value'] ? $args['max_value'] : '';
  // Max cannot be lower than min if defined.
  if ('' !== $args['max_value'] && $args['max_value'] < $args['min_value']) {
    $args['max_value'] = $args['min_value'];
  }
  ob_start();
  if ($args['max_value'] && $args['min_value'] === $args['max_value']) {

    echo '<div class="quantity hidden">
			<input type="hidden" id="' . esc_attr($args['input_id']) . '" class="qty" name="' . esc_attr($args['input_name']) . '" value="' . esc_attr($args['min_value']) . '" />
		</div>';
  } else {
    /* translators: %s: Quantity. */
    // $label = ! empty( $args['product_name'] ) ? sprintf( esc_html__( '%s quantity', 'woocommerce' ), wp_strip_all_tags( $args['product_name'] ) ) : esc_html__( 'Quantity', 'woocommerce' );
    $label = esc_html__('Quantity', 'woocommerce');
    echo '<div class="quantity">';
    do_action('woocommerce_before_quantity_input_field');

    echo '<label for="' . esc_attr($args['input_id']) . '">' . esc_attr($label) . '</label>';

    $options = '';
    for ($count = $args['min_value']; $count <= $args['max_value']; $count = $count + $args['step']) {
      $options .= '<option value="' . $count . '" ' . selected($args['input_value'], $count, false) . '>' . $count . '</option>';
    }
    // echo '<div class="quantity_select"><select name="' . esc_attr($args['input_name']) . '" title="' . _x('Qty', 'Product quantity input tooltip', 'woocommerce') . '" class="qty">' . $options . '</select></div>';
    echo '<div class="quantity_input"><input type="number" name="' . esc_attr($args['input_name']) . '" title="' . _x('Qty', 'Product quantity input tooltip', 'woocommerce') . '" class="qty" min="' . esc_attr($args['min_value']) . '" max="' . esc_attr($args['max_value']) . '" value="' . $args['input_value'] . '"/></div>';
    do_action('woocommerce_after_quantity_input_field');

    echo '</div>';
  }
  if ($echo) {
    echo ob_get_clean(); // WPCS: XSS ok.
  } else {
    return ob_get_clean();
  }
}

/**
 * Change "Clear" text from reset variation selection
 */

function filter_woocommerce_reset_variations_link($a_class_reset_variations_href_esc_html_clear_evolve_a) {
  $reset_link = '<a class="reset_variations" href="javascript:">&times;</a>';
  $sizing_guide = '</td><td class="sizing-guide"><button id="sizing-guide-link" type="button" class="sizing-guide__link"><span class="h5">Sizing Guide</span></button>';
  return $reset_link . $sizing_guide;
};

add_filter('woocommerce_reset_variations_link', 'filter_woocommerce_reset_variations_link', 10, 1);

/**
 * Convert comment submit button to a <button>
 */
add_filter('comment_form_defaults', function ($defaults) {
  // Edit this to your needs:
  $button = '<button name="%1$s" type="submit" id="%2$s" class="%3$s button" value="%4$s">%4$s</button>';

  // Override the default submit button:
  $defaults['submit_button'] = $button;

  return $defaults;
});

/**
 * WooCommerce Breadcrumb
 */

add_filter('woocommerce_breadcrumb_defaults', 'lb_woocommerce_breadcrumb');
function lb_woocommerce_breadcrumb($defaults) {
  $defaults['delimiter'] = ' - ';
  $defaults['before'] = '<span>';
  $defaults['after'] = '</span>';
  $defaults['home'] = '';

  if (is_product() || is_cart() || is_checkout()) {
    $defaults['home'] = _x('Bookstore', 'breadcrumb', 'beardbalm');
  }
  return $defaults;
}

add_filter('woocommerce_breadcrumb_home_url', 'lb_shop_breadcrumb_home_url');
function lb_shop_breadcrumb_home_url() {
  return wc_get_page_permalink('shop');
}

/**
 * WooCommerce Sale! Text
 */

/**
 * Helper function to get discount percentage of a product
 * 
 * @param WC_Product $product
 * @return string discount percentage
 */

function lb_get_discount_percentage($product) {
  if (!$product) return;
  if (!$product->is_on_sale()) return;

  // Get product prices
  $regular_price = (float) $product->get_regular_price(); // Regular price
  $sale_price = (float) $product->get_price(); // Active price (the "Sale price" when on-sale)

  // "Saving Percentage" calculation and formatting
  $precision = 0; // Max number of decimals
  $percentage = '-' . round(100 - ($sale_price / $regular_price * 100), $precision) . '%';

  return $percentage;
}

/**
 * Is Product New?
 * 
 * @param WC_Product $product
 * @return boolean
 */

function lb_is_product_new($product) {
  if (!$product) return;
  $is_new = false;

  $post_timestamp = new DateTime(get_post($product->id)->post_date);
  $today_timestamp = new DateTime();
  $interval = $post_timestamp->diff($today_timestamp);
  $interval_days = $interval->days;

  if ($interval_days < 60) {
    $is_new = true;
  }

  return $is_new;
}


function lb_custom_sale_flash() {
  global $product;
  $percentage = lb_get_discount_percentage($product);
  $is_new = lb_is_product_new($product);

  $callout = '<div class="product__callout">';
  $callout .= $percentage ? '<span class="onsale">' . $percentage . '</span>' : '';
  $callout .= $is_new ? '<span class="new">New</span>' : '';
  $callout .= '</div>';

  return $callout;
}

add_filter('woocommerce_sale_flash', 'lb_custom_sale_flash');

/**
 * Adjust WC String
 */
add_filter('gettext', 'lb_woocommerce_strings_edit', 20, 3);
function lb_woocommerce_strings_edit($translation, $text, $domain) {

  if ((strpos($domain, 'woocommerce') || strpos($domain, 'wc'))) {
    return $translation;
  }

  $custom_text = [
    'Coupon:' => 'Coupon Code',
    'Coupon code' => 'Enter Code',
    'Apply coupon' => 'Apply',
    '[Order #%s]' => 'Order #%s',
  ];

  // If we don't have replacement text in our array, return the original (translated) text.
  if (empty($custom_text[$translation])) {
    return $translation;
  }

  return $custom_text[$translation];
}

/**
 * Checkout
 */

add_filter('woocommerce_checkout_fields', 'lb_override_billing_checkout_fields', 20, 1);
function lb_override_billing_checkout_fields($fields) {
  $cats = ['shipping', 'billing'];
  foreach ($cats as $cat) :
    $fields[$cat][$cat . '_first_name']['placeholder'] = __('First Name', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_last_name']['placeholder'] = __('Last Name', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_company']['placeholder'] = __('Company Name (optional)', 'woocommerce');
    $fields[$cat][$cat . '_address_1']['placeholder'] = __('Street Address (house number, street address)', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_address_2']['placeholder'] = __('Street Address (apartment, suite, unit, etc. - optional)', 'woocommerce');
    $fields[$cat][$cat . '_city']['placeholder'] = __('Town/City', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_postcode']['placeholder'] = __('Postal Code / ZIP', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_phone']['placeholder'] = __('Phone Number', 'woocommerce') . ' *';
    $fields[$cat][$cat . '_email']['placeholder'] = __('Email', 'woocommerce') . ' *';

    $fields[$cat][$cat . '_country']['priority'] = 75;
  // $fields[$cat][$cat.'_city']['class'][] = 'form-row-first';
  // $fields[$cat][$cat.'_state']['class'][] = 'form-row-last';
  endforeach;
  return $fields;
}

/**
 * Stripe Payment Field
 */

function lb_modify_stripe_fields_styles($styles) {
  return array(
    'base' => array(
      'iconColor'     => '#707070',
      'color'         => '#272727',
      'fontSize'      => '16px',
      'fontWeight'    => '400',
      'fontFamily'    => 'Lato, Open Sans, Segoe UI, sans-serif',
      '::placeholder' => array(
        'color' => '#555152',
      ),
    ),
    'invalid' => array(
      'color' => '#ee3b41'
    )
  );
}

add_filter('wc_stripe_elements_styling', 'lb_modify_stripe_fields_styles');

// function lb_change_product_price_display($price, $product) {
//   // print_r(is_product());
//   if (is_product()) {
//     return $price;
//   }

//   // $price is outputted by Tiered Pricing plugin. Save this onto a different variable
//   $range = $price;

//   // Default Woocommerce formatting
//   if ('' === $product->get_price()) {
//     $price = apply_filters('woocommerce_empty_price_html', '', $product);
//   } elseif ($product->is_on_sale()) {
//     $price = wc_format_sale_price(wc_get_price_to_display($product, array('price' => $product->get_regular_price())), wc_get_price_to_display($product)) . $product->get_price_suffix();
//   } else {
//     $price = wc_price(wc_get_price_to_display($product)) . $product->get_price_suffix();
//   }

//   // Show only the lowest price from $range
//   $range = $range && count(explode(' - ', $range)) ? '<br /><span class="lowest">As low as ' . explode(' - ', $range)[0] . '</span>' : $range;

//   return $price . $range;
// }
// add_filter('woocommerce_get_price_html', 'lb_change_product_price_display', 1000, 2); // The plugin has priority of 999 !
// add_filter('woocommerce_cart_item_price', 'lb_change_product_price_display');

/**
 * Cart - Shipping Details title
 */
add_action('woocommerce_checkout_shipping', 'lb_woocommerce_checkout_shipping', 5);
function lb_woocommerce_checkout_shipping() {
  echo '<h3>Shipping Details</h3>';
}

/**
 * Review Form Placeholders
 */
function lb_review_form($review_form) {
  $review_form['fields']['author'] = str_replace('name="author"', 'placeholder="' . esc_html__('Name', 'woocommerce') . ' *" name="author"', $review_form['fields']['author']);
  $review_form['fields']['email'] = str_replace('name="email"', 'placeholder="' . esc_html__('Email', 'woocommerce') . ' *" name="email"', $review_form['fields']['email']);
  $review_form['comment_field'] = str_replace('required></textarea>', 'placeholder="' . esc_html__('Your review', 'woocommerce') . ' *" required></textarea>', $review_form['comment_field']);
  return $review_form;
}
add_filter('woocommerce_product_review_comment_form_args', 'lb_review_form');

/**
 * Star Ratings
 */

remove_action('woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10);
add_action('woocommerce_review_before_comment_text', 'woocommerce_review_display_rating', 10);
add_filter('woocommerce_get_star_rating_html', 'lb_woocommerce_get_star_rating_html', 15, 3);
function lb_woocommerce_get_star_rating_html($html, $rating, $count) {
  $html = '<p class="stars">';
  $html .= '<span>';
  for ($i = 1; $i <= 5; $i++) :
    $class = 'star-' . $i;
    if ($i <= $rating) :
      $class .= ' filled';
    endif;
    $html .= '<span class="' . $class . '">' . $i . '</span>';
  endfor;
  $html .= '</span>';
  $html .= '</p>';
  return $html;
}

/**
 * Single Product - By Author
 */

function lb_single_product_author() {
  if (!has_term('', 'product_author') && !get_field('byline')) {
    return;
  };


  echo '<p class="product-author">';
  if (get_field('byline')) {
    the_field('byline');
  } else {
    echo get_the_term_list(get_the_ID(), 'product_author', 'By ', ', ');
  }
  echo '</p>';
}
add_action('woocommerce_single_product_summary', 'lb_single_product_author', 7);

/**
 * Add CSS to WooCommerce Emails
 */

add_filter('woocommerce_email_styles', 'lb_add_css_to_emails', 20, 2);

function lb_add_css_to_emails($css, $email) {
  $css .= '
    h1, h2, p, td, .td, .address {
      font-family: "DM Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    h1, h2 {
      font-weight: 600;
      text-align: left;
      color: #000;
      text-shadow: none;
    }
    h1 {
      font-size: 21px;
      font-weight: 600;
    }
    h2 {
      font-size: 21px;
      margin: 0 0 10px;
    }
    b {
      font-weight: 600
    }
    p {
      line-height: 24px;
      text-align: left;
      margin: 0 0 24px;
      color: #000;
      font-size: 16px;
    }
    #template_header_image {
      width: 600px;
      height: 100px;
    }
    #template_header_image p {
      margin-bottom: 0;
    }
    #template_container {
      position: relative;
      border: 0;
      box-shadow: none !important;
    }
    #template_header {
      background: #fff;
    }
    #template_header h1 {
      color: #000;
    }
    #header_wrapper {
      padding-bottom: 0;
    }
    #body_content_outer {
      padding-top: 20px !important;
    }
    #body_content_inner > h2 {
      font-size: 16px;
      margin-top: 40px;
      margin-bottom: 20px;
    }
    #body_content_inner > blockquote {
      padding: 16px;
      margin: 0;
      background-color: #e2f1f6;
      margin-bottom: 16px;
    }
    #body_content_inner > blockquote > p:last-child {
      margin-bottom: 0 !important;
    }
    table {
      border-collapse: collapse;
    }
    .td, 
    .td p, 
    .address,
    #addresses h2 {
      font-size: 14px;
      line-height: 20px;
      font-style: normal;
      color: #000;
    }
    .td,
    #addresses td {
      border:1px solid #7c97bb !important;
    }
    #addresses:last-child {
      margin-bottom: 0 !important;
    }
    th.td {
      width: 33.3%;
    }
    th.td,
    #addresses h2 {
      font-weight: 600;
    }
    tfoot tr:first-child .td {
      border-top: 0 !important;
    }

    #addresses h2, 
    #addresses .address {
      border: 0;
      padding: 12px;
    }
    #addresses h2 {
      margin-bottom: 0;
      border-bottom: 1px solid #7c97bb !important;
    }
    a {
      color: #0054A4;
      font-weight: 600;
      text-decoration: none;
    }
    #subscribe_cta td {
      padding-right: 0 !important;
      padding-left: 0 !important;
    }
    #footer p {
      text-align: center;
      font-size: 14px;
    }
    #footer p  a {
      font-weight: normal;
    }
    #footer img {
      margin-right: 0;
    }
  ';
  return $css;
}

/**
 * Pay Button
 */
// function lb_woocommerce_pay_order_button_html($html) {
//   $order_button_text = __('Place order', 'woocommerce');
//   return '<button type="submit" class="button" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '"><span></span>' . esc_html($order_button_text) . '</button>';
// }
// // add_filter('woocommerce_pay_order_button_html', 'lb_woocommerce_pay_order_button_html', 40);
// add_filter('woocommerce_order_button_html', 'lb_woocommerce_pay_order_button_html', 40);
