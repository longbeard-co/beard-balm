<?php

/**
 * The Events Calendar - Categories
 * 
 * Injects the list of categories to the calendar template
 */

// ! Comment out on prod
add_filter('tribe_events_views_v2_should_cache_html', '__return_false');

add_filter('tribe_events_views_v2_show_latest_past_events_view', '__return_false');

add_action('tribe_template_after_include:events/v2/components/events-bar', function ($file, $name, $template) {
  $categories = get_terms([
    'taxonomy' => 'tribe_events_cat',
    'hide_empty' => false
  ]);

  $current_category_id = null;
  if (get_query_var('taxonomy') == 'tribe_events_cat' && get_query_var('term_id')) :
    $current_category_id = get_query_var('term_id');
  elseif (get_query_var('tax_query')) :
    if (isset(get_query_var('tax_query')['tribe_events_cat_term_id_in']) && isset(get_query_var('tax_query')['tribe_events_cat_term_id_in']['terms'])) :
      $current_category_id = get_query_var('tax_query')['tribe_events_cat_term_id_in']['terms'][0];
    endif;
  endif;

  $output = '';
  if ($categories && !is_wp_error($categories)) :
    $output .= '<div class="tribe-category-filter-wrapper">';
    $output .= '<ul class="tribe-category-filter secondary-nav">';

    // All
    $class = !$current_category_id ? ' current_page_item' : '';
    $aria_current = !$current_category_id ? ' aria-current="page"' : '';
    $output .= '<li class="tribe-category-filter__cat' . $class . '"><a href="' . esc_url(tribe_get_events_link()) . '" class=""' . $aria_current . '>All</a></li>';

    foreach ($categories as $category) :
      $parent = $category->parent;
      if ($parent != 0) continue;

      $subterms = get_terms([
        'taxonomy' => 'tribe_events_cat',
        'parent'   => $category->term_id,
        'hide_empty' => false,
      ]);

      $link = get_term_link($category, 'tribe_events_cat');
      // $icon = get_the_svg('tec-' . $category->slug);
      $this_cat_parent = $current_category_id ? get_term($current_category_id, 'tribe_events_cat')->parent : null;
      $class = $category->term_id == $current_category_id || $category->term_id == $this_cat_parent ? ' current_page_item' : '';
      $class .= $subterms && !is_wp_error($subterms) ? ' has-children' : '';
      $aria_current = $category->term_id == $current_category_id || $category->term_id == $this_cat_parent ? ' aria-current="page"' : '';
      $output .= '<li class="tribe-category-filter__cat tribe-category-filter__cat--' . $category->slug . $class . '"><a href="' . $link . '" class="button--icon"' . $aria_current . '>' . '<span>' . $category->name . '</span></a>';

      while ($subterms && !is_wp_error($subterms)) :
        $output .= '<ul class="subfilters bg-blur">';
        foreach ($subterms as $subterm) :
          $link = get_term_link($subterm, 'tribe_events_cat');
          // $icon = get_the_svg('tec-' . $subterm->slug);
          $class = $subterm->term_id == $current_category_id ? ' current_page_item' : '';
          $aria_current = $subterm->term_id == $current_category_id ? ' aria-current="page"' : '';
          $output .= '<li class="tribe-category-filter__cat tribe-category-filter__cat--' . $subterm->slug . $class . '"><a href="' . $link . '" ' . $aria_current . '>' . $subterm->name . '</a></li>';
        endforeach;
        $output .= '</ul>';

        $subterms = get_terms([
          'taxonomy' => 'tribe_events_cat',
          'parent'   => $subterm->term_id,
          'hide_empty' => false
        ]);
      endwhile;

      $output .= '</li>';
    endforeach;
    $output .= '</ul>';
    $output .= '</div>';
  endif;
  echo $output;
}, 50, 3);

// Changes the text labels for Google Calendar and iCal buttons on a single event page
function lb_tribe_events_ical_single_event_links() {
  $html = '';
  $html .= '<div class="tribe-events-cal-links">';
  $html .= '<div class="tribe-events-cal-links__inner">';
  $html .= '<p>Add this event to my calendar:</p>';
  $html .= '<ul>';
  $html .= '<li><a class="tribe-events-gcal tribe-events-button button" href="' . tribe_get_gcal_link() . '" title="' . __('Add to Google Calendar', 'tribe-events-calendar-pro') . '"><span></span>+ Google Calendar</a></li>';
  $html .= '<li><a class="tribe-events-ical tribe-events-button button" href="' . tribe_get_single_ical_link() . '"><span></span>+ iCal Export</a></li>';
  $html .= '</ul>';
  $html .= '</div>';
  $html .= '</div>';
  return $html;
}

add_filter('tribe_events_ical_single_event_links', 'lb_tribe_events_ical_single_event_links');
