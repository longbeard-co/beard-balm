<?php

/**
 * The template for displaying single Degree Program custom post type
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <?php
    while (have_posts()) :
      the_post();

      $apply_link = get_field('apply_now_link'); ?>

      <section class="container row page-m-t section-m-b--sm">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <div class="entry-breadcrumbs">
            <p class="breadcrumbs">
              <a href="/academics/degree-programs">Degree Programs</a> - <span><?php the_title(); ?></span>
            </p>
          </div>
        </div>
      </section>
      <section class="container row section-m-t--sm section-m-b--sm intro">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <h2><?php the_title(); ?></h2>
          <div class="text-col text-block" data-rm data-rm-xs="2">
            <?php the_content(); 
            if($apply_link) : ?>
            <p class="align-right md-hide"><a href="<?php echo $apply_link; ?>" class="button"><span></span>Apply Now</a></p>
          </div>
          <p class="align-right xs-hide md-show"><a target="_blank" href="<?php echo $apply_link; ?>" class="button"><span></span>Apply Now</a></p>
          <?php endif; ?>
        </div>
      </section>

      <?php
      if (have_rows('courses_groups')) {
      ?>
        <section class="container row section-m-t--sm section-m-b courses">
          <div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <h3>Course Requirements</h3>
            <p>5 Philosophy prerequisites are required for this program.</p>
            <div class="courses__inner">
              <ul>
                <?php
                while (have_rows('courses_groups')) {
                  the_row();
                  $title   = get_sub_field('title');
                  $units   = get_sub_field('units');
                  $desc    = get_sub_field('description');
                  $courses = get_sub_field('courses');
                ?>
                  <li>
                    <div class="courses__course card">
                      <h4><?php echo $title; ?> (<?php echo $units; ?> Unit<?php echo $units > 1 ? 's' : ''; ?>)</h4>
                      <?php if ($desc) { ?>
                        <?php echo $desc; ?>
                      <?php } ?>
                      <?php if (!empty($courses)) { ?>
                        <ol>
                          <?php foreach ($courses as $course) { ?>
                            <li><?php echo $course['title']; ?></li>
                          <?php } ?>
                        </ol>
                      <?php } ?>
                    </div>
                  </li>
                <?php
                }
                ?>
              </ul>
            </div>
          </div>
        </section>

    <?php
      }
    endwhile; // End of the loop.
    ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
