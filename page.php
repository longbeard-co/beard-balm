<?php

/**
 * The default template.
 *
 * This is the template that displays for pages by default.
 *
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div class="container page-m-t section-m-b">
      <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <?php
          while (have_posts()) :
            the_post();

            if (is_cart() || is_checkout()) {
              echo '<div class="woocommerce-page-header"><div class="woocommerce-page-header__inner">';

              echo '<div class="woocommerce-page-header__breadcrumb">';
              echo woocommerce_breadcrumb();
              echo '</div>';

              echo '<div class="woocommerce-page-header__links">';
              echo lb_get_woocommerce_header_links();
              echo '</div>';
              // echo '<div class="woocommerce-back"><a class="button wc-backward" href="' . esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))) . '">Continue Shopping</a></div>';
              echo '</div></div>';
            }

            get_template_part('template-parts/content', 'page');
          endwhile; // End of the loop.
          ?>
        </div>
      </div>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
