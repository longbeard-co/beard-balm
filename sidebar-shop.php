<?php if (!is_active_sidebar('sidebar-shop')) return; ?>

<aside id="secondary" class="widget-area">
  <div class="widget-area__header sm-hide">
    <button id="shop-filter-close-button" type="button" class="widget-area__header__close">&times;</button>
    <h4>Filter</h4>
  </div>
  <div class="widget-area__body">
    <?php
    echo do_shortcode('[searchandfilter id="284"]');
    ?>
  </div>
  <?php // dynamic_sidebar('sidebar-shop'); 
  ?>
</aside>