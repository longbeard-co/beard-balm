<?php

/**
 * Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.1
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

// Define Theme Version
if (!defined('LB_VERSION')) {
  // Replace the version number of the theme on each release.
  define('LB_VERSION', '1.1');
}

define('YOUTUBE_API_KEY', 'AIzaSyCP6LM20FNoW4--rFNFpURjgfQQcDR8Ivg');

/**
 * Change the base path.
 * This is by default WP_CONTENT_DIR.
 *
 * NOTE: Do not include trailing slash.
 */
add_filter('wptt_get_local_fonts_base_path', function ($path) {
  return get_template_directory();
});

/**
 * Change the base URL.
 * This is by default the content_url().
 *
 * NOTE: Do not include trailing slash.
 */
add_filter('wptt_get_local_fonts_base_url', function ($url) {
  return get_template_directory_uri();
});

// Enqueue essential scripts
function beardbalm_scripts() {
  // Cookie Consent
  // wp_enqueue_style( 'cookieconsent', get_template_directory_uri() . '/vendor/cookieconsent/cookieconsent.min.css', array(), '' );
  // wp_enqueue_script( 'cookieconsent', get_template_directory_uri() . '/vendor/cookieconsent/cookieconsent.min.js', array(), '', true );
  // wp_enqueue_script( 'beardbalm-cookieconsent', get_template_directory_uri() . '/js/cookieconsent.js', array(), LB_VERSION, true );

  if (is_singular('post')) {
    wp_enqueue_style('lightgallery', get_template_directory_uri() . '/vendor/lightgallery/css/lightgallery.css', array(), '', 'all');
    wp_enqueue_script('lightgallery', get_template_directory_uri() . '/vendor/lightgallery/lightgallery.min.js', array(), '', false);
    wp_enqueue_style('lightgallery-thumbnail', get_template_directory_uri() . '/vendor/lightgallery/css/lg-thumbnail.css', array(), '', 'all');
    wp_enqueue_script('lightgallery-thumbnail', get_template_directory_uri() . '/vendor/lightgallery/plugins/thumbnail/lg-thumbnail.min.js', array('lightgallery'), '', true);
  }

  if (is_page_template('page-templates/research.php') || is_singular('post') || is_singular('staff') || is_singular('product')) {
    wp_enqueue_style('lity', get_template_directory_uri() . '/vendor/lity/dist/lity.min.css', array(), '', 'all');
    wp_enqueue_script('lity', get_template_directory_uri() . '/vendor/lity/dist/lity.min.js', array('jquery'), '', true);
  }

  // Theme styles and JS
  $stylePath = __DIR__ . '/style.css';
  $filetime = filemtime($stylePath);
  wp_enqueue_style('beardbalm-style', get_stylesheet_uri(), array(), $filetime);

  $jsPath = __DIR__ . '/js/theme.js';
  $filetimeJS = filemtime($jsPath);
  wp_enqueue_script('beardbalm-js', get_template_directory_uri() . '/js/theme.js', array('jquery'), $filetimeJS, true);

  // Web font
  // Include the file.
  require_once get_theme_file_path('inc/wptt-webfont-loader.php');
  // Load the webfont.
  wp_enqueue_style(
    'google-fonts',
    wptt_get_webfont_url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600;700&family=Yeseva+One&display=swap'),
    array(),
    '1.0'
  );

  // Helper scripts from Underscores
  wp_enqueue_script('beardbalm-navigation', get_template_directory_uri() . '/js/navigation.js', array(), LB_VERSION, true);
  wp_enqueue_script('beardbalm-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), LB_VERSION, true);

  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }



  wp_localize_script('beardbalm-js', 'siteData', array(
    'nonce' => wp_create_nonce('lb-nonce'),
    'siteUrl' => get_site_url(),
    'ajaxUrl' => admin_url('admin-ajax.php'),
  ));
}
add_action('wp_enqueue_scripts', 'beardbalm_scripts');

/**
 * Remove the migrate script from the list of jQuery dependencies.
 *
 * @param WP_Scripts $scripts WP_Scripts scripts object. Passed by reference.
 */
function lb_dequeue_jquery_migrate($scripts) {
  if (!is_admin() && !empty($scripts->registered['jquery'])) {
    $jquery_dependencies = $scripts->registered['jquery']->deps;
    $scripts->registered['jquery']->deps = array_diff($jquery_dependencies, array('jquery-migrate'));
  }
}
add_action('wp_default_scripts', 'lb_dequeue_jquery_migrate');


// Get custom post types
require_once('lb-cpt.php');

// Get custom widgets for use with SOPB
require_once('lb-widgets.php');

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Functions which enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

// Customizer additions.
// require get_template_directory() . '/inc/customizer.php';

// Relevanssi Helpers
require get_template_directory() . '/inc/relevanssi.php';

// TEC Helpers
require get_template_directory() . '/inc/events-calendar.php';

// WooCommerce
require get_template_directory() . '/inc/woocommerce.php';

/**
 * Register Menu
 */
register_nav_menus(
  array(
    'menu-1' => esc_html__('Primary', 'beardbalm'),
    'menu-2' => esc_html__('Secondary', 'beardbalm'),
    'menu-mobile' => esc_html__('Mobile Nav (Header)', 'beardbalm'),
    'menu-footer-1' => esc_html__('Footer - About', 'beardbalm'),
    'menu-footer-2' => esc_html__('Footer - Academics', 'beardbalm'),
    'menu-footer-3' => esc_html__('Footer - Resources', 'beardbalm'),
  )
);


/**
 * Remove Customize from Admin Bar
 */

function lb_before_admin_bar_render() {
  global $wp_admin_bar;

  $wp_admin_bar->remove_menu('customize');
}

add_action('wp_before_admin_bar_render', 'lb_before_admin_bar_render');


if (!function_exists('beardbalm_setup')) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function beardbalm_setup() {
    // Enable support for Post Thumbnails on posts and pages.
    // @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    add_theme_support('post-thumbnails');

    /**
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support('title-tag');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    /**
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support(
      'html5',
      array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'style',
        'script',
      )
    );

    /**
     * WooCommerce Theme Support
     */
    add_theme_support(
      'woocommerce',
      array(
        'thumbnail_image_width' => 516,
        'single_image_width'    => 800,
        'product_grid'          => array(
          'default_rows'    => 3,
          'min_rows'        => 1,
          'default_columns' => 3,
          'min_columns'     => 1,
          'max_columns'     => 6,
        ),
      )
    );
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');
  }
endif;
add_action('after_setup_theme', 'beardbalm_setup');

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function beardbalm_widgets_init() {
  register_sidebar(
    array(
      'name'          => esc_html__('Sidebar', 'beardbalm'),
      'id'            => 'sidebar-1',
      'description'   => esc_html__('Add widgets here.', 'beardbalm'),
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title">',
      'after_title'   => '</h2>',
    )
  );

  register_sidebar(
    array(
      'name'          => esc_html__('Shop', 'beardbalm'),
      'id'            => 'sidebar-shop',
      'description'   => esc_html__('Add widgets here.', 'beardbalm'),
      'before_widget' => '<section id="%1$s" class="widget %2$s">',
      'after_widget'  => '</section>',
      'before_title'  => '<h2 class="widget-title h4">',
      'after_title'   => '</h2>',
    )
  );
}
add_action('widgets_init', 'beardbalm_widgets_init');

/**
 * Init Theme's ACF
 */

function lb_acf_init() {
  acf_add_options_page(array(
    'page_title'   => 'Theme General Settings',
    'menu_title'  => 'Theme Settings',
    'menu_slug'   => 'theme-general-settings',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));

  acf_add_options_sub_page(array(
    'page_title'   => 'Theme Header Settings',
    'menu_title'  => 'Header',
    'parent_slug'  => 'theme-general-settings',
  ));

  acf_add_options_sub_page(array(
    'page_title'   => 'Theme Footer Settings',
    'menu_title'  => 'Footer',
    'parent_slug'  => 'theme-general-settings',
  ));
}

add_action('acf/init', 'lb_acf_init');

/**
 * Change Login URL from WP URl to this site's url
 */
function lb_login_logo_url() {
  return home_url('/');
}
add_filter('login_headerurl', 'lb_login_logo_url');

/**
 * Change Login URL from WP URl to this site's url
 */
function lb_login_logo_url_title() {
  return 'Longbeard';
}
add_filter('login_headertext', 'lb_login_logo_url_title');

/**
 * Enqueue Login CSS
 */
function lb_login_stylesheet() {
  $filetime = filemtime(__DIR__ . '/login.css');
  wp_enqueue_style('lb-login', get_template_directory_uri() . '/login.css', array(), $filetime);
}
add_action('login_enqueue_scripts', 'lb_login_stylesheet');

function lb_register_query_vars($vars) {
  $vars[] = 'search';
  return $vars;
}
// add_filter('query_vars', 'lb_register_query_vars');

/**
 * Create standardized, themed search form
 */

function lb_get_search_form($id = 'searchform', $class = '', $action = '/', $placeholder = 'Search', $name = 's', $value = '') {
  $form = '<form role="search" method="get" id="' . $id . '" class="searchform ' . $class . '" action="' . $action . '" >
    <div class="searchform__inner"><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input type="search" value="' . $value . '" placeholder="' . $placeholder . '" name="' . $name . '" id="' . $id . '-input" />
    <button type="submit" id="' . $id . '-submit">' . get_the_svg('search') . '</button>
    </div>
  </form>';
  return $form;
}

/**
 * Filters WordPress' get_search_form()
 * 
 * @param string  $form   Form HTML
 * @return string $form   Form HTML
 * 
 * @link https://developer.wordpress.org/reference/functions/get_search_form/
 */

function lb_my_search_form($form) {
  $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url('/') . '" >
    <div class="searchform__inner"><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input type="search" value="" placeholder="Search..." name="s" id="s" />
    <button type="submit" id="searchsubmit">' . get_the_svg('search') . '</button>
    </div>
  </form>';

  return $form;
}
add_filter('get_search_form', 'lb_my_search_form');


/**
 * Add Video Tutorials link to WP Admin
 */

function lb_videotutorials_admin_menu() {
  global $submenu;
  $url = '/video-tutorials';
  $submenu['index.php'][] = array('Video Tutorials', 'manage_options', $url);
}

// add_action('admin_menu', 'videotutorials_admin_menu');

/**
 * Gravity Forms
 * Filters the next, previous and submit buttons.
 * Replaces the forms <input> buttons with <button> while maintaining attributes from original <input>.
 *
 * @param string $button Contains the <input> tag to be filtered.
 * @param object $form Contains all the properties of the current form.
 *
 * @return string The filtered button.
 */

function lb_gform_input_to_button($button, $form) {

  $dom = new DOMDocument();
  $dom->loadHTML('<?xml encoding="utf-8" ?>' . $button);
  $input = $dom->getElementsByTagName('input')->item(0);
  $new_button = $dom->createElement('button');

  $span = $dom->createElement('span');
  $new_button->appendChild($span);
  $new_button->appendChild($dom->createTextNode($input->getAttribute('value')));

  $input->removeAttribute('value');
  foreach ($input->attributes as $attribute) {
    $new_button->setAttribute($attribute->name, $attribute->value);
  }

  $input->parentNode->replaceChild($new_button, $input);

  return $dom->saveHtml($new_button);
}

add_filter('gform_next_button', 'lb_gform_input_to_button', 10, 2);
add_filter('gform_previous_button', 'lb_gform_input_to_button', 10, 2);
add_filter('gform_submit_button', 'lb_gform_input_to_button', 10, 2);

/**
 * Gravity Forms
 * Pre-populate event registration form Event choices
 */

function lb_populate_event_registration_form_events($form) {

  foreach ($form['fields'] as &$field) {

    if ($field->inputName != 'event') {
      continue;
    }

    $events = tribe_get_events([
      'posts_per_page' => 10,
      'start_date'     => 'now',
      'meta_key'       => 'event_info_allow_registration',
      'meta_value'     => true
    ]);

    $choices = array();

    foreach ($events as $event) {
      $choices[] = array('text' => $event->post_title, 'value' => $event->post_name);
    }

    $field->choices = $choices;
  }

  return $form;
}

add_filter('gform_pre_render_3', 'lb_populate_event_registration_form_events');
add_filter('gform_pre_validation_3', 'lb_populate_event_registration_form_events');
add_filter('gform_pre_submission_filter_3', 'lb_populate_event_registration_form_events');
add_filter('gform_admin_pre_render_3', 'lb_populate_event_registration_form_events');

/**
 * Get custom excerpt from Post Content
 * 
 * @param int $length       Length of excerpt in characters
 * 
 * @return string $excerpt  The Excerpt
 */

function lb_get_excerpt($length) {
  $excerpt = get_the_content();
  // $excerpt = preg_replace(" ([.*?])", '', $excerpt);
  $excerpt = strip_shortcodes($excerpt);
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $length);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
  $excerpt = $excerpt . '...';
  return $excerpt;
}

function lb_custom_excerpt_length($length) {
  return 20;
}
// add_filter('excerpt_length', 'lb_custom_excerpt_length', 99);

// SVG ICON SYSTEM

/**
 * Gets svg file from pre-defined folder
 * Process the svg for accessibility
 *
 * @param string $icon The slug of the SVG file
 * @param string $title (Optional) Title of SVG file for accessibility. Leave blank for decorative SVG
 * @param string $title_id (Optional) Title ID to reference the description. Leave blank to auto-generate ID
 * @param string $desc (Optional) Description of SVG for accessibility.
 *
 * @return string The processed SVG as HTML
 */

function get_the_svg($icon, $title = '', $title_id = '', $desc = '') {
  if (!file_exists(get_template_directory() . '/assets/icons/' . $icon . '.svg')) {
    return false;
  }

  $icon_folder = get_template_directory_uri() . '/assets/icons/';
  $icon_path = $icon_folder . $icon . '.svg';

  if (strpos(get_site_url(), 'longbeardco.com') !== false) {
    $auth = base64_encode("long:beard");
    $context = stream_context_create([
      "http" => [
        "header" => "Authorization: Basic $auth"
      ]
    ]);
    $svg = file_get_contents($icon_path, false, $context);
  } else {
    $svg = file_get_contents($icon_path);
  }

  if (!$svg) :
    return false;
  endif;

  $svgXml = new SimpleXMLElement($svg);

  if ($title) :
    // generate title_id if not defined
    $title_id = $title_id ?: sanitize_title($title) . '-icon';

    if ($svgXml['aria-hidden'] == 'true') :
      $svgXml['aria-hidden'] = 'false';
    endif;

    // Add accessibility attributes
    $svgXml['aria-labelledby'] = $title_id;
    $svgXml['role'] = 'img';

    $svgXml->title = $title;
    $svgXml->title['id'] = $title_id;

    if ($desc) :
      $svgXml->desc = $desc;
    endif;

    return $svgXml->asXml();

  else :
    // Remove icon from Accessibility API
    $svgXml['aria-hidden'] = 'true';

    // Remove unnecessary aria-labelledby attribute
    if (isset($svgXml['aria-labelledby'])) :
      unset($svgXml['aria-labelledby']);
    endif;

    // Remove unnecessary <title> tag
    if (isset($svgXml->title)) :
      unset($svgXml->title);
    endif;

    // Remove semantic meaning
    $svgXml['role'] = 'presentation';

    return $svgXml->asXml();

  endif;
}

/**
 * Echoes svg file from pre-defined folder
 * Process the svg for accessibility
 *
 * @param string $icon The slug of the SVG file
 * @param string $title (Optional) Title of SVG file for accessibility. Leave blank for decorative SVG
 * @param string $titleId (Optional) Title ID to reference the description. Leave blank to auto-generate ID
 * @param string $desc (Optional) Description of SVG for accessibility.
 *
 * @return void
 */

function the_svg($icon, $title = '', $titleId = '', $desc = '') {
  echo get_the_svg($icon, $title, $titleId, $desc);
}

/**
 * Get absolute path to image file
 *
 * @param string $filename The slug of the SVG file
 *
 * @return string Absolute path to image file
 */
function get_the_theme_image_src($filename) {
  return get_template_directory_uri() . '/assets/images/' . $filename;
}

/**
 * Echoes absolute path to image file
 *
 * @param string $filename The slug of the SVG file
 *
 * @return void
 */
function the_theme_image_src($filename) {
  echo get_the_theme_image_src($filename);
}

function lb_get_tribe_events_datetime() {
  $output = '';
  $event_id = get_the_ID();

  // $datetime_separator = tribe_get_option('dateTimeSeparator', ' @ ');
  $datetime_separator   = '<span class="separator">&nbsp;&nbsp;|&nbsp;&nbsp;</span>';

  $time_format          = get_option('time_format', Tribe__Date_Utils::TIMEFORMAT);
  $time_range_separator = tribe_get_option('timeRangeSeparator', ' - ');
  $show_time_zone       = tribe_get_option('tribe_events_timezones_show_zone', false);
  $time_zone_label      = Tribe__Events__Timezones::get_event_timezone_abbr($event_id);

  $start_datetime = tribe_get_start_date();
  $start_date = tribe_get_start_date(null, false);
  $start_time = tribe_get_start_date(null, false, $time_format);
  // $start_ts = tribe_get_start_date(null, false, Tribe__Date_Utils::DBDATEFORMAT);

  $end_datetime = tribe_get_end_date();
  $end_date = tribe_get_display_end_date(null, false);
  $end_time = tribe_get_end_date(null, false, $time_format);
  // $end_ts = tribe_get_end_date(null, false, Tribe__Date_Utils::DBDATEFORMAT);

  $time_formatted = null;
  if ($start_time == $end_time) {
    $time_formatted = esc_html($start_time);
  } else {
    $time_formatted = esc_html($start_time . $time_range_separator . $end_time);
  }

  /**
   * Returns a formatted time for a single event
   *
   * @var string Formatted time string
   * @var int Event post id
   */
  $time_formatted = apply_filters('tribe_events_single_event_time_formatted', $time_formatted, $event_id);

  if (tribe_event_is_all_day() && tribe_event_is_multiday()) :
    $output .= esc_html($start_date) . ' – ' . esc_html($end_date);
  elseif (tribe_event_is_all_day()) :
    $output .= esc_html($start_date);
  elseif (tribe_event_is_multiday()) :
    $output .= esc_html($start_datetime) . ' – ' . esc_html($end_datetime);;
    if ($show_time_zone) :
      $output .= esc_html($time_zone_label);
    endif;
  else :
    $output .= esc_html($start_date) . $datetime_separator . $time_formatted;
    if ($show_time_zone) :
      $output .= esc_html($time_zone_label);
    endif;
  endif;

  return $output;
}

/**
 * Phone to URL helper
 * 
 * @param string $number Phone number
 */
function phone_to_url($number) {
  if (!$number) return false;
  $number = preg_replace('/(extension|x|#|-|code|ext)[.]/', ',', $number);
  $number = preg_replace('/\s+/', '', $number);
  $href = 'tel:' . preg_replace('/[^0-9+-,]/', '', $number);
  return $href;
}

function lb_load_template_part($template_name, $part_name = null, $args) {
  ob_start();
  get_template_part($template_name, $part_name, $args);
  $var = ob_get_contents();
  ob_end_clean();
  return $var;
}

/**
 */

function lb_logos_articles() {
  header("Content-Type: text/html");

  $paged    = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $articles = isset($_POST['articles']) ? json_decode(urldecode($_POST['articles']), true) : array();
  $search   = isset($_POST['search']) ? $_POST['search'] : '';
  get_template_part('template-parts/logos-articles', '', ['search' => $search, 'paged' => $paged, 'articles' => $articles]);

  die();
}

add_action('wp_ajax_nopriv_lb_logos_articles', 'lb_logos_articles');
add_action('wp_ajax_lb_logos_articles', 'lb_logos_articles');

/**
 * Media Posts
 */
function lb_media_posts() {
  header("Content-Type: text/html");

  $paged    = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $search   = isset($_POST['search']) ? $_POST['search'] : '';
  $category = isset($_POST['category']) ? $_POST['category'] : '';
  $year     = isset($_POST['year']) ? $_POST['year'] : '';
  $month    = isset($_POST['month']) ? $_POST['month'] : '';

  get_template_part('template-parts/media-posts-list', '', ['search' => $search, 'category' => $category, 'paged' => $paged, 'year' => $year, 'month' => $month]);

  die();
}

add_action('wp_ajax_nopriv_lb_media_posts', 'lb_media_posts');
add_action('wp_ajax_lb_media_posts', 'lb_media_posts');

/**
 * Research Posts
 */
function lb_research_posts() {
  header("Content-Type: text/html");

  $paged    = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $search   = isset($_POST['search']) ? $_POST['search'] : '';
  $category = isset($_POST['category']) ? $_POST['category'] : '';

  get_template_part('template-parts/research-posts-list', '', ['search' => $search, 'category' => $category, 'paged' => $paged]);

  die();
}

add_action('wp_ajax_nopriv_lb_research_posts', 'lb_research_posts');
add_action('wp_ajax_lb_research_posts', 'lb_research_posts');

/**
 * Staff Posts
 */
function lb_staff_posts() {
  header("Content-Type: text/html");

  $paged    = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $search   = isset($_POST['search']) ? $_POST['search'] : '';
  $category = isset($_POST['category']) ? $_POST['category'] : '';

  get_template_part('template-parts/faculty-staff-list', '', ['search' => $search, 'category' => $category, 'paged' => $paged]);

  die();
}

add_action('wp_ajax_nopriv_lb_staff_posts', 'lb_staff_posts');
add_action('wp_ajax_lb_staff_posts', 'lb_staff_posts');

/**
 * YouTube playlist
 */
function lb_youtube_playlist() {
  header("Content-Type: text/html");

  $playlist_id    = isset($_POST['playlist_id']) && $_POST['playlist_id'] ? $_POST['playlist_id'] : '';
  $video_id       = isset($_POST['video_id']) && $_POST['video_id'] ? $_POST['video_id'] : '';
  $page_token     = isset($_POST['page_token']) && $_POST['page_token'] ? $_POST['page_token'] : '';

  get_template_part('template-parts/youtube-videos', '', ['playlist_id' => $playlist_id, 'video_id' => $video_id, 'page_token' => $page_token]);

  die();
}

add_action('wp_ajax_nopriv_lb_youtube_playlist', 'lb_youtube_playlist');
add_action('wp_ajax_lb_youtube_playlist', 'lb_youtube_playlist');

/**
 * Gallery
 */
function lb_gallery() {
  header("Content-Type: text/html");

  $paged      = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $field      = isset($_POST['field']) ? $_POST['field'] : array();
  $post_id    = isset($_POST['post_id']) ? $_POST['post_id'] : array();

  get_template_part('template-parts/gallery', '', array(
    'paged'        => $paged,
    'gallery_data' => array(
      'field'   => $field,
      'post_id' => $post_id,
    )
  ));

  die();
}

add_action('wp_ajax_nopriv_lb_gallery', 'lb_gallery');
add_action('wp_ajax_lb_gallery', 'lb_gallery');

/**
 * Publishe Works
 */
function lb_published_works() {
  header("Content-Type: text/html");

  $paged      = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $field      = isset($_POST['field']) ? $_POST['field'] : array();
  $post_id    = isset($_POST['post_id']) ? $_POST['post_id'] : array();

  get_template_part('template-parts/published-works-list', '', array(
    'paged'   => $paged,
    'field'   => $field,
    'post_id' => $post_id,
  ));

  die();
}

add_action('wp_ajax_nopriv_lb_published_works', 'lb_published_works');
add_action('wp_ajax_lb_published_works', 'lb_published_works');

/**
 * Courses List
 */
function lb_courses_list() {
  header("Content-Type: text/html");

  $paged      = isset($_POST['paged']) ? $_POST['paged'] : 1;
  $semester   = isset($_POST['semester']) ? $_POST['semester'] : '';
  $category   = isset($_POST['category']) ? $_POST['category'] : '';
  $search     = isset($_POST['search']) ? $_POST['search'] : '';

  get_template_part('template-parts/courses-list', '', array(
    'paged'    => $paged,
    'semester' => explode(',', $semester),
    'category' => explode(',', $category),
    'search'   => $search,
  ));

  die();
}

add_action('wp_ajax_nopriv_lb_courses_list', 'lb_courses_list');
add_action('wp_ajax_lb_courses_list', 'lb_courses_list');

/**
 * Body Class
 */

add_filter('body_class', 'lb_body_classes');
function lb_body_classes($classes) {

  if (is_archive('degree_program')) {
    $classes[] = 'page-template-academics-degree-programs';
  }

  if (is_date()) {
    $classes[] = 'page-template-media';
  }

  if (is_page_template('page-templates/bookstore-authors.php')) {
    $classes[] = 'woocommerce-page';
  }

  if (is_post_type_archive('product') || is_tax('product_cat') || is_tax('product_tag')) {
    $classes[] = 'woocommerce-shop';
  }

  return $classes;
}

/**
 * Get post category icon
 * @param string $category_slyg WP Category slug, e.g. 'video'
 */

function lb_get_post_category_icon($category_slug) {
  if (!$category_slug) {
    return;
  }

  $svg = get_the_svg("format-$category_slug");

  if (!$svg) {
    // Fallback
    $svg = get_the_svg('format-news');
  }

  return $svg;
}

/**
 * Allow querying terms by first character
 * 
 * Usage:
 * get_terms( "product_author", array( 'name__like' => $letter ) )
 */
// function lb_name_like_product_author($clauses) {
//   remove_filter('term_clauses', 'lb_name_like_product_author');
//   $pattern = '|(name LIKE )\'%(.+%)\'|';
//   $clauses['where'] = preg_replace($pattern, '$1 \'$2\'', $clauses['where']);
//   return $clauses;
// }
// add_filter('terms_clauses', 'lb_name_like_product_author');

/**
 * Searches article belonging to an author on a product
 */
function lb_product_article_search($article_groups,  $author_id) {
  foreach ($article_groups as $article_group) {
    $articles = $article_group['articles'];
    if (!empty($articles)) {
      foreach ($articles as $article) {
        if (in_array($author_id, $article['author'])) {
          return $article;
        }
      }
    }
  }
  return null;
}

/**
 * @param array $args (optional) Query args
 */

function lb_query_courses($args = array()) {
  $courses  = get_field('courses', 346); // 346 = Academics - Courses page id.

  // query args
  $semester = $args && isset($args['semester']) && !empty($args['semester']) ? $args['semester'] : array();
  $category = $args && isset($args['category']) && !empty($args['category']) ? $args['category'] : array();
  $search   = $args && isset($args['search']) ? $args['search'] : '';
  $paged    = $args && isset($args['paged']) ? intval($args['paged']) : 1;
  $per_page = $args && isset($args['posts_per_page']) ? intval($args['posts_per_page']) : 6;

  // cleanup empty arrays
  $semester = array_filter($semester);
  $category = array_filter($category);

  if (!empty($semester)) {
    $courses = array_filter($courses, function ($course) use ($semester) {
      return in_array($course['semester']['value'], $semester);
    });
  }

  if (!empty($category)) {
    $courses = array_filter($courses, function ($course) use ($category) {
      return !empty(array_intersect($course['category'], $category));
    });
  }

  if (!empty($search)) {
    $courses = array_filter($courses, function ($course) use ($search) {
      return strpos(json_encode($course), $search) !== false;
    });
  }

  $paged_courses = array_slice($courses, ($paged - 1) * $per_page, $per_page);

  $has_next_page = $paged * $per_page < count($courses);
  $has_prev_page = $paged > 1;

  return array(
    'items'         => $paged_courses,
    'has_next_page' => $has_next_page,
    'has_prev_page' => $has_prev_page,
  );
}

function lb_get_person_placeholder_id($type = 'male') {
  if ($type == 'female') {
    return 140;
  }

  return 139;
}

function lb_post_link($url, $post) {
  if ($post->post_type == 'post') {
    $video = get_field('video', $post->ID);
    $youtube_id = $video && isset($video['youtube_id']) ? $video['youtube_id'] : null;
    if ($youtube_id && !get_the_content($post)) {
      $url = "https://www.youtube.com/watch?v=$youtube_id";
    }
  }
  return $url;
}
add_filter('post_link', 'lb_post_link', 10, 2);

// add_action('init', function () {
//   $terms = get_terms(array(
//     'taxonomy' => 'product_author',
//   ));
//   foreach ($terms as $term) {
//     $id = $term->term_id;
//     $name = $term->name;
//     $name_arr = explode(' ', $name);
//     if (!get_field('last_name', 'product_author_' . $id)) {
//       $ln = array_pop($name_arr);
//       // print_r($ln);
//       update_field('last_name', $ln, 'product_author_' . $id);
//     }
//     if (!get_field('first_name', 'product_author_' . $id)) {
//       update_field('first_name', implode(' ', $name_arr), 'product_author_' . $id);
//     }
//   }
// });

/**
 * Prevent WordPress from scaling extremely large images
 * Large images often used as background on VO's website
 */
function lb_big_image_size_threshold($threshold) {
  $current_user = wp_get_current_user();

  if (is_user_logged_in() && ($current_user->user_login === 'kevin-lb' || $current_user->user_login === 'patrick-lb' || $current_user->user_login === 'lbadmin')) {
    return false;
  } else {
    return $threshold;
  }
}
add_filter('big_image_size_threshold', 'lb_big_image_size_threshold');

/**
 * Formats date ACF repeater
 * 
 * @param array $dates Date ACF repeater field
 * @return string Formatted date
 */
function lb_format_custom_date($dates) {
  $dates_arr = array();
  $prev_date = '';

  foreach ($dates as $d) {
    $date = $d['date'];

    if ($date && $prev_date != $date) {
      $dates_arr[] = $date;
      $prev_date = $date;
    }
  }

  // Modify array as necessary
  if (count($dates_arr) == 2) {
    $date_1 = DateTime::createFromFormat('F j, Y', $dates_arr[0]);
    $date_2 = DateTime::createFromFormat('F j, Y', $dates_arr[1]);

    $is_same_year = $date_1->format('Y') == $date_2->format('Y');
    $is_same_month = $date_1->format('M') == $date_2->format('M');

    if ($is_same_year && $is_same_month) {
      $dates_arr[0] = $date_1->format('F j');
      $dates_arr[1] = $date_2->format('j, Y');
    } else if ($is_same_year) {
      $dates_arr[0] = $date_1->format('F j');
    }

    return implode(' – ', $dates_arr);
  } else if (!empty($dates_arr)) {
    return implode(', ', $dates_arr);
  } else {
    global $post;
    return get_the_date('', $post);
  }
}

/**
 * Get either custom date or default post_date
 * Must be used within loop
 * @return string Formatted date
 */
function lb_get_the_date() {
  return get_field('custom_date') ? lb_format_custom_date(get_field('custom_date')) : get_the_date();
}


/**
 * Get post categories
 * Must be used within loop
 * @return string Formatted date
 */
function lb_get_the_category() {
  if (function_exists('yoast_get_primary_term_id')) {
    $primary_category_id = yoast_get_primary_term_id();
    $primary_category = get_category($primary_category_id);
    // print_r($primary_category);
    return '<a href="' . get_category_link($primary_category) . '">' . $primary_category->name . '</a>';
  } else {
    return the_category(', ');
  }
}
