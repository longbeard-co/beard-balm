var $ = jQuery.noConflict();

// Initialize functions
document.addEventListener("DOMContentLoaded", function () {
  console.log("JMJ");
  accessibility();
  headerSearch();
  smoothScroll();
  accordions();
  tabs();
  readMoreInit();
  faqsSearch();
  logosArticles();
  mediaPosts();
  researchPosts();
  facultyPosts();
  searchClear();
  youtubePlaylistPagination();
  gallery();
  publishedWorks();

  searchCourseCheckboxes();
  searchCourseDropdown();
  coursesList();

  horizontalScroll(".lb-tabs:not(.no-scroll) .tabs");
  horizontalScroll("#category-filter");

  shopMobileFilter();
  imagePopups();

  // slickSlider('.some-slider');
  // aosIE();
});

$(document).on("sf:ajaxfinish", ".searchandfilter", function () {
  shopMobileFilter();
});

// Initialize Gravity Forms functions
// $(document).bind('gform_post_render', function () {
//   // Gravity Forms JS
// });

/*********************
DECLARED FUNCTIONS
*********************/

/**
 * Sets up accessibility listeners
 */
function accessibility() {
  // Remove focus from links
  document.body.classList.add("is-mouse");

  // Listen to tab events to enable outlines (accessibility improvement)
  document.body.addEventListener("keyup", function (e) {
    if (e.key === "Tab") {
      document.body.classList.remove("is-mouse");
    }
  });

  // Let the document know when the mouse is being used
  document.body.addEventListener("mousedown", () => {
    document.body.classList.add("is-mouse");
  });

  // Check if this is touch device
  if (deviceHasTouchScreen()) {
    document.body.classList.add("is-touch-device");
  } else {
    document.body.classList.add("not-touch-device");
  }
}

function headerSearch() {
  function initHeaderSearch(wrapper) {
    const trigger = wrapper.querySelector(".header__search__button");
    const searchBox = wrapper.querySelector(".header__search-box");
    const searchResults = wrapper.querySelector(".header__search-box__results");
    const searchForm = wrapper.querySelector("form");
    const searchInput = searchForm.querySelector('input[type="search"]');

    if (!searchResults || !searchForm || !searchInput) {
      return;
    }

    function toggleSearchBox() {
      searchBox.classList.toggle("active");

      if (searchBox.classList.contains("active")) {
        // Focus on the input
        searchInput.focus();
        hideOnClickOutside(searchBox, wrapper);
      } else {
        searchInput.blur();
      }
    }

    function setSkeleton() {
      const skeletonHtml = `<ul class="skeleton">
       <li><h5 class="skeleton-bg">&nbsp;</h5>
        <ul>
         <li class="skeleton-bg">&nbsp;</li>
         <li class="skeleton-bg">&nbsp;</li>
         <li class="skeleton-bg">&nbsp;</li>
        </ul>
       </li>
       <li><h5 class="skeleton-bg">&nbsp;</h5>
        <ul>
         <li class="skeleton-bg">&nbsp;</li>
         <li class="skeleton-bg">&nbsp;</li>
        </ul>
       </li>
      </ul>`;
      // Delete current content
      removeChildren(searchResults);
      // Replace
      searchResults.insertAdjacentHTML("afterbegin", skeletonHtml);
    }

    function processResults(results) {
      let html = "";
      const types = ["Post", "Page"];

      if (results.length) {
        for (let i = 0; i < types.length; i++) {
          // 1. Filter results according to type
          const filteredResults = results.filter(
            (result) => result.type === types[i].toLowerCase()
          );

          if (filteredResults.length > 0) {
            html += `<li><h5 class="post-type">${types[i]}</h5>`;
            html += `<ul>`;
            for (let j = 0; j < filteredResults.length; j++) {
              const { title, link } = filteredResults[j];
              html += `<li><a href="${link}">${title.rendered}</a></li>`;
            }
            html += `</ul></li>`;
          }
        }
      }

      const finalHtml = html
        ? `<ul>${html}</ul>`
        : `<ul><li class="no-results">No results are found.</li></ul>`;

      // Delete current content
      removeChildren(searchResults);
      // Replace
      searchResults.insertAdjacentHTML("afterbegin", finalHtml);
    }

    function fetchSearchResults(query) {
      fetch(`${siteData.siteUrl}/wp-json/relevanssi/v1/search?search=${query}`)
        .then((response) => response.json())
        .then((response) => processResults(response))
        .catch((err) => console.log("Fetch Error:", err));
    }

    const getDebouncedInputValue = debounce(function () {
      if (searchInput.value) {
        fetchSearchResults(searchInput.value);
      } else {
        removeChildren(searchResults);
      }
    }, 500);

    const handleInputKeyup = () => {
      setSkeleton();
      getDebouncedInputValue();
    };

    const handleInputClear = function () {
      removeChildren(searchResults);
    };

    if (trigger) {
      trigger.addEventListener("click", toggleSearchBox);
    }
    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);
  }

  const wrappers = document.querySelectorAll(".header__search");
  for (let i = 0; i < wrappers.length; i++) {
    initHeaderSearch(wrappers[i]);
  }
}

function slickSlider(slider) {
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      /* Initializes a slick carousel only on mobile screens */
      // slick on mobile
      if ($(window).width() > 1025) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: true,
            speed: 400,
            autoplay: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: '<div class="slick-arrow slick-prev">Prev</div>', //replace arrows
            nextArrow: '<div class="slick-arrow slick-next">Next></div>', //replace arrows
            responsive: [
              {
                breakpoint: 601,
                settings: "unslick",
              },
            ],
          });
        }
      }
    });
  });
}

/**
 * Initialize Smooth Scroll
 */
function smoothScroll() {
  // Smooth scroll for direct-to-anchor external links
  // if (window.location.hash) {
  //   $("html, body").animate(
  //     {
  //       scrollTop: $(window.location.hash).offset().top - 100,
  //     },
  //     1000
  //   );
  // }
  // Select all links with hashes, same page
  $('a[href*="#"][data-smooth-scroll]').click(function (event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var hash = this.hash;
      var target = $(hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $("html, body").animate(
          {
            scrollTop:
              target.attr("role") === "tabpanel"
                ? target.parent().offset().top
                : target.offset().top,
          },
          1000,
          function () {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) {
              // Checking if the target was focused
              return false;
            } else {
              // $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
              // $target.focus(); // Set focus again
            }
          }
        );
      }
      if (window.location.hash === hash) {
        $(window).trigger("hashchange");
      } else {
        window.location.hash = hash;
      }
    }
  });
}

/**
 * Initialize Tabs
 */
function tabs() {
  var tabs = $(".lb-tabs"); // tabs container class
  var activeClass = "active";

  tabs.each(function () {
    var container = $(this);
    var tablist = $(this).find('[role="tablist"]')[0]; // Select the first one to avoid subtabs
    var tab = $(this).find('[role="tab"]');
    var tabContent = $(this).find('[role="tabpanel"]');
    var useHash = false;
    var useHashAttr = $(this).attr("data-use-hash");
    if (typeof useHashAttr !== typeof undefined && useHashAttr !== false) {
      useHash = true;
    }

    function isSameTabParent(_, element) {
      return element.closest(".lb-tabs") === container[0];
    }

    // 1. Filter tabs to exclude sub tabs / parent tabs
    tab = tab.filter(isSameTabParent).filter(':not([id*="slick"])');
    tabContent = tabContent.filter(isSameTabParent);

    function setActiveTab(id, focus = true) {
      tab.each(function () {
        if ($(this).attr("aria-controls") === id) {
          // Check if we should activate the parent tab too
          // by using data-parent-tab attribute
          var parentTabId = $(this).attr("data-parent-tab");
          if (parentTabId) {
            setActiveTab(parentTabId);
          }

          $(this).addClass(activeClass).attr({ "aria-selected": true });

          if (focus) {
            $(this).focus();
          }
        } else {
          $(this)
            .removeClass(activeClass)
            .attr({ "aria-selected": false })
            .blur();
        }
      });

      tabContent.each(function () {
        if ($(this).attr("id") === id) {
          $(this).addClass(activeClass).attr({ "aria-expanded": true });
        } else {
          $(this).removeClass(activeClass).attr({ "aria-expanded": false });
        }
      });

      // Dispatch Event
      const event = new Event("changeTab");
      window.dispatchEvent(event);
    }

    function handleTabClick(e) {
      e.preventDefault();

      // 1. Remove pre-selected default tabs classes
      tab.filter(".tab-default").removeClass("tab-default");
      tabContent.filter(".tab-default").removeClass("tab-default");

      // 2. Set active tab
      if (!$(this).hasClass(activeClass)) {
        setActiveTab($(this).attr("aria-controls"));
      }

      // 3. Update URL (if required)
      if (useHash) {
        history.replaceState({}, "", $(this).attr("href"));
      }

      // 4. Slide clicked tab into view
      if (!$(this).hasClass("slick-slide")) {
        // slide to view
        var buffer = 50; // gradient
        var container = $(this).closest(".tabs");
        var frame = container.closest(".overflow-wrapper");
        var offset;

        if (
          $(this).position().left <= buffer ||
          $(this).position().left + $(this).width() >= frame.width() - buffer
        ) {
          if ($(this).position().left > frame.width() / 2) {
            // scroll to right
            offset =
              container.scrollLeft() +
              ($(this).position().left -
                container.width() +
                $(this).width() +
                50);
          } else {
            // scroll to left
            offset = container.scrollLeft() + ($(this).position().left - 50);
          }
        }

        container.animate(
          {
            scrollLeft: offset,
          },
          250
        );
      }
    }

    function handleKeyboardNavigation(e) {
      switch (e.key) {
        case "ArrowLeft": // left arrow
        case "ArrowUp": // up arrow
          e.preventDefault();
          var prev = tab.filter("." + activeClass).prev().length
            ? tab
                .filter("." + activeClass)
                .prev()
                .attr("aria-controls")
            : tab.last().attr("aria-controls");
          setActiveTab(prev);
          break;
        case "ArrowRight": // right arrow
        case "ArrowDown": // down arrow
          e.preventDefault();
          var next = tab.filter("." + activeClass).next().length
            ? tab
                .filter("." + activeClass)
                .next()
                .attr("aria-controls")
            : tab.first().attr("aria-controls");
          setActiveTab(next);
          break;
      }
    }

    // 2. Defaults - activate the first tab
    // 1. Check if default tab should be specified by the URL
    var targetTabID;
    var urlParams = new URLSearchParams(window.location.search);
    if (window.location.hash) {
      var hash = window.location.hash.substring(1);
      if (tab.filter('[aria-controls="' + hash + '"]').length) {
        targetTabID = hash;
        setActiveTab(targetTabID, false);
        // setTimeout(() => {
        //   document.getElementById(hash).closest("section").scrollIntoView();
        // }, 50);
      }
    } else if (
      urlParams?.get("tab") &&
      tab.filter('[aria-controls="' + urlParams.get("tab") + '"]').length
    ) {
      setActiveTab(urlParams.get("tab"), false);
    } else if (!tab.filter("." + activeClass).length) {
      targetTabID = tab.first().attr("aria-controls");
      // 2. Set active the default tab
      setActiveTab(targetTabID, false);
    }

    // 3. Handle click
    tab.on("click", handleTabClick);

    // 4. Handle Keyboard navigation
    $(tablist).on("keyup", handleKeyboardNavigation);

    $(window).on("hashchange", function () {
      if (useHash && window.location.hash) {
        var hash = window.location.hash.substring(1);
        if (tab.filter('[aria-controls="' + hash + '"]').length) {
          setActiveTab(hash);
        }
      }
    });
  });
}

/**
 * Initializes Accordions
 * @param {string} [el = .accordions] - Element selector
 */

function accordions(el = ".accordions") {
  const elements = document.querySelectorAll(el);

  // 1. Loop through all accordion sets (accordions)
  for (let i = 0; i < elements.length; i++) {
    const accordionGroup = elements[i];

    if (accordionGroup.classList.contains("accordions-initialized")) return;

    const allowMultiple = accordionGroup.hasAttribute("data-allow-multiple");
    const accordions = accordionGroup.querySelectorAll(".accordion");

    // 2. Loop through all accordion within the set
    for (let j = 0; j < accordions.length; j++) {
      let accordion = accordions[j];
      const trigger = accordion.querySelector("[aria-controls]");
      if (trigger && !trigger.hasAttribute("aria-expanded"))
        trigger.setAttribute("aria-expanded", "false");

      // 3. Handle click event on the accordion head
      trigger.addEventListener("click", function (e) {
        e.preventDefault();
        const body = accordion.querySelector(
          "#" + e.currentTarget.getAttribute("aria-controls")
        );

        if (!allowMultiple) {
          // 4. Close all other accordions if necessary
          for (let k = 0; k < accordions.length; k++) {
            if (
              accordions[k] !== e.currentTarget.closest(".accordion") &&
              accordions[k].classList.contains("active")
            ) {
              const accordionTrigger =
                accordions[k].querySelector("[aria-controls]");
              const accordionBody =
                accordions[k].querySelector("[aria-labelledby]");
              accordions[k].classList.remove("active");
              accordionTrigger.setAttribute("aria-expanded", "false");
              accordionBody.removeAttribute("role");
              slideUp(accordionBody);
            }
          }
        }

        // 4. Toggle clicked accordion
        accordion.classList.toggle("active");

        if (accordion.classList.contains("active")) {
          slideDown(body);
          trigger.setAttribute("aria-expanded", "true");
          body.setAttribute("role", "region");
        } else {
          slideUp(body);
          trigger.setAttribute("aria-expanded", "false");
          body.removeAttribute("role");
        }
      });
    }

    accordionGroup.classList.add("accordions-initialized");
  }
}

/**
 * Initializes Horizontal scroll
 * @param {string} element - Element selector
 */
function horizontalScroll(element) {
  $(element)
    .each(function () {
      if (!$(this).closest(".overflow-wrapper").length) {
        $(this).wrap('<div class="overflow-wrapper"></div>');
      }
      if (!$(this).hasClass("overflow-ready")) {
        $(this).addClass("overflow-ready--x");
      }
      scrollOverflow($(this));
    })
    .scroll(function () {
      scrollOverflow($(this));
    });

  $(window).on("resize", function () {
    setTimeout(function () {
      $(element).each(function () {
        scrollOverflow($(this));
      });
    }, 500);
  });

  function scrollOverflow(el) {
    var wrap = el.closest(".overflow-wrapper");
    if (el[0].scrollWidth > el[0].clientWidth) {
      // your element have overflow
      wrap.addClass("overflow--x");

      if (el.scrollLeft() > 0) {
        wrap.addClass("overflow--left");
      } else {
        wrap.removeClass("overflow--left");
      }

      if (el.scrollLeft() < el.get(0).scrollWidth - el.outerWidth() - 10) {
        wrap.addClass("overflow--right");
      } else {
        wrap.removeClass("overflow--right");
      }
    } else {
      // your element doesn't have overflow
      wrap.removeClass("overflow--x");
    }
  }
}

/**
 * Read More by Elements
 */
function readMoreInit() {
  const elements = document.querySelectorAll("[data-rm]");
  for (let i = 0; i < elements.length; i++) {
    const el = elements[i];

    const xs = el.getAttribute("data-rm-xs");
    const sm = el.getAttribute("data-rm-sm");
    const md = el.getAttribute("data-rm-md");
    const lg = el.getAttribute("data-rm-lg");
    const moreText = el.getAttribute("data-rm-more") || "Read More";
    const lessText = el.getAttribute("data-rm-less") || "View Less";

    const options = [];

    if (xs) {
      options.push({
        numberToShow: parseInt(xs),
        viewportSize: 767,
      });
    }
    if (sm) {
      options.push({
        numberToShow: parseInt(sm),
        viewportSize: 1024,
      });
    }
    if (md) {
      options.push({
        numberToShow: parseInt(md),
        viewportSize: 1749,
      });
    }
    if (lg) {
      options.push({
        numberToShow: parseInt(lg),
        viewportSize: 9999,
      });
    }

    if (options.length) {
      readMore(el, options, moreText, lessText);
    }
  }
}

function readMore(eleWrapper, arrayOfObjects, readMore, readLess) {
  // sort from smallest to largest
  var sortedSizes = arrayOfObjects.sort(function (a, b) {
    return a.viewportSize < b.viewportSize ? -1 : 1;
  });

  var readMoreLabelInactive = readMore;
  var readMoreLabelActive = readLess;

  // get all the sizes from the object
  var sizes = [];
  sortedSizes.forEach(function (_ref) {
    var numberToShow = _ref.numberToShow,
      viewportSize = _ref.viewportSize;
    sizes.push(viewportSize);
  });

  // each ele so everything is relative to each item we call this on
  window.addEventListener("load", () => readMoreInit(eleWrapper));
  window.addEventListener("resize", () => readMoreInit(eleWrapper));

  function readMoreInit(eleWrapper) {
    var sizeToUse = sizes.filter((num) => window.innerWidth <= num)[0];

    // get the viewport size needed
    if (sizeToUse !== undefined) {
      var numberOfElementsToShow = sortedSizes.find(
        (item) => item.viewportSize == sizeToUse
      );
    }

    // if out of size range and read more is active
    if (!sizeToUse && eleWrapper.classList.contains("active")) {
      deconstructReadMore(eleWrapper);
    }

    // if inside of size range and read more is active
    if (!sizeToUse && eleWrapper.classList.contains("active")) {
      // check to see if current size is same as before or if changed sizes
      if (!eleWrapper.classList.contains(sizeToUse)) {
        deconstructReadMore(eleWrapper);
        constructReadMore(
          eleWrapper,
          numberOfElementsToShow.numberToShow,
          sizeToUse
        );
      }
    }

    // if size is in range and read more isn't active
    if (sizeToUse !== undefined && !eleWrapper.classList.contains("active")) {
      constructReadMore(
        eleWrapper,
        numberOfElementsToShow.numberToShow,
        sizeToUse
      );
    }
  }

  function constructReadMore(eleWrapper, num, className) {
    eleWrapper.classList.add("active");
    eleWrapper.classList.add(className);

    // get set of children
    const children = eleWrapper.children;

    if (children.length <= num) {
      return;
    }

    const childrenArray = Array.from(children);

    // break them up into their own seperate parts
    // TODO: this is still in jQuery
    var shownEles = childrenArray.slice(0, num);
    $(shownEles).wrapAll('<div class="shown-elements"></div>');

    // break them up into their own seperate parts
    var hiddenEles = childrenArray.slice(num);
    $(hiddenEles).wrapAll(
      '<div class="hidden-elements"><div class="hidden-elements-wrapper"></div></div>'
    );
    eleWrapper.querySelector(".hidden-elements").style.display = "none";

    // wrap the rest with the container and add button
    // TODO: this is still in jQuery
    $(eleWrapper).children().wrapAll('<div class="read-more-container"></div>');

    eleWrapper.querySelector(".read-more-container").insertAdjacentHTML(
      "beforeend",
      `<div class="view-more-container end-xs">
        <button type="button" class="button-view-more button-icon">
          <span>${readMoreLabelInactive}</span>
          <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.675 0L5.5 3.7085L9.325 0L10.5 1.1417L5.5 6L0.5 1.1417L1.675 0Z" fill="currentColor"/>
          </svg>
        </button>
      </div>`
    );

    function handleButtonClick(e) {
      e.preventDefault();
      eleWrapper
        .querySelector(".read-more-container")
        .classList.toggle("active");
      slideToggle(eleWrapper.querySelector(".hidden-elements"));
      e.currentTarget.classList.toggle("active");

      if (e.currentTarget.classList.contains("active")) {
        e.currentTarget.querySelector("span").innerText = readMoreLabelActive;
      } else {
        e.currentTarget.querySelector("span").innerText = readMoreLabelInactive;
      }
    }

    eleWrapper
      .querySelector(".view-more-container button")
      .addEventListener("click", handleButtonClick);
  }

  function deconstructReadMore(eleWrapper) {
    // TODO: In jQuery
    $(eleWrapper).find(".hidden-elements-wrapper").children().unwrap();
    $(eleWrapper).find(".hidden-elements").children().unwrap();
    $(eleWrapper).find(".shown-elements").children().unwrap();
    $(eleWrapper).find(".read-more-container").children().unwrap();
    $(eleWrapper).find(".view-more-container").remove();
    sizes.map(function (item) {
      $(eleWrapper).removeClass("" + item);
    });
    $(eleWrapper).removeClass("active");
  }
}

function faqsSearch() {
  const form = document.getElementById("faqs-searchform");
  const input = document.getElementById("faqs-searchinput");
  if (!form || !input) return;

  const wrapper = document.querySelector(".faqs__accordions");

  // Prep for when filter yields no result
  const node = document.createElement("H6");
  const textNode = document.createTextNode(
    "Your search yields no result. Please adjust your search query."
  );
  node.appendChild(textNode);
  node.classList.add("no-results");
  node.style.display = "none";
  wrapper.appendChild(node);

  let timer = null;

  input.addEventListener("keyup", function () {
    const filter = input.value.toUpperCase();
    clearTimeout(timer);
    timer = setTimeout(function () {
      filterTexts(filter);
    }, 250);
  });

  input.addEventListener("search", function () {
    const filter = input.value.toUpperCase();
    filterTexts(filter);
  });

  form.addEventListener("submit", function (e) {
    e.preventDefault();
    const filter = input.value.toUpperCase();
    filterTexts(filter);
  });

  function filterTexts(filter) {
    const faqs = wrapper.querySelectorAll(".accordion");
    let visibleCount = 0;

    for (let i = 0; i < faqs.length; i++) {
      const faq = faqs[i];
      const txtValue = faq.textContent || faq.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        faq.style.display = "";
        visibleCount++;
      } else {
        faq.style.display = "none";
      }
    }
    if (visibleCount === 0) {
      wrapper.querySelector(".no-results").style.display = "";
    } else {
      wrapper.querySelector(".no-results").style.display = "none";
    }
  }
}

/**
 * Handles LOGOS page articles Search and Pagination
 * using AJAX requests
 */

function logosArticles() {
  const containers = document.querySelectorAll("[data-js='articles-list']");

  for (let i = 0; i < containers.length; i++) {
    init(containers[i]);
  }

  function init(container) {
    const formId = container.getAttribute("data-form-id");
    const articlesKey = container.getAttribute("data-articles");
    const articles = window[articlesKey];

    if (!container || !formId || !articles?.length) {
      return;
    }

    const searchForm = document.getElementById(formId);
    const searchInput = searchForm.querySelector("input[type='search']");

    async function fetchArticles(search, paged) {
      const params = {
        action: "lb_logos_articles",
        articles: encodeURIComponent(JSON.stringify(articles)),
        search,
        paged,
      };

      // set history
      history.replaceState(
        {
          ...params,
        },
        ""
      );

      const response = await fetch(`${siteData.ajaxUrl}`, {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams(params).toString(),
      });
      const html = await response.text();

      removeChildren(container);
      container.insertAdjacentHTML("beforeend", html);
      setPaginationListener();
    }

    function setPaginationListener() {
      const pagination = container.querySelectorAll(".page-numbers[data-page]");
      for (let i = 0; i < pagination.length; i++) {
        pagination[i].addEventListener("click", handlePaginationClick);
      }
    }

    async function handleFormSubmit(e) {
      e.preventDefault();
      await fetchArticles(searchInput.value, 1);
    }

    async function handlePaginationClick(e) {
      const targetPage = e.currentTarget.getAttribute("data-page");

      await fetchArticles(searchInput.value, parseInt(targetPage));
      maybeScrollToElementTop(container);
    }

    const initSearch = debounce(async function () {
      await fetchArticles(searchInput.value, 1);
    }, 1000);

    function handleInputKeyup(e) {
      if (
        e.currentTarget.value.toLowerCase() ===
        history.state.search.toLowerCase()
      ) {
        return;
      }
      initSearch();
    }

    async function handleInputClear() {
      await fetchArticles("", 1);
    }

    searchForm.addEventListener("submit", handleFormSubmit);
    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);
    setPaginationListener();
  }
}

/**
 * Media Posts AJAX
 */

function mediaPosts() {
  const container = document.querySelector("#media-posts");

  if (!container) {
    return;
  }

  // Constants
  const searchForms = document.querySelectorAll(".media-posts-searchform");
  const dateLinks = document.querySelectorAll(".widget-year a");
  const categories = document.querySelectorAll("#category-filter a");

  // Lays out functions
  function updateState(newState) {
    const updatedState = {
      ...history.state,
      ...newState,
    };

    // Build query string
    const queryString = { ...updatedState };
    // Remove empty values
    Object.keys(queryString).forEach(
      (k) =>
        (!queryString[k] || (k === "paged" && queryString[k] === 1)) &&
        delete queryString[k]
    );

    const urlQueryString = queryString
      ? `?${new URLSearchParams(queryString).toString()}`
      : "";
    const newUrl = `${window.location.protocol}//${window.location.host}/media${urlQueryString}`;
    history.pushState(updatedState, "", newUrl);
  }

  async function loadPosts() {
    const params = {
      action: "lb_media_posts",
      paged: history.state.paged,
      category: history.state.category,
      year: history.state.year,
      month: history.state.month,
      search: history.state.search,
    };

    const response = await fetch(`${siteData.ajaxUrl}`, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(params).toString(),
    });
    const html = await response.text();

    removeChildren(container);
    container.insertAdjacentHTML("beforeend", html);
    setPaginationListener(); // we need to set listener again as pagination HTML is replaced on the DOM
  }

  /**
   * Handle Pagination Click events
   * @param {MouseEvent} e
   */
  function handlePaginationClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault();

    // 2. Get the URL from href
    const href = e.currentTarget.href;

    // 3. Parse href to get the page number
    const parse = parseWpUrl(href);
    const paged = parse.paged;

    // 4. If page number found, update history
    updateState({
      paged,
    });

    // 5. Load the posts
    loadPosts();
  }

  /**
   * Handle Category Click events
   * @param {MouseEvent} e
   */
  function handleCategoryClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault();

    // 2. Get the URL from href
    const href = e.currentTarget.href;

    // 3. Parse href to get the page number
    // There's an exception for "Events" as this is post_type instead of category
    const parse = href.includes("/calendar")
      ? { category: "events" }
      : parseWpUrl(href);
    const category = parse.category || "";

    // 4. If page number found, update history
    updateState({
      category,
      paged: 1,
    });

    // 5. Set active state
    setActiveCategory(e.currentTarget);

    // 6. Load the posts
    loadPosts();
  }

  /**
   * Handle Date Archive Click events
   * @param {MouseEvent} e
   */
  function handleDateArchiveClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault();

    // 2. Get the URL from href
    const href = e.currentTarget.href;

    // 3. Parse href to get the page number
    const parse = parseWpUrl(href);
    const year = parse.year;
    const month = parse.month;

    // 4. Check if this is already active, in which case, remove date filter

    if (year === history.state.year && month === history.state.month) {
      updateState({
        year: null,
        month: null,
      });

      setActiveDateArchive(null);
    } else {
      // 4. If page number found, update history
      updateState({
        year,
        month,
        paged: 1,
      });

      // 5. Set active state
      setActiveDateArchive(e.currentTarget);
    }

    // 6. Load the posts
    loadPosts();
  }

  /**
   * Listens to Pagination click
   */
  function setPaginationListener() {
    const paginations = document.querySelectorAll("a.page-numbers");

    if (!paginations.length) {
      return;
    }

    for (let i = 0; i < paginations.length; i += 1) {
      paginations[i].addEventListener("click", handlePaginationClick);
    }
  }

  /**
   * Listens to category filter click
   */
  function setCategoryListener() {
    if (!categories.length) {
      return;
    }

    for (let i = 0; i < categories.length; i += 1) {
      categories[i].addEventListener("click", handleCategoryClick);
    }
  }

  /**
   * Sets active state on category links
   * @param {Element} element Active element
   */
  function setActiveCategory(element) {
    if (!categories.length) {
      return;
    }

    for (let i = 0; i < categories.length; i += 1) {
      if (categories[i] === element) {
        categories[i].classList.add("active");
      } else {
        categories[i].classList.remove("active");
      }
    }
  }

  function setDateArchiveListener() {
    if (!dateLinks.length) {
      return;
    }

    for (let i = 0; i < dateLinks.length; i += 1) {
      dateLinks[i].addEventListener("click", handleDateArchiveClick);
    }
  }

  /**
   * Sets active state on date links
   * @param {Element} element Active element
   */
  function setActiveDateArchive(element) {
    if (!dateLinks.length) {
      return;
    }

    for (let i = 0; i < dateLinks.length; i += 1) {
      if (dateLinks[i] === element) {
        dateLinks[i].classList.add("active");
      } else {
        dateLinks[i].classList.remove("active");
      }
    }

    const parents = dateLinks[0]
      .closest(".accordions")
      .querySelectorAll(".accordion > button");
    for (let i = 0; i < parents.length; i += 1) {
      const activeChild = parents[i]
        .closest(".accordion")
        .querySelector("a.active");
      if (activeChild) {
        parents[i].classList.add("active");
      } else {
        parents[i].classList.remove("active");
      }
    }
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    const searchInput = e.currentTarget.querySelector('input[type="search"]');

    updateState({
      search: searchInput.value,
    });

    loadPosts();
  }

  const initSearch = debounce(async function () {
    updateState({
      search: history.state.searchQuery,
    });
    await loadPosts();
  }, 1000);

  function handleInputKeyup(e) {
    if (
      e.currentTarget.value.toLowerCase() === history.state.search.toLowerCase()
    ) {
      return;
    }
    history.state.searchQuery = e.currentTarget.value;
    initSearch();
  }

  function handleInputClear() {
    updateState({
      search: "",
    });
    loadPosts();
  }

  function init() {
    // 1. Set default state
    const parse = parseWpUrl(window.location.href);

    history.replaceState(
      {
        paged: parse.paged || 1,
        category: parse.category || "",
        search: parse.search || "",
        year: parse.year || null,
        month: parse.month || null,
      },
      ""
    );

    for (let i = 0; i < searchForms.length; i++) {
      const searchForm = searchForms[i];
      const searchInput = searchForm.querySelector("input[type='search']");
      searchForm.addEventListener("submit", handleFormSubmit);
      searchInput.addEventListener("keyup", handleInputKeyup);
      searchInput.addEventListener("search", handleInputClear);
    }

    setCategoryListener();
    setDateArchiveListener();
    setPaginationListener();
  }

  init();
}

/**
 * Research Posts AJAX
 */

function researchPosts() {
  const container = document.querySelector("#research-posts");

  if (!container) {
    return;
  }

  // 1. Listen to all pagination clicks on each section
  const sections = document.querySelectorAll("[data-js-id]");

  if (!sections.length) {
    return;
  }

  async function loadPosts(args, section) {
    const params = {
      action: "lb_research_posts",
      ...args,
    };

    const response = await fetch(`${siteData.ajaxUrl}`, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(params).toString(),
    });
    const html = await response.text();

    removeChildren(section);
    section.insertAdjacentHTML("beforeend", html);
    setPaginationListener(section); // we need to set listener again as pagination HTML is replaced on the DOM
  }

  function handlePaginationClick(e, section) {
    e.preventDefault();

    // 1. Get page number
    const { paged } = parseWpUrl(e.currentTarget.getAttribute("href"));

    // const sectionKey = section.getAttribute("data-js-id");
    const postType = section.getAttribute("data-js-post_type");
    const category = section.getAttribute("data-js-category");
    loadPosts({ post_type: postType, category, paged }, section);
  }

  function setPaginationListener(section) {
    const paginations = section.querySelectorAll("a.page-numbers");

    if (paginations.length) {
      for (let i = 0; i < paginations.length; i++) {
        paginations[i].addEventListener("click", (e) =>
          handlePaginationClick(e, section)
        );
      }
    }
  }

  for (let i = 0; i < sections.length; i++) {
    setPaginationListener(sections[i]);
  }
}

/**
 * Faculty / Staff Posts AJAX
 */

function facultyPosts() {
  const container = document.querySelector("#staff-members");

  if (!container) {
    return;
  }

  // 1. Listen to all pagination clicks on each section
  const sections = document.querySelectorAll("[data-js-id]");

  if (!sections.length) {
    return;
  }

  async function loadPosts(args, section) {
    const params = {
      action: "lb_staff_posts",
      ...args,
    };

    const response = await fetch(`${siteData.ajaxUrl}`, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(params).toString(),
    });
    const html = await response.text();

    removeChildren(section);
    section.insertAdjacentHTML("beforeend", html);
    setPaginationListener(section); // we need to set listener again as pagination HTML is replaced on the DOM
  }

  function handlePaginationClick(e, section) {
    e.preventDefault();

    // 1. Get page number
    const { paged } = parseWpUrl(e.currentTarget.getAttribute("href"));

    // 2. Get category
    const category = section.getAttribute("data-js-id");
    loadPosts({ category, paged }, section);
  }

  function setPaginationListener(section) {
    const paginations = section.querySelectorAll("a.page-numbers");

    if (paginations.length) {
      for (let i = 0; i < paginations.length; i++) {
        paginations[i].addEventListener("click", (e) =>
          handlePaginationClick(e, section)
        );
      }
    }
  }

  for (let i = 0; i < sections.length; i++) {
    setPaginationListener(sections[i]);
  }
}

/**
 * Submits form when "clear" / "x" is clicked on input[type='search']
 * @returns void
 */
function searchClear() {
  const inputs = document.querySelectorAll(
    "input[type='search']#research-searchform-input"
  );

  if (!inputs.length) {
    return;
  }

  for (let i = 0; i < inputs.length; i++) {
    const input = inputs[i];
    const form = input.closest("form");
    input.addEventListener("search", function (e) {
      form.submit();
    });
  }
}

/**
 * Handle YouTube playlist pagination
 */

function youtubePlaylistPagination() {
  const wrappers = document.querySelectorAll(
    "[data-playlist-id], [data-video-id]"
  );

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    const wrapper = el;
    const playlistId = el.getAttribute("data-playlist-id");
    const videoId = el.getAttribute("data-video-id");

    if (!wrapper || (!playlistId && !videoId)) {
      return;
    }

    async function loadPlaylist(ids, token, wrapper) {
      const params = {
        action: "lb_youtube_playlist",
        page_token: token,
      };

      if (ids.playlistId) {
        params.playlist_id = playlistId;
      }

      if (ids.videoId) {
        params.video_id = videoId;
      }

      const response = await fetch(`${siteData.ajaxUrl}`, {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams(params).toString(),
      });
      const html = await response.text();

      removeChildren(wrapper);
      wrapper.insertAdjacentHTML("beforeend", html);
      setListener();
    }

    function handlePaginationClick(e) {
      e.preventDefault();
      const token = e.currentTarget.getAttribute("data-token");

      if (!token) {
        return;
      }

      loadPlaylist({ playlistId, videoId }, token, wrapper);
    }

    function setListener() {
      const prev = wrapper.querySelector(".page-numbers.prev");
      const next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }
      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  }

  // Initialize
  for (let i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}

/**
 * Handle gallery initialization & pagination
 */

function gallery() {
  const wrappers = document.querySelectorAll(".post-gallery-wrapper");

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    const wrapper = el;

    function initializeLightGallery() {
      const gallery = el.querySelector(".lightgallery");

      if (!gallery) {
        return;
      }
      lightGallery(gallery, {
        plugins: [lgThumbnail],
        speed: 250,
        licenseKey: "0000-0000-000-0000",
        selector: ".post-gallery__item",
      });
    }

    initializeLightGallery();

    async function loadGallery(galleryData, page, wrapper) {
      const params = {
        action: "lb_gallery",
        paged: page,
        ...galleryData,
      };

      const response = await fetch(`${siteData.ajaxUrl}`, {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams(params).toString(),
      });
      const html = await response.text();

      removeChildren(wrapper);
      wrapper.insertAdjacentHTML("beforeend", html);
      initializeLightGallery();
      setListener();
    }

    function handlePaginationClick(e) {
      e.preventDefault();

      const galleryData = window._gallery[wrapper.id];
      if (!galleryData) {
        return;
      }
      const page = e.currentTarget.getAttribute("data-page");

      loadGallery(galleryData, parseInt(page), wrapper);
    }

    function setListener() {
      const prev = wrapper.querySelector(".page-numbers.prev");
      const next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }
      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  }

  // Initialize
  for (let i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}

/**
 * Handle staff's published works pagination
 */

function publishedWorks() {
  const wrappers = document.querySelectorAll(".published-works");

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    const wrapper = el;

    function initializeLightGallery() {
      const gallery = el.querySelector(".lightgallery");

      if (!gallery) {
        return;
      }
      lightGallery(gallery, {
        plugins: [lgThumbnail],
        speed: 250,
        licenseKey: "0000-0000-000-0000",
        selector: ".post-gallery__item",
      });
    }

    initializeLightGallery();

    async function loadPosts(page) {
      const field = wrapper.getAttribute("data-field");
      const postId = wrapper.getAttribute("data-post_id");

      const params = {
        action: "lb_published_works",
        field,
        post_id: postId,
        paged: page,
      };

      const response = await fetch(`${siteData.ajaxUrl}`, {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams(params).toString(),
      });
      const html = await response.text();

      removeChildren(wrapper);
      wrapper.insertAdjacentHTML("beforeend", html);
      initializeLightGallery();
      setListener();
    }

    function handlePaginationClick(e) {
      e.preventDefault();

      const page = e.currentTarget.getAttribute("data-page");
      loadPosts(parseInt(page));
    }

    function setListener() {
      const prev = wrapper.querySelector(".page-numbers.prev");
      const next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }
      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  }

  // Initialize
  for (let i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}

/**
 * Handle courses search
 */

function coursesList() {
  const searchForm = document.querySelector("#search-course-form form");
  const container = document.querySelector("#courses-list");

  if (!container || !searchForm) {
    return;
  }

  // Constants
  const searchInput = searchForm.querySelector("#search-course-keyword");
  const semesterCheckboxes = searchForm.querySelectorAll(
    'input[type="checkbox"][name="semester[]"]'
  );
  const categoryCheckboxes = searchForm.querySelectorAll(
    'input[type="checkbox"][name="category[]"]'
  );

  // Lays out functions
  function updateState(newState) {
    const updatedState = {
      ...history.state,
      ...newState,
    };

    // Build query string
    const queryString = { ...updatedState };

    // Reformat arrays differently ...
    const categoryArray = queryString.category;
    const semesterArray = queryString.semester;
    delete queryString.category;
    delete queryString.semester;

    // Remove empty values
    Object.keys(queryString).forEach(
      (k) =>
        (!queryString[k] || (k === "paged" && queryString[k] === 1)) &&
        delete queryString[k]
    );

    const params = new URLSearchParams(queryString);
    if (categoryArray.length) {
      categoryArray.forEach((cat) => {
        params.append("category[]", cat);
      });
    }
    if (semesterArray.length) {
      semesterArray.forEach((sem) => {
        params.append("semester[]", sem);
      });
    }

    const urlQueryString = params.toString() ? `?${params.toString()}` : "";
    const newUrl = `${window.location.protocol}//${window.location.host}/academics/courses${urlQueryString}`;
    history.pushState(updatedState, "", newUrl);
  }

  async function loadPosts() {
    const params = {
      action: "lb_courses_list",
      paged: history.state.paged,
      category: history.state.category,
      search: history.state.search,
      semester: history.state.semester,
    };

    const response = await fetch(`${siteData.ajaxUrl}`, {
      method: "POST",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams(params).toString(),
    });
    const html = await response.text();

    removeChildren(container);
    container.insertAdjacentHTML("beforeend", html);
    setPaginationListener(); // we need to set listener again as pagination HTML is replaced on the DOM
  }

  /**
   * Handle Pagination Click events
   * @param {MouseEvent} e
   */
  function handlePaginationClick(e) {
    e.preventDefault();

    const paged = e.currentTarget.getAttribute("data-page");

    updateState({ paged });

    loadPosts();
  }

  /**
   * Listens to Pagination click
   */
  function setPaginationListener() {
    const paginations = container.querySelectorAll("button.page-numbers");

    if (!paginations.length) {
      return;
    }

    for (let i = 0; i < paginations.length; i += 1) {
      paginations[i].addEventListener("click", handlePaginationClick);
    }
  }

  function handleSemesterChange() {
    // 1. Get checked values
    const semester = [];
    for (let i = 0; i < semesterCheckboxes.length; i += 1) {
      if (semesterCheckboxes[i].checked && semesterCheckboxes[i].value) {
        semester.push(semesterCheckboxes[i].value);
      }
    }

    // 2. If value is the same, bail out
    if (history.semester === semester) {
      return;
    }

    // 3. Update history
    updateState({
      semester,
      paged: 1,
    });

    // 4. Load the posts
    loadPosts();
  }

  function handleCategoryChange() {
    // 1. Get checked values
    const category = [];
    for (let i = 0; i < categoryCheckboxes.length; i += 1) {
      if (categoryCheckboxes[i].checked && categoryCheckboxes[i].value) {
        category.push(categoryCheckboxes[i].value);
      }
    }

    // 2. If value is the same, bail out
    if (history.category === category) {
      return;
    }

    // 3. Update history
    updateState({
      category,
      paged: 1,
    });

    // 4. Load the posts
    loadPosts();
  }

  function handleFormSubmit(e) {
    e.preventDefault();

    updateState({
      search: searchInput.value,
    });

    loadPosts();
  }

  const initSearch = debounce(async function () {
    updateState({
      search: searchInput.value,
    });
    await loadPosts();
  }, 1000);

  function handleInputKeyup(e) {
    if (
      e.currentTarget.value.toLowerCase() === history.state.search.toLowerCase()
    ) {
      return;
    }
    initSearch();
  }

  function handleInputClear() {
    updateState({
      search: "",
    });
    loadPosts();
  }

  function init() {
    // 1. Set default state0
    const parse = parseWpUrl(window.location.href);
    const searchParams = new URLSearchParams(window.location.search);

    history.replaceState(
      {
        paged: parse.paged || 1,
        category: searchParams.get("category") || [],
        semester: searchParams.get("semester") || [],
        search: searchParams.get("search") || "",
      },
      ""
    );

    searchForm.addEventListener("submit", handleFormSubmit);
    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);

    window.addEventListener("semesterUpdated", handleSemesterChange);
    window.addEventListener("categoryUpdated", handleCategoryChange);

    // for (let i = 0; i < categoryCheckboxes.length; i += 1) {
    //   categoryCheckboxes[i].addEventListener("change", handleCategoryChange);
    // }
    setPaginationListener();
  }

  init();
}

/**
 * Shop Mobile Filter initialization
 */
function shopMobileFilter() {
  const button = document.getElementById("shop-filter-button");
  if (!button) return;

  const page = document.getElementById("page");
  const widget = document.getElementById("secondary");
  const closeButton = document.getElementById("shop-filter-close-button");

  function handleOutsideClick(e) {
    if (!widget.classList.contains("active")) {
      return;
    }
    const select2 = document.querySelector(".select2-container");
    if (
      !widget.contains(e.target) &&
      !button.contains(e.target) &&
      !select2.contains(e.target)
    ) {
      closeMobileFilter();
      document.removeEventListener("click", handleOutsideClick);
    }
  }

  function openMobileFilter(e) {
    e.preventDefault();

    widget.classList.add("active");
    page.classList.add("mobile-filter-active");
    document.documentElement.classList.add("noscroll");
    document.addEventListener("click", handleOutsideClick);

    // Re-initialize all select2 selects
    // Warning, this causes S&F to refresh the search
    // const selects = document.querySelectorAll("select");
    // for (let i = 0; i < selects.length; i++) {
    //   selects[i].dispatchEvent(new Event("change"));
    // }
  }

  function closeMobileFilter() {
    widget.classList.remove("active");
    page.classList.remove("mobile-filter-active");
    document.documentElement.classList.remove("noscroll");
  }

  button.addEventListener("click", openMobileFilter);
  closeButton.addEventListener("click", closeMobileFilter);

  const sfSubmit = document.querySelector('input[name="_sf_submit"]');
  const sfReset = document.querySelector(".search-filter-reset");

  if (sfSubmit) {
    sfSubmit.addEventListener("click", closeMobileFilter);
  }
  if (sfReset) {
    sfReset.addEventListener("click", closeMobileFilter);
  }

  document.body.addEventListener("keyup", function (e) {
    if (e.key === "Escape") {
      closeMobileFilter();
    }
  });
}

function searchCourseCheckboxes() {
  const names = ["semester[]", "category[]"];

  function handleCheckboxesChange(checkboxes) {
    const allCheckboxes = Array.from(checkboxes).filter((c) => !c.value);
    const otherCheckboxes = Array.from(checkboxes).filter((c) => c.value);

    if (allCheckboxes.length && otherCheckboxes.length) {
      const allCheckbox = allCheckboxes[0];

      // when checking "all", uncheck the other checkboxes
      allCheckbox.addEventListener("change", function (e) {
        if (e.currentTarget.checked) {
          for (let i = 0; i < otherCheckboxes.length; i++) {
            otherCheckboxes[i].checked = false;
          }
        }
      });

      // when checking other checkboxes and "all" is checked, uncheck "all"
      for (let i = 0; i < otherCheckboxes.length; i++) {
        otherCheckboxes[i].addEventListener("change", function (e) {
          if (e.currentTarget.checked && allCheckbox.checked) {
            allCheckbox.checked = false;
          }
        });
      }
    }
  }

  function init(name) {
    const checkboxes = document.querySelectorAll(
      `input[type="checkbox"][name="${name}"]`
    );

    if (!checkboxes.length) {
      return;
    }
    handleCheckboxesChange(checkboxes);
  }

  for (let i = 0; i < names.length; i += 1) {
    init(names[i]);
  }
}

function searchCourseDropdown() {
  const triggers = document.querySelectorAll("button[data-dropdown]");
  if (!triggers.length) {
    return;
  }

  function getDropdown(trigger) {
    const dropdownId = trigger.getAttribute("data-dropdown");
    const dropdown = document.getElementById(dropdownId);
    return dropdown;
  }

  function closeDropdown(trigger, dropdown) {
    trigger.classList.remove("active");
    dropdown.classList.remove("active");
  }

  function openDropdown(trigger, dropdown) {
    trigger.classList.add("active");
    dropdown.classList.add("active");

    const wrapper = trigger.closest(".search-course__fields__select");

    hideOnClickOutside(trigger, wrapper, function () {
      closeDropdown(trigger, dropdown);
    });
  }

  function handleTriggerClick(e) {
    e.preventDefault();

    const dropdown = getDropdown(e.currentTarget);

    if (dropdown) {
      if (!e.currentTarget.classList.contains("active")) {
        openDropdown(e.currentTarget, dropdown);
      } else {
        closeDropdown(e.currentTarget, dropdown);
      }
    }
  }

  function handleFocusEvent(e, wrapper, trigger) {
    if (e.key === "Tab") {
      const dropdown = getDropdown(trigger);
      if (wrapper.contains(document.activeElement)) {
        openDropdown(trigger, dropdown);
      } else {
        closeDropdown(trigger, dropdown);
      }
    }
  }

  function handleCheckboxesChange(checkboxes, trigger) {
    if (!trigger) {
      return;
    }

    // Listen to checkboxes change and update the trigger text
    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].addEventListener("change", function (e) {
        let checkedLabels = [];

        if (trigger.hasAttribute("data-event")) {
          window.dispatchEvent(new Event(trigger.getAttribute("data-event")));
        }

        // 1. Get checked labels
        for (let j = 0; j < checkboxes.length; j++) {
          if (checkboxes[j].checked) {
            const id = checkboxes[j].id;
            const label = document.querySelector(`label[for="${id}"]`);
            const labelString = label.innerText;
            checkedLabels.push(labelString);
          }
        }

        // 2. Update trigger text
        trigger.innerText = checkedLabels.length
          ? checkedLabels.join(", ")
          : trigger.getAttribute("data-placeholder");
      });
    }
  }

  for (let i = 0; i < triggers.length; i++) {
    triggers[i].addEventListener("click", handleTriggerClick);

    // Listen to focus events
    const wrapper = triggers[i].closest(".search-course__fields__select");
    document.addEventListener("keyup", (e) =>
      handleFocusEvent(e, wrapper, triggers[i])
    );

    // Listen to checkbox changes for better UX
    const checkboxes = wrapper.querySelectorAll('input[type="checkbox"]');
    handleCheckboxesChange(checkboxes, triggers[i]);
  }
}

/**
 * Detect linked images and set up popups using lity
 * Only applied on single product pages
 */
function imagePopups() {
  // Only apply this to single products to avoid bloat on other pages
  if (!document.body.classList.contains("single-product")) {
    return;
  }

  // 1. Get all applicable links. Using * selector instead of $ just in case there is query string
  const links = document.querySelectorAll(
    "a[href*='.jpg'], a[href*='.jpeg'], a[href*='.png'], a[href*='.gif']"
  );

  // 2. Append data-lity
  links.forEach((link) => {
    // Check if this link has <img> as it's child
    // TODO: Should we also check if href and src are similar?
    // This is tricky as sometimes WP appends sizes to the filename and the link can be different from the source
    if (link.querySelector("img")) {
      link.setAttribute("data-lity", "");
    }
  });
}

/*********************
HELPER METHODS
*********************/

/**
 * Format number to USD format
 * @param {number} number
 * @returns {string} Formatted currency
 */
function formatCurrency(number) {
  if (number && !isNaN(parseInt(number))) {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(number);
  }
}

/**
 * Vanilla JS slide up animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */
function slideUp(target, duration = 250) {
  target.style.transitionProperty = "height, margin, padding";
  target.style.transitionDuration = duration + "ms";
  target.style.boxSizing = "border-box";
  target.style.height = target.offsetHeight + "px";
  target.offsetHeight;
  target.style.overflow = "hidden";
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  window.setTimeout(() => {
    target.style.display = "none";
    target.style.removeProperty("height");
    target.style.removeProperty("padding-top");
    target.style.removeProperty("padding-bottom");
    target.style.removeProperty("margin-top");
    target.style.removeProperty("margin-bottom");
    target.style.removeProperty("overflow");
    target.style.removeProperty("transition-duration");
    target.style.removeProperty("transition-property");
  }, duration);
}

/**
 * Vanilla JS slide down animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */
function slideDown(target, duration = 250) {
  target.style.removeProperty("display");
  let display = window.getComputedStyle(target).display;
  if (display === "none") display = "block";
  target.style.display = display;
  let height = target.offsetHeight;
  target.style.overflow = "hidden";
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  target.offsetHeight;
  target.style.boxSizing = "border-box";
  target.style.transitionProperty = "height, margin, padding";
  target.style.transitionDuration = duration + "ms";
  target.style.height = height + "px";
  target.style.removeProperty("padding-top");
  target.style.removeProperty("padding-bottom");
  target.style.removeProperty("margin-top");
  target.style.removeProperty("margin-bottom");
  window.setTimeout(() => {
    target.style.removeProperty("height");
    target.style.removeProperty("overflow");
    target.style.removeProperty("transition-duration");
    target.style.removeProperty("transition-property");
  }, duration);
}

/**
 * Vanilla JS slide toggle animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */
function slideToggle(target, duration = 250) {
  if (window.getComputedStyle(target).display === "none") {
    return slideDown(target, duration);
  } else {
    return slideUp(target, duration);
  }
}

/**
 * Checks if the user's device has touch screen
 * @returns {Boolean}
 */
function deviceHasTouchScreen() {
  let hasTouchScreen = false;
  if (typeof window !== "undefined") {
    if ("maxTouchPoints" in navigator) {
      hasTouchScreen = navigator.maxTouchPoints > 0;
    } else if ("msMaxTouchPoints" in navigator) {
      hasTouchScreen = navigator.msMaxTouchPoints > 0;
    } else {
      const mQ = window.matchMedia && matchMedia("(pointer:coarse)");
      if (mQ && mQ.media === "(pointer:coarse)") {
        hasTouchScreen = !!mQ.matches;
      } else if ("orientation" in window) {
        hasTouchScreen = true; // deprecated, but good fallback
      } else {
        // Only as a last resort, fall back to user agent sniffing
        const UA = navigator.userAgent;
        hasTouchScreen =
          /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) ||
          /\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA);
      }
    }
  }
  return hasTouchScreen;
}

/**0
 * Debounce helper
 * @see https://github.com/angus-c/just/tree/master/packages/function-debounce
 *
 * @param {Function} fn Callback function
 * @param {number} [wait = 0] Debounce wait interval in milliseconds
 * @param {boolean} [callFirst = false] Debounce wait interval in milliseconds
 * @return {Function} Callback function
 */
function debounce(fn, wait, callFirst) {
  var timeout = null;
  var debouncedFn = null;

  var clear = function () {
    if (timeout) {
      clearTimeout(timeout);

      debouncedFn = null;
      timeout = null;
    }
  };

  var flush = function () {
    var call = debouncedFn;
    clear();

    if (call) {
      call();
    }
  };

  var debounceWrapper = function () {
    if (!wait) {
      return fn.apply(this, arguments);
    }

    var context = this;
    var args = arguments;
    var callNow = callFirst && !timeout;
    clear();

    debouncedFn = function () {
      fn.apply(context, args);
    };

    timeout = setTimeout(function () {
      timeout = null;

      if (!callNow) {
        var call = debouncedFn;
        debouncedFn = null;

        return call();
      }
    }, wait);

    if (callNow) {
      return debouncedFn();
    }
  };

  debounceWrapper.cancel = clear;
  debounceWrapper.flush = flush;

  return debounceWrapper;
}

/**
 * Efficiently removes children of a parent element
 * @param {Element} el Parent element whose children will be removed
 */
function removeChildren(el) {
  while (el.firstChild) {
    el.removeChild(el.lastChild);
  }
}

/**
 * Removes 'active' class on an element on clicking outside of it
 * @param {Element} element Element which will lose the 'active' class
 * @param {Element} [wrapper] Wrapper element where clicks inside will be ignored. Optional
 * @param {() => void} [cb] Callback executed after outside click is detected. Optional
 */
function hideOnClickOutside(element, wrapper, cb) {
  if (!wrapper) {
    wrapper = element;
  }
  const outsideClickListener = (event) => {
    // console.log(isVisible(element));
    if (!wrapper.contains(event.target) && isVisible(wrapper)) {
      // or use: event.target.closest(selector) === null
      // element.style.display = "none";
      element.classList.remove("active");

      if (typeof cb === "function") {
        cb();
      }

      removeClickListener();
    }
  };

  const removeClickListener = () => {
    document.removeEventListener("click", outsideClickListener);
  };

  document.addEventListener("click", outsideClickListener);
}

/**
 * Check if an element is visible
 * @param {Element} el Element to check
 * @returns {Boolean}
 */
function isVisible(el) {
  return (
    !!el && !!(el.offsetWidth || el.offsetHeight || el.getClientRects().length)
  );
}

/**
 * May be scroll to element top, if not visible yet
 * @param {Element} el Element to scroll to
 */
function maybeScrollToElementTop(el) {
  const elementTopPosition = el.getBoundingClientRect().top;
  if (elementTopPosition < 0) {
    window.scrollTo({
      top: window.pageYOffset + elementTopPosition - 100,
      behavior: "smooth",
    });
  }
}

/**
 * Parse WP URL and extract query data
 * @param {string} url
 */
function parseWpUrl(url) {
  const pathnames = url.split("/");
  const qs = new URLSearchParams(url.split("?").pop());
  const data = {};

  function isMaybeYear(string) {
    return string.length === 4 && !isNaN(string);
  }

  function isMaybeMonth(string) {
    return string.length === 2 && !isNaN(string);
  }

  for (let i = 0; i < pathnames.length; i += 1) {
    // 1. Check if this contains page query, e.g. /page/2/
    if (
      pathnames[i] === "page" &&
      pathnames[i + 1] &&
      !isNaN(pathnames[i + 1])
    ) {
      data.paged = parseInt(pathnames[i + 1]);
    } else if (qs.has("paged")) {
      data.paged = qs.get("paged");
    }

    // 2. Check if this contains category query, e.g. /category/slug/
    if (pathnames[i] === "category" && pathnames[i + 1]) {
      data.category = pathnames[i + 1];
    } else if (qs.has("category")) {
      data.category = qs.get("category");
    }

    // 3. Check if this contains date query, e.g. /2021/19/
    if (isMaybeYear(pathnames[i]) && isMaybeMonth(pathnames[i + 1])) {
      data.year = parseInt(pathnames[i]);
      data.month = parseInt(pathnames[i + 1]);
    } else if (qs.has("year") && qs.has("month")) {
      data.year = qs.get("year");
      data.month = qs.get("month");
    }
  }

  return data;
}
