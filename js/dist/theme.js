"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var $ = jQuery.noConflict(); // Initialize functions

document.addEventListener("DOMContentLoaded", function () {
  console.log("JMJ");
  accessibility();
  headerSearch();
  smoothScroll();
  accordions();
  tabs();
  readMoreInit();
  faqsSearch();
  logosArticles();
  mediaPosts();
  researchPosts();
  facultyPosts();
  searchClear();
  youtubePlaylistPagination();
  gallery();
  publishedWorks();
  searchCourseCheckboxes();
  searchCourseDropdown();
  coursesList();
  horizontalScroll(".lb-tabs:not(.no-scroll) .tabs");
  horizontalScroll("#category-filter");
  shopMobileFilter();
  imagePopups(); // slickSlider('.some-slider');
  // aosIE();
});
$(document).on("sf:ajaxfinish", ".searchandfilter", function () {
  shopMobileFilter();
}); // Initialize Gravity Forms functions
// $(document).bind('gform_post_render', function () {
//   // Gravity Forms JS
// });

/*********************
DECLARED FUNCTIONS
*********************/

/**
 * Sets up accessibility listeners
 */

function accessibility() {
  // Remove focus from links
  document.body.classList.add("is-mouse"); // Listen to tab events to enable outlines (accessibility improvement)

  document.body.addEventListener("keyup", function (e) {
    if (e.key === "Tab") {
      document.body.classList.remove("is-mouse");
    }
  }); // Let the document know when the mouse is being used

  document.body.addEventListener("mousedown", function () {
    document.body.classList.add("is-mouse");
  }); // Check if this is touch device

  if (deviceHasTouchScreen()) {
    document.body.classList.add("is-touch-device");
  } else {
    document.body.classList.add("not-touch-device");
  }
}

function headerSearch() {
  function initHeaderSearch(wrapper) {
    var trigger = wrapper.querySelector(".header__search__button");
    var searchBox = wrapper.querySelector(".header__search-box");
    var searchResults = wrapper.querySelector(".header__search-box__results");
    var searchForm = wrapper.querySelector("form");
    var searchInput = searchForm.querySelector('input[type="search"]');

    if (!searchResults || !searchForm || !searchInput) {
      return;
    }

    function toggleSearchBox() {
      searchBox.classList.toggle("active");

      if (searchBox.classList.contains("active")) {
        // Focus on the input
        searchInput.focus();
        hideOnClickOutside(searchBox, wrapper);
      } else {
        searchInput.blur();
      }
    }

    function setSkeleton() {
      var skeletonHtml = "<ul class=\"skeleton\">\n       <li><h5 class=\"skeleton-bg\">&nbsp;</h5>\n        <ul>\n         <li class=\"skeleton-bg\">&nbsp;</li>\n         <li class=\"skeleton-bg\">&nbsp;</li>\n         <li class=\"skeleton-bg\">&nbsp;</li>\n        </ul>\n       </li>\n       <li><h5 class=\"skeleton-bg\">&nbsp;</h5>\n        <ul>\n         <li class=\"skeleton-bg\">&nbsp;</li>\n         <li class=\"skeleton-bg\">&nbsp;</li>\n        </ul>\n       </li>\n      </ul>"; // Delete current content

      removeChildren(searchResults); // Replace

      searchResults.insertAdjacentHTML("afterbegin", skeletonHtml);
    }

    function processResults(results) {
      var html = "";
      var types = ["Post", "Page"];

      if (results.length) {
        var _loop = function _loop(i) {
          // 1. Filter results according to type
          var filteredResults = results.filter(function (result) {
            return result.type === types[i].toLowerCase();
          });

          if (filteredResults.length > 0) {
            html += "<li><h5 class=\"post-type\">".concat(types[i], "</h5>");
            html += "<ul>";

            for (var j = 0; j < filteredResults.length; j++) {
              var _filteredResults$j = filteredResults[j],
                  title = _filteredResults$j.title,
                  link = _filteredResults$j.link;
              html += "<li><a href=\"".concat(link, "\">").concat(title.rendered, "</a></li>");
            }

            html += "</ul></li>";
          }
        };

        for (var i = 0; i < types.length; i++) {
          _loop(i);
        }
      }

      var finalHtml = html ? "<ul>".concat(html, "</ul>") : "<ul><li class=\"no-results\">No results are found.</li></ul>"; // Delete current content

      removeChildren(searchResults); // Replace

      searchResults.insertAdjacentHTML("afterbegin", finalHtml);
    }

    function fetchSearchResults(query) {
      fetch("".concat(siteData.siteUrl, "/wp-json/relevanssi/v1/search?search=").concat(query)).then(function (response) {
        return response.json();
      }).then(function (response) {
        return processResults(response);
      }).catch(function (err) {
        return console.log("Fetch Error:", err);
      });
    }

    var getDebouncedInputValue = debounce(function () {
      if (searchInput.value) {
        fetchSearchResults(searchInput.value);
      } else {
        removeChildren(searchResults);
      }
    }, 500);

    var handleInputKeyup = function handleInputKeyup() {
      setSkeleton();
      getDebouncedInputValue();
    };

    var handleInputClear = function handleInputClear() {
      removeChildren(searchResults);
    };

    if (trigger) {
      trigger.addEventListener("click", toggleSearchBox);
    }

    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);
  }

  var wrappers = document.querySelectorAll(".header__search");

  for (var i = 0; i < wrappers.length; i++) {
    initHeaderSearch(wrappers[i]);
  }
}

function slickSlider(slider) {
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      /* Initializes a slick carousel only on mobile screens */
      // slick on mobile

      if ($(window).width() > 1025) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: true,
            speed: 400,
            autoplay: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: '<div class="slick-arrow slick-prev">Prev</div>',
            //replace arrows
            nextArrow: '<div class="slick-arrow slick-next">Next></div>',
            //replace arrows
            responsive: [{
              breakpoint: 601,
              settings: "unslick"
            }]
          });
        }
      }
    });
  });
}
/**
 * Initialize Smooth Scroll
 */


function smoothScroll() {
  // Smooth scroll for direct-to-anchor external links
  // if (window.location.hash) {
  //   $("html, body").animate(
  //     {
  //       scrollTop: $(window.location.hash).offset().top - 100,
  //     },
  //     1000
  //   );
  // }
  // Select all links with hashes, same page
  $('a[href*="#"][data-smooth-scroll]').click(function (event) {
    // On-page links
    if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
      // Figure out element to scroll to
      var hash = this.hash;
      var target = $(hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]"); // Does a scroll target exist?

      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $("html, body").animate({
          scrollTop: target.attr("role") === "tabpanel" ? target.parent().offset().top : target.offset().top
        }, 1000, function () {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();

          if ($target.is(":focus")) {
            // Checking if the target was focused
            return false;
          } else {// $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
            // $target.focus(); // Set focus again
          }
        });
      }

      if (window.location.hash === hash) {
        $(window).trigger("hashchange");
      } else {
        window.location.hash = hash;
      }
    }
  });
}
/**
 * Initialize Tabs
 */


function tabs() {
  var tabs = $(".lb-tabs"); // tabs container class

  var activeClass = "active";
  tabs.each(function () {
    var container = $(this);
    var tablist = $(this).find('[role="tablist"]')[0]; // Select the first one to avoid subtabs

    var tab = $(this).find('[role="tab"]');
    var tabContent = $(this).find('[role="tabpanel"]');
    var useHash = false;
    var useHashAttr = $(this).attr("data-use-hash");

    if (_typeof(useHashAttr) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && useHashAttr !== false) {
      useHash = true;
    }

    function isSameTabParent(_, element) {
      return element.closest(".lb-tabs") === container[0];
    } // 1. Filter tabs to exclude sub tabs / parent tabs


    tab = tab.filter(isSameTabParent).filter(':not([id*="slick"])');
    tabContent = tabContent.filter(isSameTabParent);

    function setActiveTab(id) {
      var focus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
      tab.each(function () {
        if ($(this).attr("aria-controls") === id) {
          // Check if we should activate the parent tab too
          // by using data-parent-tab attribute
          var parentTabId = $(this).attr("data-parent-tab");

          if (parentTabId) {
            setActiveTab(parentTabId);
          }

          $(this).addClass(activeClass).attr({
            "aria-selected": true
          });

          if (focus) {
            $(this).focus();
          }
        } else {
          $(this).removeClass(activeClass).attr({
            "aria-selected": false
          }).blur();
        }
      });
      tabContent.each(function () {
        if ($(this).attr("id") === id) {
          $(this).addClass(activeClass).attr({
            "aria-expanded": true
          });
        } else {
          $(this).removeClass(activeClass).attr({
            "aria-expanded": false
          });
        }
      }); // Dispatch Event

      var event = new Event("changeTab");
      window.dispatchEvent(event);
    }

    function handleTabClick(e) {
      e.preventDefault(); // 1. Remove pre-selected default tabs classes

      tab.filter(".tab-default").removeClass("tab-default");
      tabContent.filter(".tab-default").removeClass("tab-default"); // 2. Set active tab

      if (!$(this).hasClass(activeClass)) {
        setActiveTab($(this).attr("aria-controls"));
      } // 3. Update URL (if required)


      if (useHash) {
        history.replaceState({}, "", $(this).attr("href"));
      } // 4. Slide clicked tab into view


      if (!$(this).hasClass("slick-slide")) {
        // slide to view
        var buffer = 50; // gradient

        var container = $(this).closest(".tabs");
        var frame = container.closest(".overflow-wrapper");
        var offset;

        if ($(this).position().left <= buffer || $(this).position().left + $(this).width() >= frame.width() - buffer) {
          if ($(this).position().left > frame.width() / 2) {
            // scroll to right
            offset = container.scrollLeft() + ($(this).position().left - container.width() + $(this).width() + 50);
          } else {
            // scroll to left
            offset = container.scrollLeft() + ($(this).position().left - 50);
          }
        }

        container.animate({
          scrollLeft: offset
        }, 250);
      }
    }

    function handleKeyboardNavigation(e) {
      switch (e.key) {
        case "ArrowLeft": // left arrow

        case "ArrowUp":
          // up arrow
          e.preventDefault();
          var prev = tab.filter("." + activeClass).prev().length ? tab.filter("." + activeClass).prev().attr("aria-controls") : tab.last().attr("aria-controls");
          setActiveTab(prev);
          break;

        case "ArrowRight": // right arrow

        case "ArrowDown":
          // down arrow
          e.preventDefault();
          var next = tab.filter("." + activeClass).next().length ? tab.filter("." + activeClass).next().attr("aria-controls") : tab.first().attr("aria-controls");
          setActiveTab(next);
          break;
      }
    } // 2. Defaults - activate the first tab
    // 1. Check if default tab should be specified by the URL


    var targetTabID;
    var urlParams = new URLSearchParams(window.location.search);

    if (window.location.hash) {
      var hash = window.location.hash.substring(1);

      if (tab.filter('[aria-controls="' + hash + '"]').length) {
        targetTabID = hash;
        setActiveTab(targetTabID, false); // setTimeout(() => {
        //   document.getElementById(hash).closest("section").scrollIntoView();
        // }, 50);
      }
    } else if (urlParams !== null && urlParams !== void 0 && urlParams.get("tab") && tab.filter('[aria-controls="' + urlParams.get("tab") + '"]').length) {
      setActiveTab(urlParams.get("tab"), false);
    } else if (!tab.filter("." + activeClass).length) {
      targetTabID = tab.first().attr("aria-controls"); // 2. Set active the default tab

      setActiveTab(targetTabID, false);
    } // 3. Handle click


    tab.on("click", handleTabClick); // 4. Handle Keyboard navigation

    $(tablist).on("keyup", handleKeyboardNavigation);
    $(window).on("hashchange", function () {
      if (useHash && window.location.hash) {
        var hash = window.location.hash.substring(1);

        if (tab.filter('[aria-controls="' + hash + '"]').length) {
          setActiveTab(hash);
        }
      }
    });
  });
}
/**
 * Initializes Accordions
 * @param {string} [el = .accordions] - Element selector
 */


function accordions() {
  var el = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : ".accordions";
  var elements = document.querySelectorAll(el); // 1. Loop through all accordion sets (accordions)

  var _loop2 = function _loop2(i) {
    var accordionGroup = elements[i];
    if (accordionGroup.classList.contains("accordions-initialized")) return {
      v: void 0
    };
    var allowMultiple = accordionGroup.hasAttribute("data-allow-multiple");
    var accordions = accordionGroup.querySelectorAll(".accordion"); // 2. Loop through all accordion within the set

    var _loop3 = function _loop3(j) {
      var accordion = accordions[j];
      var trigger = accordion.querySelector("[aria-controls]");
      if (trigger && !trigger.hasAttribute("aria-expanded")) trigger.setAttribute("aria-expanded", "false"); // 3. Handle click event on the accordion head

      trigger.addEventListener("click", function (e) {
        e.preventDefault();
        var body = accordion.querySelector("#" + e.currentTarget.getAttribute("aria-controls"));

        if (!allowMultiple) {
          // 4. Close all other accordions if necessary
          for (var k = 0; k < accordions.length; k++) {
            if (accordions[k] !== e.currentTarget.closest(".accordion") && accordions[k].classList.contains("active")) {
              var accordionTrigger = accordions[k].querySelector("[aria-controls]");
              var accordionBody = accordions[k].querySelector("[aria-labelledby]");
              accordions[k].classList.remove("active");
              accordionTrigger.setAttribute("aria-expanded", "false");
              accordionBody.removeAttribute("role");
              slideUp(accordionBody);
            }
          }
        } // 4. Toggle clicked accordion


        accordion.classList.toggle("active");

        if (accordion.classList.contains("active")) {
          slideDown(body);
          trigger.setAttribute("aria-expanded", "true");
          body.setAttribute("role", "region");
        } else {
          slideUp(body);
          trigger.setAttribute("aria-expanded", "false");
          body.removeAttribute("role");
        }
      });
    };

    for (var j = 0; j < accordions.length; j++) {
      _loop3(j);
    }

    accordionGroup.classList.add("accordions-initialized");
  };

  for (var i = 0; i < elements.length; i++) {
    var _ret = _loop2(i);

    if (_typeof(_ret) === "object") return _ret.v;
  }
}
/**
 * Initializes Horizontal scroll
 * @param {string} element - Element selector
 */


function horizontalScroll(element) {
  $(element).each(function () {
    if (!$(this).closest(".overflow-wrapper").length) {
      $(this).wrap('<div class="overflow-wrapper"></div>');
    }

    if (!$(this).hasClass("overflow-ready")) {
      $(this).addClass("overflow-ready--x");
    }

    scrollOverflow($(this));
  }).scroll(function () {
    scrollOverflow($(this));
  });
  $(window).on("resize", function () {
    setTimeout(function () {
      $(element).each(function () {
        scrollOverflow($(this));
      });
    }, 500);
  });

  function scrollOverflow(el) {
    var wrap = el.closest(".overflow-wrapper");

    if (el[0].scrollWidth > el[0].clientWidth) {
      // your element have overflow
      wrap.addClass("overflow--x");

      if (el.scrollLeft() > 0) {
        wrap.addClass("overflow--left");
      } else {
        wrap.removeClass("overflow--left");
      }

      if (el.scrollLeft() < el.get(0).scrollWidth - el.outerWidth() - 10) {
        wrap.addClass("overflow--right");
      } else {
        wrap.removeClass("overflow--right");
      }
    } else {
      // your element doesn't have overflow
      wrap.removeClass("overflow--x");
    }
  }
}
/**
 * Read More by Elements
 */


function readMoreInit() {
  var elements = document.querySelectorAll("[data-rm]");

  for (var i = 0; i < elements.length; i++) {
    var el = elements[i];
    var xs = el.getAttribute("data-rm-xs");
    var sm = el.getAttribute("data-rm-sm");
    var md = el.getAttribute("data-rm-md");
    var lg = el.getAttribute("data-rm-lg");
    var moreText = el.getAttribute("data-rm-more") || "Read More";
    var lessText = el.getAttribute("data-rm-less") || "View Less";
    var options = [];

    if (xs) {
      options.push({
        numberToShow: parseInt(xs),
        viewportSize: 767
      });
    }

    if (sm) {
      options.push({
        numberToShow: parseInt(sm),
        viewportSize: 1024
      });
    }

    if (md) {
      options.push({
        numberToShow: parseInt(md),
        viewportSize: 1749
      });
    }

    if (lg) {
      options.push({
        numberToShow: parseInt(lg),
        viewportSize: 9999
      });
    }

    if (options.length) {
      readMore(el, options, moreText, lessText);
    }
  }
}

function readMore(eleWrapper, arrayOfObjects, readMore, readLess) {
  // sort from smallest to largest
  var sortedSizes = arrayOfObjects.sort(function (a, b) {
    return a.viewportSize < b.viewportSize ? -1 : 1;
  });
  var readMoreLabelInactive = readMore;
  var readMoreLabelActive = readLess; // get all the sizes from the object

  var sizes = [];
  sortedSizes.forEach(function (_ref) {
    var numberToShow = _ref.numberToShow,
        viewportSize = _ref.viewportSize;
    sizes.push(viewportSize);
  }); // each ele so everything is relative to each item we call this on

  window.addEventListener("load", function () {
    return readMoreInit(eleWrapper);
  });
  window.addEventListener("resize", function () {
    return readMoreInit(eleWrapper);
  });

  function readMoreInit(eleWrapper) {
    var sizeToUse = sizes.filter(function (num) {
      return window.innerWidth <= num;
    })[0]; // get the viewport size needed

    if (sizeToUse !== undefined) {
      var numberOfElementsToShow = sortedSizes.find(function (item) {
        return item.viewportSize == sizeToUse;
      });
    } // if out of size range and read more is active


    if (!sizeToUse && eleWrapper.classList.contains("active")) {
      deconstructReadMore(eleWrapper);
    } // if inside of size range and read more is active


    if (!sizeToUse && eleWrapper.classList.contains("active")) {
      // check to see if current size is same as before or if changed sizes
      if (!eleWrapper.classList.contains(sizeToUse)) {
        deconstructReadMore(eleWrapper);
        constructReadMore(eleWrapper, numberOfElementsToShow.numberToShow, sizeToUse);
      }
    } // if size is in range and read more isn't active


    if (sizeToUse !== undefined && !eleWrapper.classList.contains("active")) {
      constructReadMore(eleWrapper, numberOfElementsToShow.numberToShow, sizeToUse);
    }
  }

  function constructReadMore(eleWrapper, num, className) {
    eleWrapper.classList.add("active");
    eleWrapper.classList.add(className); // get set of children

    var children = eleWrapper.children;

    if (children.length <= num) {
      return;
    }

    var childrenArray = Array.from(children); // break them up into their own seperate parts
    // TODO: this is still in jQuery

    var shownEles = childrenArray.slice(0, num);
    $(shownEles).wrapAll('<div class="shown-elements"></div>'); // break them up into their own seperate parts

    var hiddenEles = childrenArray.slice(num);
    $(hiddenEles).wrapAll('<div class="hidden-elements"><div class="hidden-elements-wrapper"></div></div>');
    eleWrapper.querySelector(".hidden-elements").style.display = "none"; // wrap the rest with the container and add button
    // TODO: this is still in jQuery

    $(eleWrapper).children().wrapAll('<div class="read-more-container"></div>');
    eleWrapper.querySelector(".read-more-container").insertAdjacentHTML("beforeend", "<div class=\"view-more-container end-xs\">\n        <button type=\"button\" class=\"button-view-more button-icon\">\n          <span>".concat(readMoreLabelInactive, "</span>\n          <svg width=\"11\" height=\"6\" viewBox=\"0 0 11 6\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n            <path d=\"M1.675 0L5.5 3.7085L9.325 0L10.5 1.1417L5.5 6L0.5 1.1417L1.675 0Z\" fill=\"currentColor\"/>\n          </svg>\n        </button>\n      </div>"));

    function handleButtonClick(e) {
      e.preventDefault();
      eleWrapper.querySelector(".read-more-container").classList.toggle("active");
      slideToggle(eleWrapper.querySelector(".hidden-elements"));
      e.currentTarget.classList.toggle("active");

      if (e.currentTarget.classList.contains("active")) {
        e.currentTarget.querySelector("span").innerText = readMoreLabelActive;
      } else {
        e.currentTarget.querySelector("span").innerText = readMoreLabelInactive;
      }
    }

    eleWrapper.querySelector(".view-more-container button").addEventListener("click", handleButtonClick);
  }

  function deconstructReadMore(eleWrapper) {
    // TODO: In jQuery
    $(eleWrapper).find(".hidden-elements-wrapper").children().unwrap();
    $(eleWrapper).find(".hidden-elements").children().unwrap();
    $(eleWrapper).find(".shown-elements").children().unwrap();
    $(eleWrapper).find(".read-more-container").children().unwrap();
    $(eleWrapper).find(".view-more-container").remove();
    sizes.map(function (item) {
      $(eleWrapper).removeClass("" + item);
    });
    $(eleWrapper).removeClass("active");
  }
}

function faqsSearch() {
  var form = document.getElementById("faqs-searchform");
  var input = document.getElementById("faqs-searchinput");
  if (!form || !input) return;
  var wrapper = document.querySelector(".faqs__accordions"); // Prep for when filter yields no result

  var node = document.createElement("H6");
  var textNode = document.createTextNode("Your search yields no result. Please adjust your search query.");
  node.appendChild(textNode);
  node.classList.add("no-results");
  node.style.display = "none";
  wrapper.appendChild(node);
  var timer = null;
  input.addEventListener("keyup", function () {
    var filter = input.value.toUpperCase();
    clearTimeout(timer);
    timer = setTimeout(function () {
      filterTexts(filter);
    }, 250);
  });
  input.addEventListener("search", function () {
    var filter = input.value.toUpperCase();
    filterTexts(filter);
  });
  form.addEventListener("submit", function (e) {
    e.preventDefault();
    var filter = input.value.toUpperCase();
    filterTexts(filter);
  });

  function filterTexts(filter) {
    var faqs = wrapper.querySelectorAll(".accordion");
    var visibleCount = 0;

    for (var i = 0; i < faqs.length; i++) {
      var faq = faqs[i];
      var txtValue = faq.textContent || faq.innerText;

      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        faq.style.display = "";
        visibleCount++;
      } else {
        faq.style.display = "none";
      }
    }

    if (visibleCount === 0) {
      wrapper.querySelector(".no-results").style.display = "";
    } else {
      wrapper.querySelector(".no-results").style.display = "none";
    }
  }
}
/**
 * Handles LOGOS page articles Search and Pagination
 * using AJAX requests
 */


function logosArticles() {
  var containers = document.querySelectorAll("[data-js='articles-list']");

  for (var i = 0; i < containers.length; i++) {
    init(containers[i]);
  }

  function init(container) {
    var formId = container.getAttribute("data-form-id");
    var articlesKey = container.getAttribute("data-articles");
    var articles = window[articlesKey];

    if (!container || !formId || !(articles !== null && articles !== void 0 && articles.length)) {
      return;
    }

    var searchForm = document.getElementById(formId);
    var searchInput = searchForm.querySelector("input[type='search']");

    function fetchArticles(_x, _x2) {
      return _fetchArticles.apply(this, arguments);
    }

    function _fetchArticles() {
      _fetchArticles = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(search, paged) {
        var params, response, html;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                params = {
                  action: "lb_logos_articles",
                  articles: encodeURIComponent(JSON.stringify(articles)),
                  search: search,
                  paged: paged
                }; // set history

                history.replaceState(_objectSpread({}, params), "");
                _context2.next = 4;
                return fetch("".concat(siteData.ajaxUrl), {
                  method: "POST",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  body: new URLSearchParams(params).toString()
                });

              case 4:
                response = _context2.sent;
                _context2.next = 7;
                return response.text();

              case 7:
                html = _context2.sent;
                removeChildren(container);
                container.insertAdjacentHTML("beforeend", html);
                setPaginationListener();

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));
      return _fetchArticles.apply(this, arguments);
    }

    function setPaginationListener() {
      var pagination = container.querySelectorAll(".page-numbers[data-page]");

      for (var _i = 0; _i < pagination.length; _i++) {
        pagination[_i].addEventListener("click", handlePaginationClick);
      }
    }

    function handleFormSubmit(_x3) {
      return _handleFormSubmit.apply(this, arguments);
    }

    function _handleFormSubmit() {
      _handleFormSubmit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(e) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                e.preventDefault();
                _context3.next = 3;
                return fetchArticles(searchInput.value, 1);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));
      return _handleFormSubmit.apply(this, arguments);
    }

    function handlePaginationClick(_x4) {
      return _handlePaginationClick.apply(this, arguments);
    }

    function _handlePaginationClick() {
      _handlePaginationClick = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(e) {
        var targetPage;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                targetPage = e.currentTarget.getAttribute("data-page");
                _context4.next = 3;
                return fetchArticles(searchInput.value, parseInt(targetPage));

              case 3:
                maybeScrollToElementTop(container);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));
      return _handlePaginationClick.apply(this, arguments);
    }

    var initSearch = debounce( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return fetchArticles(searchInput.value, 1);

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })), 1000);

    function handleInputKeyup(e) {
      if (e.currentTarget.value.toLowerCase() === history.state.search.toLowerCase()) {
        return;
      }

      initSearch();
    }

    function handleInputClear() {
      return _handleInputClear.apply(this, arguments);
    }

    function _handleInputClear() {
      _handleInputClear = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return fetchArticles("", 1);

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));
      return _handleInputClear.apply(this, arguments);
    }

    searchForm.addEventListener("submit", handleFormSubmit);
    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);
    setPaginationListener();
  }
}
/**
 * Media Posts AJAX
 */


function mediaPosts() {
  var container = document.querySelector("#media-posts");

  if (!container) {
    return;
  } // Constants


  var searchForms = document.querySelectorAll(".media-posts-searchform");
  var dateLinks = document.querySelectorAll(".widget-year a");
  var categories = document.querySelectorAll("#category-filter a"); // Lays out functions

  function updateState(newState) {
    var updatedState = _objectSpread(_objectSpread({}, history.state), newState); // Build query string


    var queryString = _objectSpread({}, updatedState); // Remove empty values


    Object.keys(queryString).forEach(function (k) {
      return (!queryString[k] || k === "paged" && queryString[k] === 1) && delete queryString[k];
    });
    var urlQueryString = queryString ? "?".concat(new URLSearchParams(queryString).toString()) : "";
    var newUrl = "".concat(window.location.protocol, "//").concat(window.location.host, "/media").concat(urlQueryString);
    history.pushState(updatedState, "", newUrl);
  }

  function loadPosts() {
    return _loadPosts.apply(this, arguments);
  }
  /**
   * Handle Pagination Click events
   * @param {MouseEvent} e
   */


  function _loadPosts() {
    _loadPosts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
      var params, response, html;
      return regeneratorRuntime.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              params = {
                action: "lb_media_posts",
                paged: history.state.paged,
                category: history.state.category,
                year: history.state.year,
                month: history.state.month,
                search: history.state.search
              };
              _context7.next = 3;
              return fetch("".concat(siteData.ajaxUrl), {
                method: "POST",
                credentials: "same-origin",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: new URLSearchParams(params).toString()
              });

            case 3:
              response = _context7.sent;
              _context7.next = 6;
              return response.text();

            case 6:
              html = _context7.sent;
              removeChildren(container);
              container.insertAdjacentHTML("beforeend", html);
              setPaginationListener(); // we need to set listener again as pagination HTML is replaced on the DOM

            case 10:
            case "end":
              return _context7.stop();
          }
        }
      }, _callee7);
    }));
    return _loadPosts.apply(this, arguments);
  }

  function handlePaginationClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault(); // 2. Get the URL from href

    var href = e.currentTarget.href; // 3. Parse href to get the page number

    var parse = parseWpUrl(href);
    var paged = parse.paged; // 4. If page number found, update history

    updateState({
      paged: paged
    }); // 5. Load the posts

    loadPosts();
  }
  /**
   * Handle Category Click events
   * @param {MouseEvent} e
   */


  function handleCategoryClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault(); // 2. Get the URL from href

    var href = e.currentTarget.href; // 3. Parse href to get the page number
    // There's an exception for "Events" as this is post_type instead of category

    var parse = href.includes("/calendar") ? {
      category: "events"
    } : parseWpUrl(href);
    var category = parse.category || ""; // 4. If page number found, update history

    updateState({
      category: category,
      paged: 1
    }); // 5. Set active state

    setActiveCategory(e.currentTarget); // 6. Load the posts

    loadPosts();
  }
  /**
   * Handle Date Archive Click events
   * @param {MouseEvent} e
   */


  function handleDateArchiveClick(e) {
    // 1. Prevent default browser navigation
    e.preventDefault(); // 2. Get the URL from href

    var href = e.currentTarget.href; // 3. Parse href to get the page number

    var parse = parseWpUrl(href);
    var year = parse.year;
    var month = parse.month; // 4. Check if this is already active, in which case, remove date filter

    if (year === history.state.year && month === history.state.month) {
      updateState({
        year: null,
        month: null
      });
      setActiveDateArchive(null);
    } else {
      // 4. If page number found, update history
      updateState({
        year: year,
        month: month,
        paged: 1
      }); // 5. Set active state

      setActiveDateArchive(e.currentTarget);
    } // 6. Load the posts


    loadPosts();
  }
  /**
   * Listens to Pagination click
   */


  function setPaginationListener() {
    var paginations = document.querySelectorAll("a.page-numbers");

    if (!paginations.length) {
      return;
    }

    for (var i = 0; i < paginations.length; i += 1) {
      paginations[i].addEventListener("click", handlePaginationClick);
    }
  }
  /**
   * Listens to category filter click
   */


  function setCategoryListener() {
    if (!categories.length) {
      return;
    }

    for (var i = 0; i < categories.length; i += 1) {
      categories[i].addEventListener("click", handleCategoryClick);
    }
  }
  /**
   * Sets active state on category links
   * @param {Element} element Active element
   */


  function setActiveCategory(element) {
    if (!categories.length) {
      return;
    }

    for (var i = 0; i < categories.length; i += 1) {
      if (categories[i] === element) {
        categories[i].classList.add("active");
      } else {
        categories[i].classList.remove("active");
      }
    }
  }

  function setDateArchiveListener() {
    if (!dateLinks.length) {
      return;
    }

    for (var i = 0; i < dateLinks.length; i += 1) {
      dateLinks[i].addEventListener("click", handleDateArchiveClick);
    }
  }
  /**
   * Sets active state on date links
   * @param {Element} element Active element
   */


  function setActiveDateArchive(element) {
    if (!dateLinks.length) {
      return;
    }

    for (var i = 0; i < dateLinks.length; i += 1) {
      if (dateLinks[i] === element) {
        dateLinks[i].classList.add("active");
      } else {
        dateLinks[i].classList.remove("active");
      }
    }

    var parents = dateLinks[0].closest(".accordions").querySelectorAll(".accordion > button");

    for (var _i2 = 0; _i2 < parents.length; _i2 += 1) {
      var activeChild = parents[_i2].closest(".accordion").querySelector("a.active");

      if (activeChild) {
        parents[_i2].classList.add("active");
      } else {
        parents[_i2].classList.remove("active");
      }
    }
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    var searchInput = e.currentTarget.querySelector('input[type="search"]');
    updateState({
      search: searchInput.value
    });
    loadPosts();
  }

  var initSearch = debounce( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            updateState({
              search: history.state.searchQuery
            });
            _context6.next = 3;
            return loadPosts();

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  })), 1000);

  function handleInputKeyup(e) {
    if (e.currentTarget.value.toLowerCase() === history.state.search.toLowerCase()) {
      return;
    }

    history.state.searchQuery = e.currentTarget.value;
    initSearch();
  }

  function handleInputClear() {
    updateState({
      search: ""
    });
    loadPosts();
  }

  function init() {
    // 1. Set default state
    var parse = parseWpUrl(window.location.href);
    history.replaceState({
      paged: parse.paged || 1,
      category: parse.category || "",
      search: parse.search || "",
      year: parse.year || null,
      month: parse.month || null
    }, "");

    for (var i = 0; i < searchForms.length; i++) {
      var searchForm = searchForms[i];
      var searchInput = searchForm.querySelector("input[type='search']");
      searchForm.addEventListener("submit", handleFormSubmit);
      searchInput.addEventListener("keyup", handleInputKeyup);
      searchInput.addEventListener("search", handleInputClear);
    }

    setCategoryListener();
    setDateArchiveListener();
    setPaginationListener();
  }

  init();
}
/**
 * Research Posts AJAX
 */


function researchPosts() {
  var container = document.querySelector("#research-posts");

  if (!container) {
    return;
  } // 1. Listen to all pagination clicks on each section


  var sections = document.querySelectorAll("[data-js-id]");

  if (!sections.length) {
    return;
  }

  function loadPosts(_x5, _x6) {
    return _loadPosts2.apply(this, arguments);
  }

  function _loadPosts2() {
    _loadPosts2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(args, section) {
      var params, response, html;
      return regeneratorRuntime.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              params = _objectSpread({
                action: "lb_research_posts"
              }, args);
              _context8.next = 3;
              return fetch("".concat(siteData.ajaxUrl), {
                method: "POST",
                credentials: "same-origin",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: new URLSearchParams(params).toString()
              });

            case 3:
              response = _context8.sent;
              _context8.next = 6;
              return response.text();

            case 6:
              html = _context8.sent;
              removeChildren(section);
              section.insertAdjacentHTML("beforeend", html);
              setPaginationListener(section); // we need to set listener again as pagination HTML is replaced on the DOM

            case 10:
            case "end":
              return _context8.stop();
          }
        }
      }, _callee8);
    }));
    return _loadPosts2.apply(this, arguments);
  }

  function handlePaginationClick(e, section) {
    e.preventDefault(); // 1. Get page number

    var _parseWpUrl = parseWpUrl(e.currentTarget.getAttribute("href")),
        paged = _parseWpUrl.paged; // const sectionKey = section.getAttribute("data-js-id");


    var postType = section.getAttribute("data-js-post_type");
    var category = section.getAttribute("data-js-category");
    loadPosts({
      post_type: postType,
      category: category,
      paged: paged
    }, section);
  }

  function setPaginationListener(section) {
    var paginations = section.querySelectorAll("a.page-numbers");

    if (paginations.length) {
      for (var i = 0; i < paginations.length; i++) {
        paginations[i].addEventListener("click", function (e) {
          return handlePaginationClick(e, section);
        });
      }
    }
  }

  for (var i = 0; i < sections.length; i++) {
    setPaginationListener(sections[i]);
  }
}
/**
 * Faculty / Staff Posts AJAX
 */


function facultyPosts() {
  var container = document.querySelector("#staff-members");

  if (!container) {
    return;
  } // 1. Listen to all pagination clicks on each section


  var sections = document.querySelectorAll("[data-js-id]");

  if (!sections.length) {
    return;
  }

  function loadPosts(_x7, _x8) {
    return _loadPosts3.apply(this, arguments);
  }

  function _loadPosts3() {
    _loadPosts3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(args, section) {
      var params, response, html;
      return regeneratorRuntime.wrap(function _callee9$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              params = _objectSpread({
                action: "lb_staff_posts"
              }, args);
              _context9.next = 3;
              return fetch("".concat(siteData.ajaxUrl), {
                method: "POST",
                credentials: "same-origin",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: new URLSearchParams(params).toString()
              });

            case 3:
              response = _context9.sent;
              _context9.next = 6;
              return response.text();

            case 6:
              html = _context9.sent;
              removeChildren(section);
              section.insertAdjacentHTML("beforeend", html);
              setPaginationListener(section); // we need to set listener again as pagination HTML is replaced on the DOM

            case 10:
            case "end":
              return _context9.stop();
          }
        }
      }, _callee9);
    }));
    return _loadPosts3.apply(this, arguments);
  }

  function handlePaginationClick(e, section) {
    e.preventDefault(); // 1. Get page number

    var _parseWpUrl2 = parseWpUrl(e.currentTarget.getAttribute("href")),
        paged = _parseWpUrl2.paged; // 2. Get category


    var category = section.getAttribute("data-js-id");
    loadPosts({
      category: category,
      paged: paged
    }, section);
  }

  function setPaginationListener(section) {
    var paginations = section.querySelectorAll("a.page-numbers");

    if (paginations.length) {
      for (var i = 0; i < paginations.length; i++) {
        paginations[i].addEventListener("click", function (e) {
          return handlePaginationClick(e, section);
        });
      }
    }
  }

  for (var i = 0; i < sections.length; i++) {
    setPaginationListener(sections[i]);
  }
}
/**
 * Submits form when "clear" / "x" is clicked on input[type='search']
 * @returns void
 */


function searchClear() {
  var inputs = document.querySelectorAll("input[type='search']#research-searchform-input");

  if (!inputs.length) {
    return;
  }

  var _loop4 = function _loop4(i) {
    var input = inputs[i];
    var form = input.closest("form");
    input.addEventListener("search", function (e) {
      form.submit();
    });
  };

  for (var i = 0; i < inputs.length; i++) {
    _loop4(i);
  }
}
/**
 * Handle YouTube playlist pagination
 */


function youtubePlaylistPagination() {
  var wrappers = document.querySelectorAll("[data-playlist-id], [data-video-id]");

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    var wrapper = el;
    var playlistId = el.getAttribute("data-playlist-id");
    var videoId = el.getAttribute("data-video-id");

    if (!wrapper || !playlistId && !videoId) {
      return;
    }

    function loadPlaylist(_x9, _x10, _x11) {
      return _loadPlaylist.apply(this, arguments);
    }

    function _loadPlaylist() {
      _loadPlaylist = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(ids, token, wrapper) {
        var params, response, html;
        return regeneratorRuntime.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                params = {
                  action: "lb_youtube_playlist",
                  page_token: token
                };

                if (ids.playlistId) {
                  params.playlist_id = playlistId;
                }

                if (ids.videoId) {
                  params.video_id = videoId;
                }

                _context10.next = 5;
                return fetch("".concat(siteData.ajaxUrl), {
                  method: "POST",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  body: new URLSearchParams(params).toString()
                });

              case 5:
                response = _context10.sent;
                _context10.next = 8;
                return response.text();

              case 8:
                html = _context10.sent;
                removeChildren(wrapper);
                wrapper.insertAdjacentHTML("beforeend", html);
                setListener();

              case 12:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }));
      return _loadPlaylist.apply(this, arguments);
    }

    function handlePaginationClick(e) {
      e.preventDefault();
      var token = e.currentTarget.getAttribute("data-token");

      if (!token) {
        return;
      }

      loadPlaylist({
        playlistId: playlistId,
        videoId: videoId
      }, token, wrapper);
    }

    function setListener() {
      var prev = wrapper.querySelector(".page-numbers.prev");
      var next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }

      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  } // Initialize


  for (var i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}
/**
 * Handle gallery initialization & pagination
 */


function gallery() {
  var wrappers = document.querySelectorAll(".post-gallery-wrapper");

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    var wrapper = el;

    function initializeLightGallery() {
      var gallery = el.querySelector(".lightgallery");

      if (!gallery) {
        return;
      }

      lightGallery(gallery, {
        plugins: [lgThumbnail],
        speed: 250,
        licenseKey: "0000-0000-000-0000",
        selector: ".post-gallery__item"
      });
    }

    initializeLightGallery();

    function loadGallery(_x12, _x13, _x14) {
      return _loadGallery.apply(this, arguments);
    }

    function _loadGallery() {
      _loadGallery = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(galleryData, page, wrapper) {
        var params, response, html;
        return regeneratorRuntime.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                params = _objectSpread({
                  action: "lb_gallery",
                  paged: page
                }, galleryData);
                _context11.next = 3;
                return fetch("".concat(siteData.ajaxUrl), {
                  method: "POST",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  body: new URLSearchParams(params).toString()
                });

              case 3:
                response = _context11.sent;
                _context11.next = 6;
                return response.text();

              case 6:
                html = _context11.sent;
                removeChildren(wrapper);
                wrapper.insertAdjacentHTML("beforeend", html);
                initializeLightGallery();
                setListener();

              case 11:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }));
      return _loadGallery.apply(this, arguments);
    }

    function handlePaginationClick(e) {
      e.preventDefault();
      var galleryData = window._gallery[wrapper.id];

      if (!galleryData) {
        return;
      }

      var page = e.currentTarget.getAttribute("data-page");
      loadGallery(galleryData, parseInt(page), wrapper);
    }

    function setListener() {
      var prev = wrapper.querySelector(".page-numbers.prev");
      var next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }

      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  } // Initialize


  for (var i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}
/**
 * Handle staff's published works pagination
 */


function publishedWorks() {
  var wrappers = document.querySelectorAll(".published-works");

  if (!wrappers.length) {
    return;
  }

  function init(el) {
    var wrapper = el;

    function initializeLightGallery() {
      var gallery = el.querySelector(".lightgallery");

      if (!gallery) {
        return;
      }

      lightGallery(gallery, {
        plugins: [lgThumbnail],
        speed: 250,
        licenseKey: "0000-0000-000-0000",
        selector: ".post-gallery__item"
      });
    }

    initializeLightGallery();

    function loadPosts(_x15) {
      return _loadPosts4.apply(this, arguments);
    }

    function _loadPosts4() {
      _loadPosts4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(page) {
        var field, postId, params, response, html;
        return regeneratorRuntime.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                field = wrapper.getAttribute("data-field");
                postId = wrapper.getAttribute("data-post_id");
                params = {
                  action: "lb_published_works",
                  field: field,
                  post_id: postId,
                  paged: page
                };
                _context12.next = 5;
                return fetch("".concat(siteData.ajaxUrl), {
                  method: "POST",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  body: new URLSearchParams(params).toString()
                });

              case 5:
                response = _context12.sent;
                _context12.next = 8;
                return response.text();

              case 8:
                html = _context12.sent;
                removeChildren(wrapper);
                wrapper.insertAdjacentHTML("beforeend", html);
                initializeLightGallery();
                setListener();

              case 13:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }));
      return _loadPosts4.apply(this, arguments);
    }

    function handlePaginationClick(e) {
      e.preventDefault();
      var page = e.currentTarget.getAttribute("data-page");
      loadPosts(parseInt(page));
    }

    function setListener() {
      var prev = wrapper.querySelector(".page-numbers.prev");
      var next = wrapper.querySelector(".page-numbers.next");

      if (prev) {
        prev.addEventListener("click", handlePaginationClick);
      }

      if (next) {
        next.addEventListener("click", handlePaginationClick);
      }
    }

    setListener();
  } // Initialize


  for (var i = 0; i < wrappers.length; i++) {
    init(wrappers[i]);
  }
}
/**
 * Handle courses search
 */


function coursesList() {
  var searchForm = document.querySelector("#search-course-form form");
  var container = document.querySelector("#courses-list");

  if (!container || !searchForm) {
    return;
  } // Constants


  var searchInput = searchForm.querySelector("#search-course-keyword");
  var semesterCheckboxes = searchForm.querySelectorAll('input[type="checkbox"][name="semester[]"]');
  var categoryCheckboxes = searchForm.querySelectorAll('input[type="checkbox"][name="category[]"]'); // Lays out functions

  function updateState(newState) {
    var updatedState = _objectSpread(_objectSpread({}, history.state), newState); // Build query string


    var queryString = _objectSpread({}, updatedState); // Reformat arrays differently ...


    var categoryArray = queryString.category;
    var semesterArray = queryString.semester;
    delete queryString.category;
    delete queryString.semester; // Remove empty values

    Object.keys(queryString).forEach(function (k) {
      return (!queryString[k] || k === "paged" && queryString[k] === 1) && delete queryString[k];
    });
    var params = new URLSearchParams(queryString);

    if (categoryArray.length) {
      categoryArray.forEach(function (cat) {
        params.append("category[]", cat);
      });
    }

    if (semesterArray.length) {
      semesterArray.forEach(function (sem) {
        params.append("semester[]", sem);
      });
    }

    var urlQueryString = params.toString() ? "?".concat(params.toString()) : "";
    var newUrl = "".concat(window.location.protocol, "//").concat(window.location.host, "/academics/courses").concat(urlQueryString);
    history.pushState(updatedState, "", newUrl);
  }

  function loadPosts() {
    return _loadPosts5.apply(this, arguments);
  }
  /**
   * Handle Pagination Click events
   * @param {MouseEvent} e
   */


  function _loadPosts5() {
    _loadPosts5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
      var params, response, html;
      return regeneratorRuntime.wrap(function _callee14$(_context14) {
        while (1) {
          switch (_context14.prev = _context14.next) {
            case 0:
              params = {
                action: "lb_courses_list",
                paged: history.state.paged,
                category: history.state.category,
                search: history.state.search,
                semester: history.state.semester
              };
              _context14.next = 3;
              return fetch("".concat(siteData.ajaxUrl), {
                method: "POST",
                credentials: "same-origin",
                headers: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                body: new URLSearchParams(params).toString()
              });

            case 3:
              response = _context14.sent;
              _context14.next = 6;
              return response.text();

            case 6:
              html = _context14.sent;
              removeChildren(container);
              container.insertAdjacentHTML("beforeend", html);
              setPaginationListener(); // we need to set listener again as pagination HTML is replaced on the DOM

            case 10:
            case "end":
              return _context14.stop();
          }
        }
      }, _callee14);
    }));
    return _loadPosts5.apply(this, arguments);
  }

  function handlePaginationClick(e) {
    e.preventDefault();
    var paged = e.currentTarget.getAttribute("data-page");
    updateState({
      paged: paged
    });
    loadPosts();
  }
  /**
   * Listens to Pagination click
   */


  function setPaginationListener() {
    var paginations = container.querySelectorAll("button.page-numbers");

    if (!paginations.length) {
      return;
    }

    for (var i = 0; i < paginations.length; i += 1) {
      paginations[i].addEventListener("click", handlePaginationClick);
    }
  }

  function handleSemesterChange() {
    // 1. Get checked values
    var semester = [];

    for (var i = 0; i < semesterCheckboxes.length; i += 1) {
      if (semesterCheckboxes[i].checked && semesterCheckboxes[i].value) {
        semester.push(semesterCheckboxes[i].value);
      }
    } // 2. If value is the same, bail out


    if (history.semester === semester) {
      return;
    } // 3. Update history


    updateState({
      semester: semester,
      paged: 1
    }); // 4. Load the posts

    loadPosts();
  }

  function handleCategoryChange() {
    // 1. Get checked values
    var category = [];

    for (var i = 0; i < categoryCheckboxes.length; i += 1) {
      if (categoryCheckboxes[i].checked && categoryCheckboxes[i].value) {
        category.push(categoryCheckboxes[i].value);
      }
    } // 2. If value is the same, bail out


    if (history.category === category) {
      return;
    } // 3. Update history


    updateState({
      category: category,
      paged: 1
    }); // 4. Load the posts

    loadPosts();
  }

  function handleFormSubmit(e) {
    e.preventDefault();
    updateState({
      search: searchInput.value
    });
    loadPosts();
  }

  var initSearch = debounce( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            updateState({
              search: searchInput.value
            });
            _context13.next = 3;
            return loadPosts();

          case 3:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  })), 1000);

  function handleInputKeyup(e) {
    if (e.currentTarget.value.toLowerCase() === history.state.search.toLowerCase()) {
      return;
    }

    initSearch();
  }

  function handleInputClear() {
    updateState({
      search: ""
    });
    loadPosts();
  }

  function init() {
    // 1. Set default state0
    var parse = parseWpUrl(window.location.href);
    var searchParams = new URLSearchParams(window.location.search);
    history.replaceState({
      paged: parse.paged || 1,
      category: searchParams.get("category") || [],
      semester: searchParams.get("semester") || [],
      search: searchParams.get("search") || ""
    }, "");
    searchForm.addEventListener("submit", handleFormSubmit);
    searchInput.addEventListener("keyup", handleInputKeyup);
    searchInput.addEventListener("search", handleInputClear);
    window.addEventListener("semesterUpdated", handleSemesterChange);
    window.addEventListener("categoryUpdated", handleCategoryChange); // for (let i = 0; i < categoryCheckboxes.length; i += 1) {
    //   categoryCheckboxes[i].addEventListener("change", handleCategoryChange);
    // }

    setPaginationListener();
  }

  init();
}
/**
 * Shop Mobile Filter initialization
 */


function shopMobileFilter() {
  var button = document.getElementById("shop-filter-button");
  if (!button) return;
  var page = document.getElementById("page");
  var widget = document.getElementById("secondary");
  var closeButton = document.getElementById("shop-filter-close-button");

  function handleOutsideClick(e) {
    if (!widget.classList.contains("active")) {
      return;
    }

    var select2 = document.querySelector(".select2-container");

    if (!widget.contains(e.target) && !button.contains(e.target) && !select2.contains(e.target)) {
      closeMobileFilter();
      document.removeEventListener("click", handleOutsideClick);
    }
  }

  function openMobileFilter(e) {
    e.preventDefault();
    widget.classList.add("active");
    page.classList.add("mobile-filter-active");
    document.documentElement.classList.add("noscroll");
    document.addEventListener("click", handleOutsideClick); // Re-initialize all select2 selects
    // Warning, this causes S&F to refresh the search
    // const selects = document.querySelectorAll("select");
    // for (let i = 0; i < selects.length; i++) {
    //   selects[i].dispatchEvent(new Event("change"));
    // }
  }

  function closeMobileFilter() {
    widget.classList.remove("active");
    page.classList.remove("mobile-filter-active");
    document.documentElement.classList.remove("noscroll");
  }

  button.addEventListener("click", openMobileFilter);
  closeButton.addEventListener("click", closeMobileFilter);
  var sfSubmit = document.querySelector('input[name="_sf_submit"]');
  var sfReset = document.querySelector(".search-filter-reset");

  if (sfSubmit) {
    sfSubmit.addEventListener("click", closeMobileFilter);
  }

  if (sfReset) {
    sfReset.addEventListener("click", closeMobileFilter);
  }

  document.body.addEventListener("keyup", function (e) {
    if (e.key === "Escape") {
      closeMobileFilter();
    }
  });
}

function searchCourseCheckboxes() {
  var names = ["semester[]", "category[]"];

  function handleCheckboxesChange(checkboxes) {
    var allCheckboxes = Array.from(checkboxes).filter(function (c) {
      return !c.value;
    });
    var otherCheckboxes = Array.from(checkboxes).filter(function (c) {
      return c.value;
    });

    if (allCheckboxes.length && otherCheckboxes.length) {
      (function () {
        var allCheckbox = allCheckboxes[0]; // when checking "all", uncheck the other checkboxes

        allCheckbox.addEventListener("change", function (e) {
          if (e.currentTarget.checked) {
            for (var i = 0; i < otherCheckboxes.length; i++) {
              otherCheckboxes[i].checked = false;
            }
          }
        }); // when checking other checkboxes and "all" is checked, uncheck "all"

        for (var i = 0; i < otherCheckboxes.length; i++) {
          otherCheckboxes[i].addEventListener("change", function (e) {
            if (e.currentTarget.checked && allCheckbox.checked) {
              allCheckbox.checked = false;
            }
          });
        }
      })();
    }
  }

  function init(name) {
    var checkboxes = document.querySelectorAll("input[type=\"checkbox\"][name=\"".concat(name, "\"]"));

    if (!checkboxes.length) {
      return;
    }

    handleCheckboxesChange(checkboxes);
  }

  for (var i = 0; i < names.length; i += 1) {
    init(names[i]);
  }
}

function searchCourseDropdown() {
  var triggers = document.querySelectorAll("button[data-dropdown]");

  if (!triggers.length) {
    return;
  }

  function getDropdown(trigger) {
    var dropdownId = trigger.getAttribute("data-dropdown");
    var dropdown = document.getElementById(dropdownId);
    return dropdown;
  }

  function closeDropdown(trigger, dropdown) {
    trigger.classList.remove("active");
    dropdown.classList.remove("active");
  }

  function openDropdown(trigger, dropdown) {
    trigger.classList.add("active");
    dropdown.classList.add("active");
    var wrapper = trigger.closest(".search-course__fields__select");
    hideOnClickOutside(trigger, wrapper, function () {
      closeDropdown(trigger, dropdown);
    });
  }

  function handleTriggerClick(e) {
    e.preventDefault();
    var dropdown = getDropdown(e.currentTarget);

    if (dropdown) {
      if (!e.currentTarget.classList.contains("active")) {
        openDropdown(e.currentTarget, dropdown);
      } else {
        closeDropdown(e.currentTarget, dropdown);
      }
    }
  }

  function handleFocusEvent(e, wrapper, trigger) {
    if (e.key === "Tab") {
      var dropdown = getDropdown(trigger);

      if (wrapper.contains(document.activeElement)) {
        openDropdown(trigger, dropdown);
      } else {
        closeDropdown(trigger, dropdown);
      }
    }
  }

  function handleCheckboxesChange(checkboxes, trigger) {
    if (!trigger) {
      return;
    } // Listen to checkboxes change and update the trigger text


    for (var i = 0; i < checkboxes.length; i++) {
      checkboxes[i].addEventListener("change", function (e) {
        var checkedLabels = [];

        if (trigger.hasAttribute("data-event")) {
          window.dispatchEvent(new Event(trigger.getAttribute("data-event")));
        } // 1. Get checked labels


        for (var j = 0; j < checkboxes.length; j++) {
          if (checkboxes[j].checked) {
            var id = checkboxes[j].id;
            var label = document.querySelector("label[for=\"".concat(id, "\"]"));
            var labelString = label.innerText;
            checkedLabels.push(labelString);
          }
        } // 2. Update trigger text


        trigger.innerText = checkedLabels.length ? checkedLabels.join(", ") : trigger.getAttribute("data-placeholder");
      });
    }
  }

  var _loop5 = function _loop5(i) {
    triggers[i].addEventListener("click", handleTriggerClick); // Listen to focus events

    var wrapper = triggers[i].closest(".search-course__fields__select");
    document.addEventListener("keyup", function (e) {
      return handleFocusEvent(e, wrapper, triggers[i]);
    }); // Listen to checkbox changes for better UX

    var checkboxes = wrapper.querySelectorAll('input[type="checkbox"]');
    handleCheckboxesChange(checkboxes, triggers[i]);
  };

  for (var i = 0; i < triggers.length; i++) {
    _loop5(i);
  }
}
/**
 * Detect linked images and set up popups using lity
 * Only applied on single product pages
 */


function imagePopups() {
  // Only apply this to single products to avoid bloat on other pages
  if (!document.body.classList.contains("single-product")) {
    return;
  } // 1. Get all applicable links. Using * selector instead of $ just in case there is query string


  var links = document.querySelectorAll("a[href*='.jpg'], a[href*='.jpeg'], a[href*='.png'], a[href*='.gif']"); // 2. Append data-lity

  links.forEach(function (link) {
    // Check if this link has <img> as it's child
    // TODO: Should we also check if href and src are similar?
    // This is tricky as sometimes WP appends sizes to the filename and the link can be different from the source
    if (link.querySelector("img")) {
      link.setAttribute("data-lity", "");
    }
  });
}
/*********************
HELPER METHODS
*********************/

/**
 * Format number to USD format
 * @param {number} number
 * @returns {string} Formatted currency
 */


function formatCurrency(number) {
  if (number && !isNaN(parseInt(number))) {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD"
    }).format(number);
  }
}
/**
 * Vanilla JS slide up animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */


function slideUp(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 250;
  target.style.transitionProperty = "height, margin, padding";
  target.style.transitionDuration = duration + "ms";
  target.style.boxSizing = "border-box";
  target.style.height = target.offsetHeight + "px";
  target.offsetHeight;
  target.style.overflow = "hidden";
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  window.setTimeout(function () {
    target.style.display = "none";
    target.style.removeProperty("height");
    target.style.removeProperty("padding-top");
    target.style.removeProperty("padding-bottom");
    target.style.removeProperty("margin-top");
    target.style.removeProperty("margin-bottom");
    target.style.removeProperty("overflow");
    target.style.removeProperty("transition-duration");
    target.style.removeProperty("transition-property");
  }, duration);
}
/**
 * Vanilla JS slide down animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */


function slideDown(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 250;
  target.style.removeProperty("display");
  var display = window.getComputedStyle(target).display;
  if (display === "none") display = "block";
  target.style.display = display;
  var height = target.offsetHeight;
  target.style.overflow = "hidden";
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  target.offsetHeight;
  target.style.boxSizing = "border-box";
  target.style.transitionProperty = "height, margin, padding";
  target.style.transitionDuration = duration + "ms";
  target.style.height = height + "px";
  target.style.removeProperty("padding-top");
  target.style.removeProperty("padding-bottom");
  target.style.removeProperty("margin-top");
  target.style.removeProperty("margin-bottom");
  window.setTimeout(function () {
    target.style.removeProperty("height");
    target.style.removeProperty("overflow");
    target.style.removeProperty("transition-duration");
    target.style.removeProperty("transition-property");
  }, duration);
}
/**
 * Vanilla JS slide toggle animation
 * @param {ELement} target  Element to animate
 * @param {number} [duration = 250] Animation duration in milliseconds
 */


function slideToggle(target) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 250;

  if (window.getComputedStyle(target).display === "none") {
    return slideDown(target, duration);
  } else {
    return slideUp(target, duration);
  }
}
/**
 * Checks if the user's device has touch screen
 * @returns {Boolean}
 */


function deviceHasTouchScreen() {
  var hasTouchScreen = false;

  if (typeof window !== "undefined") {
    if ("maxTouchPoints" in navigator) {
      hasTouchScreen = navigator.maxTouchPoints > 0;
    } else if ("msMaxTouchPoints" in navigator) {
      hasTouchScreen = navigator.msMaxTouchPoints > 0;
    } else {
      var mQ = window.matchMedia && matchMedia("(pointer:coarse)");

      if (mQ && mQ.media === "(pointer:coarse)") {
        hasTouchScreen = !!mQ.matches;
      } else if ("orientation" in window) {
        hasTouchScreen = true; // deprecated, but good fallback
      } else {
        // Only as a last resort, fall back to user agent sniffing
        var UA = navigator.userAgent;
        hasTouchScreen = /\b(BlackBerry|webOS|iPhone|IEMobile)\b/i.test(UA) || /\b(Android|Windows Phone|iPad|iPod)\b/i.test(UA);
      }
    }
  }

  return hasTouchScreen;
}
/**0
 * Debounce helper
 * @see https://github.com/angus-c/just/tree/master/packages/function-debounce
 *
 * @param {Function} fn Callback function
 * @param {number} [wait = 0] Debounce wait interval in milliseconds
 * @param {boolean} [callFirst = false] Debounce wait interval in milliseconds
 * @return {Function} Callback function
 */


function debounce(fn, wait, callFirst) {
  var timeout = null;
  var debouncedFn = null;

  var clear = function clear() {
    if (timeout) {
      clearTimeout(timeout);
      debouncedFn = null;
      timeout = null;
    }
  };

  var flush = function flush() {
    var call = debouncedFn;
    clear();

    if (call) {
      call();
    }
  };

  var debounceWrapper = function debounceWrapper() {
    if (!wait) {
      return fn.apply(this, arguments);
    }

    var context = this;
    var args = arguments;
    var callNow = callFirst && !timeout;
    clear();

    debouncedFn = function debouncedFn() {
      fn.apply(context, args);
    };

    timeout = setTimeout(function () {
      timeout = null;

      if (!callNow) {
        var call = debouncedFn;
        debouncedFn = null;
        return call();
      }
    }, wait);

    if (callNow) {
      return debouncedFn();
    }
  };

  debounceWrapper.cancel = clear;
  debounceWrapper.flush = flush;
  return debounceWrapper;
}
/**
 * Efficiently removes children of a parent element
 * @param {Element} el Parent element whose children will be removed
 */


function removeChildren(el) {
  while (el.firstChild) {
    el.removeChild(el.lastChild);
  }
}
/**
 * Removes 'active' class on an element on clicking outside of it
 * @param {Element} element Element which will lose the 'active' class
 * @param {Element} [wrapper] Wrapper element where clicks inside will be ignored. Optional
 * @param {() => void} [cb] Callback executed after outside click is detected. Optional
 */


function hideOnClickOutside(element, wrapper, cb) {
  if (!wrapper) {
    wrapper = element;
  }

  var outsideClickListener = function outsideClickListener(event) {
    // console.log(isVisible(element));
    if (!wrapper.contains(event.target) && isVisible(wrapper)) {
      // or use: event.target.closest(selector) === null
      // element.style.display = "none";
      element.classList.remove("active");

      if (typeof cb === "function") {
        cb();
      }

      removeClickListener();
    }
  };

  var removeClickListener = function removeClickListener() {
    document.removeEventListener("click", outsideClickListener);
  };

  document.addEventListener("click", outsideClickListener);
}
/**
 * Check if an element is visible
 * @param {Element} el Element to check
 * @returns {Boolean}
 */


function isVisible(el) {
  return !!el && !!(el.offsetWidth || el.offsetHeight || el.getClientRects().length);
}
/**
 * May be scroll to element top, if not visible yet
 * @param {Element} el Element to scroll to
 */


function maybeScrollToElementTop(el) {
  var elementTopPosition = el.getBoundingClientRect().top;

  if (elementTopPosition < 0) {
    window.scrollTo({
      top: window.pageYOffset + elementTopPosition - 100,
      behavior: "smooth"
    });
  }
}
/**
 * Parse WP URL and extract query data
 * @param {string} url
 */


function parseWpUrl(url) {
  var pathnames = url.split("/");
  var qs = new URLSearchParams(url.split("?").pop());
  var data = {};

  function isMaybeYear(string) {
    return string.length === 4 && !isNaN(string);
  }

  function isMaybeMonth(string) {
    return string.length === 2 && !isNaN(string);
  }

  for (var i = 0; i < pathnames.length; i += 1) {
    // 1. Check if this contains page query, e.g. /page/2/
    if (pathnames[i] === "page" && pathnames[i + 1] && !isNaN(pathnames[i + 1])) {
      data.paged = parseInt(pathnames[i + 1]);
    } else if (qs.has("paged")) {
      data.paged = qs.get("paged");
    } // 2. Check if this contains category query, e.g. /category/slug/


    if (pathnames[i] === "category" && pathnames[i + 1]) {
      data.category = pathnames[i + 1];
    } else if (qs.has("category")) {
      data.category = qs.get("category");
    } // 3. Check if this contains date query, e.g. /2021/19/


    if (isMaybeYear(pathnames[i]) && isMaybeMonth(pathnames[i + 1])) {
      data.year = parseInt(pathnames[i]);
      data.month = parseInt(pathnames[i + 1]);
    } else if (qs.has("year") && qs.has("month")) {
      data.year = qs.get("year");
      data.month = qs.get("month");
    }
  }

  return data;
}
//# sourceMappingURL=theme.js.map
