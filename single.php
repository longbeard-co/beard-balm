<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();
?>
<div class="container page-m-t section-m-b">
	<div class="row">
		<div class="col-xs-12 col-md-7 col-lg-6 col-lg-offset-1">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<?php while (have_posts()) {
						the_post();
						get_template_part('template-parts/content', get_post_type());
					} ?>
				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
		<div class="col-xs-12 col-md-4 col-md-offset-1 col-lg-3 col-lg-offset-1">
			<?php get_template_part('template-parts/sidebar'); ?>
		</div>
	</div>

	<?php
	get_footer();
