<?php

/**
 * Template name: Alumni
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <section class="container row page-m-t section-m-b intro">
      <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5 col-lg-offset-1">
        <h1>Alumni Network</h1>
        <p><em class="blockquote">The Sheptytsky Institute is proud to have many alumni across the world, from lay students involved in many industries to the head of the Ukrainian Catholic Church himself.</em></p>
        <p>We are proud of our alumni and ever grateful for the support they offer our Institute and its activities.</p>
      </div>
      <div class="xs-hide sm-show col-xs-4 col-md-4 col-md-offset-3 col-lg-4 col-lg-offset-2">
        <div class="intro__img">
          <?php echo wp_get_attachment_image(203, 'large'); ?>
        </div>
      </div>
    </section>

    <section class="container row section-m-t section-m-b events">
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <h2 class="h1">Alumni News & Events</h2>
        <div class="events__wrapper">
          <?php
          $category_slug = 'alumni';
          ?>
          <div class="lb-tabs">
            <div class="tabs" role="tablist">
              <a href="#news" id="tab-news" class="tab active" aria-selected="true" aria-controls="news" role="tab"><span>News</span></a>
              <a href="#events" id="tab-events" class="tab" aria-selected="false" aria-controls="events" role="tab"><span>Events</span></a>
            </div>
            <div class="tab-panels">
              <div id="news" class="tab-panel active" aria-expanded="true" aria-labelledby="tab-news" role="tabpanel">
                <?php
                $args = array(
                  'post_type'           => 'post',
                  // 'category_name'       => $category_slug, // TODO: Enable this once we have posts
                  'post_status'         => 'publish',
                  'posts_per_page'      => 4,
                  'no_found_rows'       => true,
                  'ignore_sticky_posts' => true,
                  'fields'              => 'ids',
                );
                $the_query = new WP_Query($args);
                $the_posts = $the_query->get_posts();
                get_template_part('template-parts/featured-posts', '', ['posts' => $the_posts, 'link' => '/media/', 'post_type' => 'post']);
                ?>
              </div>
              <div id="events" class="tab-panel" aria-expanded="false" aria-labelledby="tab-events" role="tabpanel">
                <?php
                $events_query = tribe_get_events([
                  'posts_per_page' => 4,
                  'start_date'     => 'now',
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'tribe_events_cat',
                      'field' => 'slug',
                      'terms' => $category_slug
                    )
                  ),
                ], true);
                $event_ids = $events_query->get_posts();

                get_template_part('template-parts/featured-posts', '', ['posts' => $event_ids, 'link' => '/calendar/category/alumni/', 'post_type' => 'tribe_events']);
                ?>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    <?php get_template_part('template-parts/subscribe', '', array('class' => 'section-m-t section-m-b', 'type' => 'alumni')); ?>

    <?php
    $phone = '416-926-7133';
    $email = 'sheptytsky@utoronto.ca';
    $address_link = 'https://goo.gl/maps/CkhRh3CXgAgSDx2h8';
    $address_title = 'Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies';
    $address_text = 'University of St. Michael’s College in the University of Toronto<br />
      81 St. Mary Street<br />
      Toronto, ON M5S 1J4<br />
      Canada';
    ?>

    <section class="container row section-m-t section-m-b contact">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div class="contact__intro">
          <h2 class="h1">Alumni Contacts</h2>
          <p class="narrow">To get more information or join our alumni association, please contact the Sheptytsky Institute.</p>
        </div>
        <div class="contact__inner">
          <div class="contact-info-wrapper">
            <div class="contact__row">
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('phone'); ?>
                    <a href="<?php echo phone_to_url($phone); ?>" target="_blank" rel="noopener noreferrer"><?php echo $phone; ?></a>
                  </div>
                </div>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('email'); ?>
                    <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                  </div>
                </div>
              </div>
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('location'); ?>
                    <div class="contact-info__text">
                      <h3 class="h5"><a href="<?php echo $address_link; ?>" target="_blank" rel="noopener noreferrer"><strong><?php echo $address_title; ?></strong></a></h3>
                      <p><?php echo $address_text; ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
