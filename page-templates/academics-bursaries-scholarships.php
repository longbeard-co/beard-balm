<?php

/**
 * Template name: Academics - Bursaries & Scholarships
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$bursaries = array(
  array(
    'title' => 'Ontario Student Opportunity Trust Fund (OSOTF) Sheptytsky Account',
    'body'  => '<p><strong>Eligible:</strong> Ontario resident Sheptytsky Institute students</p>
                <p><strong>Amount awarded:</strong> reviewed on a case-by-case basis</p>
                <p><strong>Applied towards:</strong> University fees, tuition, student employment, housing, living expenses</p>
                <p><strong>Approving Authority:</strong> Director, Sheptytsky Institute</p>',
  ),
  array(
    'title' => 'Sheptytsky Institute Summer Intensive Program Bursary',
    'body'  => '<p><strong>Eligible:</strong> Theology students from the Lviv Theological Academy (Ukraine).</p>
                <p><strong>Amount awarded:</strong> Usually $12,000 US per summer.</p>
                <p><strong>Applied towards:</strong> Full bursary (university fees, Canadian visas, transportation, health, living expenses, books, etc.)</p>
                <p><strong>Approving Authority:</strong> Director, Sheptytsky Institute</p>',
  ),
);

$scholarships = array(
  array(
    'title' => 'Kobrynsky Fund',
    'body' => '<p>Available to any full-time seminarian, or other full-time student in need. This fund is not restricted to graduate students. Graduate students are encouraged to apply for other funds</p>'
  ),
  array(
    'title' => 'Kunyk Endowment Fund',
    'body' => '<p>Fellowship and research grants for graduate students.</p>'
  ),
  array(
    'title' => 'Maksymowicz Scholarship Fund',
    'body' => '<p>Fellowship and research grants for graduate students.</p>'
  ),
  array(
    'title' => 'Shkilnyk-Kingham Endowment Fund',
    'body' => '<p>Completion of graduate work with emphasis on creative/innovative leadership. A publication grant is also available to disseminate stories of the Eastern Christian Churches (ECC) by graduate students.</p>'
  ),
  array(
    'title' => 'Rizok-Bilous Fund',
    'body' => '<p>Available to any full-time student, graduate or other.</p>'
  ),
  array(
    'title' => 'Borecky Fund',
    'body' => '<p>Available to any student who is also a member of the Eparchy of Toronto.</p>'
  ),
  array(
    'title' => 'Kosick Scholarship Fund',
    'body' => '<p>Available to any full-time undergraduate student and may be renewable for the same student annually.</p>'
  ),
);

$person = array(
  'name' => 'Fr. Alexander Laschuk, JCD, PhD',
  'role' => 'Executive Director',
  'email' => 'alexander.laschuk@utoronto.ca',
  'img' => 314,
);

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section id="bursaries" class="container row page-m-t section-m-b bursaries">
      <div class="col-xs-12 col-sm-8 col-md-12 col-lg-10 col-lg-offset-1">
        <h2 class="h1">Bursaries</h2>
        <div class="text-col text-block bursaries__text">
          <p>The Sheptytsky Institute, along with the financial aid committee of the Faculty of Theology, administers the Ontario Student Opportunity Trust Fund (OSOTF) for qualified Ontario residents.</p>
          <p>Other scholarships are available for both full time and part time students and are available through the Metropolitan Andrey Sheptytsky Institute Foundation.</p>
          <p><strong>More information on applying for these opportunities can be obtained from the Institute’s director. <a href="#submissions">Click here</a> to go to submissions.</strong></p>
          <div class="xs-hide md-show">&nbsp;</div>
        </div>
      </div>
      <div class="col-xs-12 col-md-8 col-lg-7 col-lg-offset-1 col-accordions">
        <?php get_template_part('template-parts/accordions', '', array('data' => $bursaries)); ?>
      </div>
      <div class="xs-hide sm-show col-sm-4 col-md-3 col-lg-3 col-lg-offset-1">
        <div class="bursaries__img">
          <?php echo wp_get_attachment_image(299, 'large'); ?>
        </div>
      </div>
    </section>

    <section id="scholarships" class="container row section-m-t section-m-b scholarships">
      <div class="col-xs-12 col-lg-9 col-lg-offset-1">
        <h2 class="h1">Scholarships</h2>
        <p>Scholarships Administered by the MASI Foundation.</p>
        <?php get_template_part('template-parts/vertical-tabs', '', array('data' => $scholarships)); ?>
      </div>
    </section>

    <section id="submissions" class="container row section-m-t section-m-b submissions">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h2 class="h1">Submissions</h2>
        <div class="submissions__inner">
          <ol class="submissions__steps">
            <li>As a follow-up, you will be required to submit additional documents including letters of reference and a current CV in addition to transcripts and an official letter of acceptance into the Program from the Faculty of Theology as USMC.</li>
            <li>You may also be required to submit evidence of “need” if you are applying for any scholarship based upon that requirement.</li>
          </ol>
          <div class="submissions__contact">
            <p>For more information on bursaries please contact:</p>
            <?php echo get_template_part("template-parts/contact-person", '', array('person' => $person)); ?>
          </div>
        </div>
      </div>
    </section>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
