<?php

/**
 * Template name: Academics - Degree Programs
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$courses_list = array(
  array(
    'title' => 'Scripture',
    'units' => 5,
    'courses' => array(
      'Introduction to the Old Testament',
      'Old Testament (Any Course)',
      'Introduction to the New Testament',
      'New Testament: Gospels',
      'New Testament: Paul',
    )
  ),
  array(
    'title' => 'History',
    'units' => 3,
    'courses' => array(
      'Early Church History to 843',
      'History of Christianity II to 1648',
      'Byzantine and Slavic Church History',
    )
  ),
  array(
    'title' => 'Field Education',
    'units' => 4,
    'courses' => array(
      'Theology of Ministry',
      'Theological Field Education Placement',
      'Theological Reflection Seminar',
      'Pastoral Skills Units (3)',
    )
  ),
  array(
    'title' => 'Pastoral Theology',
    'units' => 3,
    'courses' => array(
      'Canonical Tradition of the Christian East',
      'Pastoral: Pastoral Care / Counselling / Spiritual Direction',
      'Eastern Christian Spirituality',
    )
  ),
  array(
    'title' => 'Systematic Theology',
    'units' => 7,
    'courses' => array(
      'Foundations of Eastern Christian Theology',
      'Triune God',
      'Christ the Saviour',
      'Theology: Creation / Grace / Eschatology',
      'Byzantine Sacraments',
      'Eastern Christian Ecclesiology',
      'Introduction to Eastern Christian Worship',
    )
  ),
  array(
    'title' => 'Ethics',
    'units' => 3,
    'courses' => array(
      'Introduction to Eastern Christian Ethics',
      'Eastern Christian Approaches to Contemporary Ethics Issues',
      'Ethics: Justice, or Social Teachings, or Ecological Ethics',
    )
  ),
  array(
    'title' => 'Additional Requirement',
    'units' => 1,
    'courses' => array(
      'Byzantine Eucharistic Liturgy',
    )
  ),
  array(
    'title' => 'Electives',
    'units' => 4,
    'courses' => array(
      '4 &times; electives',
    )
  ),
);


$args = array(
  'post_type'           => 'degree_program',
  'posts_per_page'      => 40,
  'no_found_rows'       => true,
  'ignore_sticky_posts' => true,
  'order'               => 'ASC',
  'orderby'             => 'post_title',
);
$the_query = new WP_Query($args);
$programs_1 = array();
$programs_2 = array();
if ($the_query->have_posts()) {
  $i = 0;
  $divider = ceil($the_query->post_count / 2);
  while ($the_query->have_posts()) {
    $the_query->the_post();
    $program = array(
      'title' => get_the_title(),
      'body' => wpautop(get_the_excerpt()) . '<p class="align-right"><a href="' . get_permalink() . '" class="button"><span></span>Learn More</a></p>',
    );
    if ($i < $divider) {
      $programs_1[] = $program;
    } else {
      $programs_2[] = $program;
    }
    $i++;
  }
}

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b title">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h1>Degree Programs</h1>
        <div class="title__accordions">
          <div class="title__accordions__col">
            <?php get_template_part('template-parts/accordions', '', ['data' => $programs_1]); ?>
          </div>
          <div class="title__accordions__col">
            <?php get_template_part('template-parts/accordions', '', ['data' => $programs_2]); ?>
          </div>
        </div>
      </div>
    </section>


  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
