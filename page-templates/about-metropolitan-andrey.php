<?php

/**
 * Template name: About - Metropolitan Andrey
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();


?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">

      <div class="col-xs-12 col-md-11 col-lg-9 col-lg-offset-1">
        <h1>Metropolitan Andrey Sheptytsky</h1>
        <div class="text-col intro__text" data-rm data-rm-xs="4">
          <p>Metropolitan Andrey Sheptytsky was born in 1865 in the village of Prylbychi. </p>
          <figure class="intro__text__img intro__text__img--mobile sm-hide">
            <?php echo wp_get_attachment_image(1195, 'large'); ?>
            <figcaption class="caption">Andrey Sheptytsky, 1910</figcaption>
          </figure>
          <p>The son of a polonized Ukrainian Aristocrat, the young Count Sheptytsky was able to enter the Order of Saint Basil the Great (OSBM) in 1891 and accepted the monastic name Andrey. </p>
          <p>In 1900 he was made Bishop of Stanyslaviv and at the age of 36 became the Metropolitan. He remained at this post until his death on November 1, 1944.</p>
          <p><em class="blockquote">His life was an example of heroic virtue. An extremely active pastor, who used his personal wealth to fund thousands of philanthropic projects, he was also a man of deep prayer. </em></p>
          <p>He traveled widely, visiting his flock in Western Europe, North and South America, and seeing to it that they would have bishops of their own to take care of them.</p>
          <p>
            <span class="intro__text__img xs-hide sm-show">
              <?php echo wp_get_attachment_image(1195, 'large'); ?>
              <span class="caption">Andrey Sheptytsky, 1910</span>
            </span>

            Never of good health, his last fifteen years were a constant agony of pain and paralysis. Even so, he valiantly led his Church through extremely difficult and oppressive times.
          </p>
          <p>His two great passions in life were the restoration of authentic Eastern Christian Monasticism in his Church, and the union of Churches. He valued education and founded the L’viv Theological Academy in 1929.</p>
          <p>Metropolitan Andrey led his flock of some five million faithful through two world wars. He courageously saved many Jews from the Nazis during World War II. Metropolitan Andrey died as the Red Army occupied his city of L’viv once again in 1944. The cause for his beatification and canonization is underway.</p>
        </div>
      </div>
    </section>
    <?php
    $about_texts = array(
      array(
        'lang'  => 'English',
        'title' => 'Prayer for the Beatification of the Servant of God Metropolitan Andrey',
        'text'  => '<p>Our Lord Jesus Christ – You always reward Your faithful servants, not only with special gifts of Your love, but also with the eternal reward of the saints in heaven, and in many cases You grant them the recognition of sanctity by Your Church here on earth.</p><p>We humbly pray: grant that Your faithful servant Metropolitan Andrey be numbered among the saints. Throughout his just life, “full of suffering and trials,” he was a good shepherd for his flock and a great labourer for Christian unity. And through his beatification and intercession, grant our entire people the great gift of unity and love. Amen.</p>',
      ),
      array(
        'lang'  => 'Ukrainian',
        'title' => 'Молитва за Прославу Слуги Божого Митрополита Андрея',
        'text'  => "<p>Господи Ісусе Христе – Ти завжди нагороджуєш Твоїх вірних слуг не тільки особливішими дарами Своєї любови, але й вічною нагородою святих у небі, а в многих випадках і прославою на Твоїх святих престолах, тут на землі.</p><p>Покірно благаємо Тебе: зволь так прославити Твого вірного слугу Митрополита Андрея. Він упродовж свого праведного життя, „повного терпінь і досвідів”, був добрим пастирем свого стада і великим подвижником церковної єдности. А через його прославу і заступництво пошли і цілому народові нашому великий дар єдности і любови. Амінь.</p>",
      )
    );
    ?>
    <section class="container row section-m-t section-m-b about">
      <div class="about__img">
        <?php echo wp_get_attachment_image(1199, 'full'); ?>
      </div>
      <div class="col-xs-12 col-md-10 col-lg-8 col-lg-offset-1">
        <div class="about__inner">
          <div class="about__card card card--with-cross-img">
            <div class="about__card__img card__img xs-hide sm-show">
              <?php echo wp_get_attachment_image(136, 'large'); ?>
            </div>
            <div class="about__card__text">
              <div class="lb-tabs">
                <?php
                $tabs = '';
                $tab_panels = '';
                $i = 0;
                foreach ($about_texts as $about_text) {
                  $lang = $about_text['lang'];
                  $key   = sanitize_title($lang);
                  $title  = $about_text['title'];
                  $text  = $about_text['text'];
                  $active = $i == 0 ? 'active' : '';
                  $selected = $i == 0 ? 'true' : '';

                  $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$lang</span></a>";
                  $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'><h2 class='about__card__title'>$title</h2><div class='text-col quote-text'>$text</div></div>";

                  $i++;
                }
                ?>
                <div class="tabs tabs--alt" role="tablist">
                  <?php echo $tabs; ?>
                </div>
                <div class="tab-panels">
                  <?php echo $tab_panels; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="container row section-m-t section-m-b prayer">
      <div class="col-xs-12 col-md-10 col-lg-8 col-lg-offset-1">
        <div class="prayer__inner">
          <div class="prayer__card card card--with-cross-img">
            <div class="prayer__card__img card__img xs-hide sm-show">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 66 84">
                <path fill="url(#prayer-cross)" d="M26.9 65.288V42.435h-8.814c-.07 1.054 0 2.107-.211 3.088-.529 2.435-2.574 4.033-4.971 4.106-2.433.036-4.548-1.453-5.183-3.815-.247-.945-.458-1.526-1.622-1.708C2.503 43.416 0 40.146 0 36.476c0-3.632 2.433-6.72 5.994-7.52.881-.181 1.41-.436 1.692-1.49.74-2.942 3.173-4.468 6.029-4.032 2.644.436 4.442 2.761 4.371 5.704v1.199h8.815V19.764c-2.292.327-4.513.255-5.994-1.67-.776-.982-1.375-2.326-1.516-3.561-.388-3.198 1.445-5.232 5.394-6.431C26.16 2.616 28.805 0 33 0c4.23 0 6.945 2.689 8.215 8.102 4.195 1.38 5.958 3.633 5.429 6.976-.529 3.306-2.962 4.904-7.474 4.795v10.5h8.673c.105-1.053.07-2.034.317-2.943.6-2.434 2.609-3.996 5.006-4.032 2.398-.037 4.513 1.49 5.113 3.887.246 1.017.634 1.49 1.692 1.708 3.56.726 5.993 3.778 6.029 7.448.035 3.669-2.468 6.793-6.064 7.702-.6.145-1.34.726-1.517 1.271-.987 3.052-3.49 4.724-6.346 4.106-2.714-.581-4.371-3.197-4.09-6.286.036-.254.036-.472.071-.835h-8.92v22.998c.565 0 1.093-.037 1.587 0 2.574.218 4.619 2.143 4.936 4.614.352 2.834-1.058 5.268-3.667 6.03-1.058.328-1.48.764-1.692 1.89-.67 3.56-3.878 6.14-7.369 6.068-3.525-.037-6.487-2.616-7.298-6.213-.14-.618-.705-1.38-1.234-1.599-2.961-1.053-4.477-3.342-4.019-6.358.388-2.652 2.574-4.432 5.57-4.505.283.073.565 0 .953-.036Z" />
                <defs>
                  <linearGradient id="prayer-cross" x1="33" x2="33" y1="0" y2="84" gradientUnits="userSpaceOnUse">
                    <stop stop-color="#D1A410" />
                    <stop offset="1" stop-color="#FFD64F" />
                  </linearGradient>
                </defs>
              </svg>
            </div>
            <div class="prayer__card__text">
              <h2 class="prayer__card__title">Prayer for the Servant of God of Metropolitan Andrey for Divine Wisdom</h2>
              <div data-rm data-rm-xs="9" data-rm-sm="9" data-rm-md="9" data-rm-lg="9" class="quote-text">
                <p>O Great and Almighty God, send down upon me from Your high and holy heavens and from the throne of Your holy glory, Your holy wisdom, that sits at Your side.</p>
                <p>Grant me the wisdom of Your good pleasure so that in my life I may know how to desire fervently, seek wisely, acknowledge in truth and fulfill perfectly that which is pleasing to You, to the glory and honour of You holy Name, “to the praise of the glory of Your grace.”</p>
                <p>Grant me, O God, the wisdom of my state, so that I may do what You desire; grant that I may understand my obligations, grant me the wisdom of my duties, and grant that I may do them as they ought to be done and as is fitting of Your glory and for the benefit of my soul.</p>
                <p>Grant me the wisdom of Your ways and the wisdom to walk the paths of Your holy will.</p>
                <p>Grant me the wisdom of success and failure so that I would know how not to exalt myself in the former and not to be downcast in the latter.</p>
                <p>Grant me the wisdom of joy and the wisdom of sadness; may I rejoice only in that which leads to You and be sad only in that which separates from You.</p>
                <p>Grant me the wisdom of everything that passes and everything that lasts; may the first decrease in my sight, and the second grow.</p>
                <p>Grant me the wisdom of work and the wisdom of rest; may work for You be luxury for me, and rest without You – fatigue.</p>
                <p>Grant me the wisdom of a sincere and straight-forward intention, the wisdom of simplicity, the wisdom of sincerity. May my heart turn to You and seek You in all things all my life long.</p>
                <!-- Read More -->
                <p>Grant me the wisdom of obedience for Your law, for Your Church.</p>
                <p>Grant me the wisdom of poverty, so that I would never value goods in any other way except according to their real worth.</p>
                <p>Grant me the wisdom of chastity according to my state and vocation.</p>
                <p>Grant me the wisdom of patience, the wisdom of humility, the wisdom of gladness and seriousness, the wisdom of the fear of the Lord; the wisdom of truthfulness and of good deeds; may I be patient with no complaining, humble without the least pretending, joyful without inordinate laughter, serious without severity; that I may fear You without the temptation to despair; that I may be truthful without the shadow of duplicity; may all my good deeds be free from self-complacence.</p>
                <p>Grant me the wisdom to admonish my neighbour when necessary without exalting myself; grant that I may edify in word and deed without hypocrisy.</p>
                <p>Grant me, O Lord, the wisdom of vigilance, attention and wariness; may no vain thought lead me astray.</p>
                <p>Grant me the wisdom of nobleness; may I never be brought down by any impure and unworthy attachment.</p>
                <p>Grant me the wisdom of what is right; may no selfish intention ever lead me away from the path of my duties.</p>
                <p>Grant me the wisdom of courage and strength; may no storm overthrow me.</p>
                <p>Grant me the wisdom of freedom; may no powerful passion ever enslave me.</p>
                <p>Grant me the wisdom of the theological virtues and the moral virtues: faith, hope, love, prudence, devotion, temperance and fortitude.</p>
                <p>Grant me, O Lord, the wisdom of the apostles, the wisdom of the martyrs; grant me a priestly and pastoral wisdom; grant me the wisdom of preachers and teachers; grant me the wisdom of those who administer the Holy Mysteries; grant me the Eucharistic wisdom and mystical wisdom – the wisdom of prayer and spiritual wisdom, and above all, O Lord, grant me the wisdom of sincere repentance, imperfect and perfect contrition; grant me the wisdom of to know myself in my weakness and malice; grant me the wisdom of mortification and fasting; grant me the wisdom of self-denial and self-sacrifice; grant me the wisdom of sacrifice, the wisdom of the Cross, the wisdom of Blood.</p>
                <p>O God, grant me, finally, that wisdom which, in accord with Your holy purpose, leads to the unity of churches under one supreme pastor, the Universal Pontiff; grant me the wisdom to cherish the work of holy unity, to love it and to consecrate my life to it.</p>
                <p>Grant me the wisdom of our Eastern rite, to hold it, to renew it and develop it. Grant me the wisdom of the Fathers of the holy Eastern Church and all the great ecclesiastical teachers.</p>
                <p>Grant me the wisdom of Your great apostle, Paul, so that I would at least well understand his epistles, remember them and know how to explain them to Your people.</p>
                <p>Grant me the wisdom of Your first Vicar that I may understand the designs of Your Divine Providence, which governs the Church through the Roman Pontiffs; grant me the wisdom of obedience to them and to the Universal Catholic Church; grant me the wisdom of church history and theology; grant me the wisdom that I and my people lack; grant me the wisdom of true satisfaction, true happiness. Amen.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
