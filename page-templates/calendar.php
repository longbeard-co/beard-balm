<?php

/**
 * Template name: Calendar
 * Template Post Type: page
 *
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

if (is_singular('tribe_events')) {
  get_template_part('page-templates/event');
  return;
}

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div class="container page-m-t section-m-b">
      <div class="row">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <h1>Calendar</h1>
          <div class="text-block">
            <p>If you have any questions regarding any of these events, please <a href="/contact">reach out</a> to us.</p>
          </div>
          <?php
          while (have_posts()) :
            the_post();
            get_template_part('template-parts/content', 'page');
          endwhile; // End of the loop.
          ?>
        </div>
      </div>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
