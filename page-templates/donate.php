<?php

/**
 * Template name: Donate
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

// $address_link = 'https://goo.gl/maps/CkhRh3CXgAgSDx2h8';
$address_title = 'Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies';
$address_text = 'University of St. Michael’s College in the University of Toronto<br />
      81 St. Mary Street<br />
      Toronto, ON M5S 1J4<br />
      Canada';
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main lb-tabs">
    <section class="container row page-m-t intro">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h1 class="m-b">Support Us</h1>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-5 col-lg-offset-1">
        <div class="intro__text">
          <p>Your support allows the Sheptytsky Institute to continue to bring extraordinary students and faculty together in a dynamic, intellectually-driven learning community that is committed to our Church and its future.</p>
          <p>We are committed to the idea that <strong>funds received from donors are a sacred trust</strong>, to be managed with fiscal prudence and a deep sense of responsibility.</p>
          <p><em class="blockquote">Every gift makes a meaningful impact.</em></p>
          <p>To that end, your donation will help fund the teaching and academic activity of the Sheptytsky Institute’s full-time professors, as well as its sessional lecturers and support staff. It will also contribute to the development of worthwhile educational programs for future generations. </p>
          <p>All donors are welcomed into the Sheptytsky Institute family and <strong>remembered in our prayers</strong> and worship at St Sophia chapel.</p>
        </div>
      </div>
      <div class="col-xs-12 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-0">
        <div class="intro__donate">
          <h2 class="h3">Donate by Mail </h2>
          <p>To donate by mail, please send a check to:</p>
          <div class="contact-info">
            <div class="contact-info__inner">
              <?php the_svg('location'); ?>
              <div class="contact-info__text">
                <h3 class="h5"><strong><?php echo $address_title; ?></strong></h3>
                <p><?php echo $address_text; ?></p>
              </div>
            </div>
          </div>
          <h2 class="h3 m-t donate-online-title">
            <span>Donate Online</span>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 27 30">
              <path fill="#D1A410" d="M26.168 16.63a.626.626 0 0 0-.578-.386h-6.846V.624A.625.625 0 0 0 18.119 0H8.122a.625.625 0 0 0-.625.625v15.62H.625a.626.626 0 0 0-.442 1.066l12.464 12.505a.625.625 0 0 0 .885.002l12.5-12.506a.623.623 0 0 0 .136-.681Z" />
            </svg>
          </h2>
          <div class="tabs tabs--alt tabs--small" role="tablist">
            <a href="#donate-money" id="tab-donate-money" class="tab active" aria-controls="donate-money" aria-selected="true" role="tab"><?php the_svg('money'); ?> Money</a>
            <a href="#donate-securities" id="tab-donate-securities" class="tab" aria-controls="donate-securities" aria-selected="false" role="tab"><?php the_svg('securities'); ?> Securities</a>
          </div>
        </div>
      </div>
    </section>

    <section class="donate">
      <div class="tab-panels">
        <div id="donate-money" class="tab-panel active" role="tabpanel" aria-labelledby="tab-donate-money" aria-expanded="true">
          <script id="ch_cdn_embed" type="text/javascript" src="https://www.canadahelps.org/secure/js/cdf_embed.2.js" charSet="utf-8" data-language="en" data-page-id="64973" data-root-url="https://www.canadahelps.org" data-formtype="1" data-cfasync="false"></script>
          <!-- <script id="ch_cdn_embed" type="text/javascript" src="https://www.canadahelps.org/secure/js/cdf_embed.2.js" charSet="utf-8" data-language="en" data-page-id="64961" data-root-url="https://www.canadahelps.org" data-formtype="4" data-cfasync="false"></script> -->
        </div>
        <div id="donate-securities" class="tab-panel" role="tabpanel" aria-labelledby="tab-donate-securities" aria-expanded="false">
          <iframe src="https://www.canadahelps.org/en/dne/m/64961" width="100%" scrolling="no" id="iFrameResizer0" style="height: 2600px; border: 0px; overflow: hidden;"></iframe>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<!-- <script>
  window.addEventListener('changeTab', function() {
    const money = document.querySelector('#donate-money');
    const securities = document.querySelector('#donate-securities');

    if (securities.classList.contains('active')) {
      removeChildren(money);
      
    }
  })
</script> -->

<?php
get_footer();
