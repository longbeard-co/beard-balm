<?php

/**
 * Template name: Template
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();


?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">

    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
