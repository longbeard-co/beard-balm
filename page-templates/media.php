<?php

/**
 * Template name: Media
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$query_category = isset($_GET['category']) && $_GET['category'] ? $_GET['category'] : '';
$query_search   = isset($_GET['search']) && $_GET['search'] ? $_GET['search'] : '';

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b">
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <div class="title-nav">
          <div class="title-nav__inner">
            <div class="title-nav__title">
              <h1>Media</h1>
            </div>
            <div class="title-nav__search md-hide">
              <?php echo lb_get_search_form('media-posts-searchform-2', 'media-posts-searchform', '/media', 'Search Posts', 'search', $query_search); ?>
            </div>
            <div class="title-nav__nav">
              <?php
              $categories = get_categories(array(
                'orderby'      => 'name',
                'order'        => 'ASC',
                'hide_empty'   => true,
                'exclude_tree' => [1, 36]
              ));
              $category_filter = array_map(function ($category) {
                return array(
                  'name' => $category->name,
                  'link' => get_category_link($category),
                  'slug' => $category->slug
                );
              }, $categories);
              $category_filter[] = array(
                'name' => 'Events',
                'link' => tribe_get_events_link(),
                'slug' => 'events'
              );
              function sort_categories($a, $b) {
                $sort = array('news', 'events', 'blogs', 'press', 'video', 'audio');
                $a_index = array_search($a['slug'], $sort);
                $b_index = array_search($b['slug'], $sort);
                if ($a_index === false) {
                  return 1;
                }
                if ($b_index === false) {
                  return -1;
                }
                return ($a_index < $b_index) ? -1 : 1;
              }
              usort($category_filter, 'sort_categories');
              ?>
              <ul id="category-filter">
                <li>
                  <a href="/" class="<?php echo '' == $query_category ? 'active' : ''; ?>">All</a>
                </li>
                <?php foreach ($category_filter as $category) { ?>
                  <li>
                    <a href="<?php echo esc_url($category['link']); ?>" class="<?php echo $category['slug'] == $query_category ? 'active' : ''; ?>">
                      <?php echo lb_get_post_category_icon($category['slug']); ?><?php echo $category['name']; ?>
                    </a>
                  </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <div id="media-posts">
          <?php $args = array(); ?>
          <?php get_template_part('template-parts/media-posts-list', '', $args); ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-10 col-md-3 col-lg-3">
        <aside class="widget-area">
          <!-- <section class="widget widget-search xs-hide md-show">
            <?php echo lb_get_search_form('media-posts-searchform', 'media-posts-searchform', '/media', 'Search Posts', 'search', $query_search); ?>
          </section> -->
          <?php get_template_part('template-parts/widget-year-archive'); ?>
        </aside>
      </div>
    </section>

    <?php get_template_part('template-parts/subscribe', '', array(
      'class'     => 'section-m-t section-m-b',
      'type'      => 'media',
      'col_class' => 'col-xs-12 col-lg-8 col-lg-offset-1'
    )); ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
