<?php

/**
 * Template name: Single Event
 * Template Post Type: page
 *
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div class="container page-m-t section-m-b">
      <div class="row">
        <div class="col-xs-12 col-sm-11 col-md-7 col-lg-6 col-lg-offset-1">
          <?php
          while (have_posts()) :
            the_post();
            get_template_part('template-parts/content', 'page');
          endwhile; // End of the loop.
          ?>
        </div>
        <div class="col-xs-12 col-sm-11 col-md-3 col-md-offset-2 col-lg-3 col-lg-offset-1">
          <aside class="widget-area">
            <section class="widget widget-search">
              <?php echo lb_get_search_form('event-searchform', '', '/calendar', 'Search Calendar', 'tribe-bar-search'); ?>
            </section>
            <section class="widget widget-posts-list">
              <h4>Upcoming Events</h4>
              <ul>
                <?php
                // Retrieve the next 5 upcoming events
                $events = tribe_get_events([
                  'posts_per_page' => 5,
                  'start_date'     => 'now',
                ], true);
                if ($events->have_posts()) {
                  while ($events->have_posts()) {
                    $events->the_post();
                    echo '<li>';
                    get_template_part('template-parts/post-entry', '', array(
                      'show_thumbnail' => false,
                      'show_meta'      => true,
                      'show_excerpt'   => false,
                    ));
                    echo '</li>';
                  }
                }
                ?>
              </ul>
            </section>
          </aside>
        </div>
      </div>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
