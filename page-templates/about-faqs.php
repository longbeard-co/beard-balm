<?php

/**
 * Template name: About - FAQs
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$faqs = array(
  array(
    'title' => 'What is MASI?',
    'body'  => '<p>The Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies (MASI) is an autonomous academic unit of the Faculty of Theology of the University of St. Michael’s College (USMC) in the University of Toronto (UofT). It specializes in Eastern Christian Studies, including the theology, spirituality, liturgy, history, and ecclesial polity of the Eastern Churches, both Orthodox and Catholic. The Institute focuses on all four families of Eastern Churches: Eastern Orthodox, Pre-Chalcedonian, Assyrian and Eastern Catholic.</p>',
  ),
  array(
    'title' => 'When was it founded?',
    'body'  => '<p>The Sheptytsky Institute was founded in 1986 by Father Andriy Chirovsky in Chicago. In 1990, MASI moved to Ottawa’s Saint Paul University. In 2016, MASI signed an agreement with USMC paving the way for its move to Toronto. Since July 1, 2017, MASI has been an autonomous academic unit within USMC’s Faculty of Theology and part of the Toronto School of Theology, an ecumenical consortium of 7 colleges.</p>',
  ),
  array(
    'title' => 'Whom is MASI named after?',
    'body'  => '<p>Born into an aristocratic family in western Ukraine, Andrey Sheptytsky became the Metropolitan, the highest-ranking leader of the Greco-Catholic Church in 1901, at the age of 36, and held this position until his death in 1944. He was a man of deep prayer, a gifted teacher, preacher and philanthropist, who valued education (having the equivalent of three doctorates himself). He founded the Lviv Theological Academy in 1929.</p>',
  ),
  array(
    'title' => 'Who can study at MASI?',
    'body'  => '<p>Students of MASI include university students studying at USMC and UofT as well as clergy, church leaders and lay individuals from North America, Ukraine and around the world. One of its most notable former students is Patriarch Sviatoslav Shevchuk of Ukraine, who is MASI’s international patron.</p>',
  ),
  array(
    'title' => 'What courses and programs does MASI offer?',
    'body'  => '<p>The Faculty of Theology offers primarily graduate university degree programs in Eastern Christian Studies. Some undergraduate course offerings are being contemplated at this time.</p>
    <p>At the graduate level there are two professional (pastorally oriented) degrees: M.Div. and M.T.S., with an emphasis on Eastern Christianity, as well as two research-oriented degrees: the M.A. and the Ph.D.</p>
    <p>Four general areas of specialization are represented by MASI professors: Spirituality-Doctrine, Liturgical Studies, Historical Studies, Ecumenism and Eastern Christianity. Some courses are offered online as distance education.</p>',
  ),
  array(
    'title' => 'How is MASI funded?',
    'body'  => '<p>Several income streams support MASI. USMC shares tuition income with MASI and gives the Institute infrastructure support (use of Windle House and chapel space in Elmsley Hall, administrative support, basic office equipment and building maintenance). Salaries are covered by donations from the MASI Foundation, which holds the Institute’s endowments.</p>
    <p>A registered charity in Canada and a 501(c)3 in the USA, the Foundation makes donations to help USMC with the Institute’s annual budget. MASI’s academic and support staff are employees of USMC. The Foundation also invested wisely years ago in the Ontario Student Opportunity Trust Fund; thus, there are some monies set aside for scholarships for Ontario residents wishing to study at MASI. The Institute also has a small revenue stream through some of its publications.</p>',
  ),
);

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b faqs">
      <div class="faqs__bg xs-hide md-show">
        <?php echo wp_get_attachment_image(101, 'large'); ?>
      </div>
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <div class="faqs__heading">
          <div class="faqs__title">
            <h1>FAQs</h1>
          </div>
          <div class="faqs__search">
            <form role="search" method="get" id="faqs-searchform" class="searchform searchform--shadow" action="/">
              <div class="searchform__inner"><label class="screen-reader-text" for="faqs-searchinput">Search FAQs</label>
                <input type="search" value="" placeholder="Search FAQs" name="s" id="faqs-searchinput" />
                <button class="button" type="submit" id="searchsubmit"><?php the_svg('search', 'Search FAQs'); ?></button>
              </div>
            </form>
          </div>
        </div>
        <div class="faqs__accordions">
          <?php get_template_part('template-parts/accordions', '', array('data' => $faqs)); ?>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
