<?php

/**
 * Template name: About - Overview
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div class="intro__text">
          <div class="intro__text__card card">
            <h2 class="h4">A centre of higher learning, research, ecumenical encounter and prayer, in the Faculty of Theology at the University of St. Michael’s College in the University of Toronto.</h2>
          </div>
        </div>
        <div class="intro__images">
          <div class="intro__images__img intro__images__img--1">
            <div class="intro__images__img__wrapper">
              <div class="intro__images__img__inner intro__images__img__inner--1">
                <?php echo wp_get_attachment_image(112, 'large'); ?>
              </div>
            </div>
          </div>
          <div class="intro__images__img intro__images__img--2 xs-hide sm-show">
            <div class="intro__images__img__wrapper">
              <div class="intro__images__img__inner intro__images__img__inner--2">
                <?php echo wp_get_attachment_image(113, 'large'); ?>
              </div>
            </div>
          </div>
          <div class="intro__images__img intro__images__img--3">
            <div class="intro__images__img__wrapper">
              <div class="intro__images__img__inner intro__images__img__inner--3">
                <?php echo wp_get_attachment_image(235, 'large'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="container row section-m-t section-m-b">
      <div class="col-xs-12 col-sm-10 col-md-12 col-lg-10 col-lg-offset-1">
        <h2 class="h1">Overview</h2>
        <div class="text-col" data-rm data-rm-xs="3">
          <p>The Institute is a centre of higher learning committed to quality education in Eastern Christian theology and related disciplines, both at the University of St. Michael’s College and in its outreach programs. </p>
          <p><em class="blockquote">It is particularly devoted to following the mandate of the Second Vatican Council and developing a distinctive Eastern Catholic theology in all its breadth.</em></p>
          <p>The Institute is also a centre of research committed to scholarship and publication in the various fields of Eastern Christian Studies, cooperating with other educational institutions, learned societies and individual scholars.</p>
          <p>Committed to ecumenical and interfaith encounters, the Institute aims to foster respectful and fruitful dialogue among the various Eastern Christian Churches (Orthodox and Catholic) and between Eastern and Western Christians. The Institute also promotes understanding between Christians and members of other faith traditions, especially the Jewish and Muslim faiths.</p>
          <p>The Institute is dedicated to integrating academic study and worship of the Triune God. It does so by integrating liturgical services into its daily schedule and sponsoring initiatives such as the Sheptytsky Institute Choir and Cantor Training programs.</p>
        </div>
      </div>
    </section>

    <section class="container row section-m-t section-m-b history">
      <div class="col-xs-12 col-sm-4 xs-hide sm-show">
        <div class="history__img">
          <?php echo wp_get_attachment_image(106, 'large'); ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-6 col-md-offset-1 col-lg-5 col-lg-offset-1">
        <h2 class="h1">History</h2>
        <p><em class="blockquote">Founded in 1986 by Father Andriy Chirovsky</em></p>
        <p>Founded in 1986 by Father Andriy Chirovsky at the Catholic Theological Union in Chicago, the Sheptytsky Institute came under the patronage of the Ukrainian Catholic Bishops of Canada in 1989 and moved to Ottawa’s Saint Paul University in 1990.</p>
        <p>Since July 1, 2017, MASI has been an autonomous academic unit within USMC’s Faculty of Theology and part of the Toronto School of Theology, an ecumenical consortium of 7 colleges.</p>
        <p>The Institute aspires to communicate the power of Christian Faith and living Tradition, so that all may share in the very life of God, Father, Son and Holy Spirit.</p>
      </div>
    </section>

    <?php get_template_part('template-parts/subscribe', '', array('class' => 'section-m-t section-m-b', 'type' => 'about')); ?>

    <section class="container row section-m-t section-m-b--sm masif">
      <div class="col-xs-12 col-sm-10 col-md-7 col-lg-6 col-lg-offset-1">
        <h2 class="h1">MASI Foundation</h2>
        <p>The Metropolitan Andrey Sheptytsky Institute Foundation is a non-profit, charitable organization, registered in Canada and the United States, and operating under the spiritual and financial patronage of the Ukrainian Catholic Bishops of Canada.</p>
        <p><em class="blockquote">The mission of the Foundation is to raise and manage capital funds in order to provide long-term financial support to the Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies. </em></p>
        <p>The Foundation’s mission is fulfilled through development, investment and philanthropy. Donations towards the Institute result in Canadian or U.S. income tax receipts promptly issued by the Foundation.</p>
        <p class="align-right"><a href="/about/masi-foundation/" class="button"><span></span>Learn More</a></p>
      </div>
    </section>

    <section class="container row section-m-b andrey">
      <div class="andrey__img">
        <?php echo wp_get_attachment_image(941, 'large', '', array('class' => 'xs-hide sm-show')); ?>
        <?php echo wp_get_attachment_image(940, 'large', '', array('class' => 'sm-hide')); ?>
      </div>
      <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7 col-lg-offset-1">
        <div class="andrey__card card card--with-cross-img">
          <div class="andrey__card__img card__img xs-hide sm-show">
            <?php echo wp_get_attachment_image(109, 'large'); ?>
          </div>
          <div class="andrey__card__text">
            <h2 class="h1">Metropolitan Andrey</h2>
            <p>Metropolitan Andrey Sheptytsky was the ranking hierarch of the Ukrainian Greco-Catholic Church from 1901 until his death on November 1, 1944. His life was an example of heroic virtue.</p>
            <p>An extremely active pastor, he used his personal wealth to fund thousands of philanthropic projects and was also a man of deep prayer. His two great passions in life were the restoration of authentic Eastern Christian Monasticism in his Church and the union of Churches.</p>
            <p>Metropolitan Andrey led his flock of some five million faithful through two world wars. The cause for his beatification is underway.</p>
            <p class="align-right"><a href="/about/metropolitan-andrey/" class="button"><span></span>Learn More</a></p>
          </div>
        </div>
      </div>
    </section>

    <section class="container row section-m-t section-m-b reports">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h2 class="h1">Annual Reports</h2>
        <p>Sheptytsky Institute Annual Reports.</p>
        <?php
        $reports = array(
          array(
            'title' => '2019 - 2020',
            'link'  => '/wp-content/uploads/2021/12/MASI-Report-2019-20.pdf',
          ),
          array(
            'title' => '2018 - 2019',
            'link'  => '/wp-content/uploads/2021/12/Annual-Report-2018-2019-FINAL-Web.pdf',
          ),
          array(
            'title' => '2017 - 2018',
            'link'  => '/wp-content/uploads/2021/12/Sheptytsky-Institute-Annual-Report-16-page-2017-2018.pdf',
          ),
        );
        ?>
        <?php if ($reports) { ?>
          <div class="reports__list">
            <ul>
              <?php foreach ($reports as $report) { ?>
                <li>
                  <article class="reports__report">
                    <h2 class="reports__report__title h6"><?php echo $report['title']; ?></h2>
                    <a href="<?php echo $report['link']; ?>" target="_blank" rel="noopener noreferrer" class="button-icon">
                      PDF
                      <?php the_svg('external-link'); ?>
                    </a>
                  </article>
                </li>
              <?php } ?>
            </ul>
          </div>
        <?php } ?>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
