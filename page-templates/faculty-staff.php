<?php

/**
 * Template name: Faculty & Staff
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$search = isset($_GET['search']) ? $_GET['search'] : '';

$categories = get_terms(
  array(
    'taxonomy'    => 'position',
    'hide_empty'  => false,
    'orderby'     => 'name',
    'order'       => 'ASC',
  )
);

// Custom sort categories
function sort_categories($a, $b) {
  $sort = array('professors', 'instructors', 'retired-professors', 'academic-collaborators', 'administration');
  $a_index = array_search($a->slug, $sort);
  $b_index = array_search($b->slug, $sort);
  if ($a_index === false) {
    return 1;
  }
  if ($b_index === false) {
    return -1;
  }
  return ($a_index < $b_index) ? -1 : 1;
}
usort($categories, 'sort_categories');

if ($search) {
  $all = (object) [
    'name' => 'All',
    'slug' => 'all'
  ];
  array_unshift($categories, $all);
}

$tabs = '';
$tab_panels = '';

$i = 0;
foreach ($categories as $cat) {
  $title    = $cat->name;
  $key      = $cat->slug;
  $active   = $i == 0 ? 'active' : '';
  $selected = $i == 0 ? 'true' : '';

  $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' data-tab='$key' role='tab'><span>$title</span></a>";
  $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";

  $deceased_query = new WP_Query(array(
    'post_type'      => 'staff',
    'posts_per_page' => 1,
    'meta_key'       => 'is_deceased',
    'meta_value'     => true,
    'no_found_rows'  => true,
    'fields'         => 'ids',
    'tax_query'      => array(
      array(
        'taxonomy'   => 'position',
        'field'      => 'slug',
        'terms'      => $cat->slug,
      )
    )
  ));
  $is_tabbed = $deceased_query->have_posts();

  // Faculty members list
  $sub_tabs = '';
  $sub_tab_panels = '';

  if ($is_tabbed) {
    $sub_tabs_labels = array(
      array(
        'is_deceased' => false,
        'title'       => 'Past and Present',
      ),
      array(
        'is_deceased' => true,
        'title'       => 'Deceased ' . $title,
      )

    );
    $j = 0;
    foreach ($sub_tabs_labels as $ss) {
      $ss_title    = $ss['title'];
      $ss_key      = sanitize_key($ss_title);
      $ss_active   = $j == 0 ? 'active' : '';
      $ss_selected = $j == 0 ? 'true' : '';

      $sub_tabs .= "<a href='#$ss_key' id='tab-$ss_key' class='tab $ss_active' aria-selected='$ss_selected' aria-controls='$ss_key' role='tab'><span>$ss_title</span></a>";
      $sub_tab_panels .= "<div id='$ss_key' class='tab-panel $ss_active' aria-expanded='$ss_selected' aria-labelledby='tab-$ss_key' role='tabpanel'>";
      $sub_tab_panels .= "<div data-js-id='$key' data-is_deceased='true'>";
      $sub_tab_panels .= lb_load_template_part('template-parts/faculty-staff-list', '', array('category' => $cat->slug, 'search' => $search, 'is_deceased' => $ss['is_deceased']));;
      $sub_tab_panels .= "</div>"; // data-js-id
      $sub_tab_panels .= "</div>"; // tab-panel

      $j++;
    }

    $tab_panels .= '<div class="lb-tabs" data-use-hash>';
    $tab_panels .= '<div class="tabs tabs--small" role="tablist">';
    $tab_panels .= $sub_tabs;
    $tab_panels .= '</div>'; // tabs
    $tab_panels .= '<div class="tab-panels">';
    $tab_panels .= $sub_tab_panels;
    $tab_panels .= '</div>'; // tab-panels
    $tab_panels .= '</div>'; // lb-tabs
  } else {
    $tab_panels .= "<div data-js-id='$key'>";
    $tab_panels .= lb_load_template_part('template-parts/faculty-staff-list', '', array('category' => $cat->slug, 'search' => $search));
    $tab_panels .= "</div>";
  }

  $tab_panels .= "</div>"; // tab-panel

  $i++;
}

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <div id="faculty" class="lb-tabs" data-use-hash>
      <section class="container row page-m-t intro">
        <div class="intro__bg xs-hide sm-show">
          <?php echo wp_get_attachment_image(521, 'large'); ?>
        </div>
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <h1>Faculty & Staff</h1>
          <p class="narrow">Our institute hosts leading academics who contribute to a truly unique educational experience in Eastern Christian Studies.</p>
          <div class="boxed-nav">
            <div class="boxed-nav__tabs">
              <div class="tabs-wrapper">
                <div class="tabs tabs--alt" role="tablist">
                  <?php echo $tabs; ?>
                </div>
              </div>
            </div>
            <div class="boxed-nav__search">
              <?php echo lb_get_search_form('faculty-searchform', '', home_url('/faculty/'), 'Search by Keyword', 'search', $search); ?>
            </div>
          </div>
        </div>
      </section>
      <section id="staff-members" class="container row section-m-b">
        <div class="col-xs-12 col-lg-9 col-lg-offset-1">
          <div class="tab-panels tab-panels--main">
            <?php echo $tab_panels; ?>
          </div>
        </div>
      </section>
    </div>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
