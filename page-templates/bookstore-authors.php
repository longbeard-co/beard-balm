<?php

/**
 * Template name: Bookstore - Authors
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$search = isset($_GET['search']) ? $_GET['search'] : '';

$char = isset($_GET['char']) ? $_GET['char'] : ($search ? '' : 'A');
if ($char && !in_array(strtoupper($char), range('A', 'Z'))) {
  $char = $search ? '' : 'A';
}


?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <?php echo lb_get_woocommerce_top_header(); ?>
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <?php lb_woocommerce_nav_open(); ?>

        <nav class="bookstore-authors-nav">
          <ul>
            <?php foreach (range('A', 'Z') as $ch) { ?>
              <li class="<?php echo $ch == $char ? 'current' : ''; ?>">
                <a href="/bookstore/authors/?char=<?php echo $ch; ?>"><?php echo $ch; ?></a>
              </li>
            <?php } ?>
          </ul>
        </nav>

        <div class="results">
          <?php get_template_part('template-parts/bookstore-authors-list', '', array('char' => $char, 'search' => $search)); ?>
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
        <?php // get_sidebar('shop'); 
        ?>
      </div>
    </section>

    <?php get_template_part('template-parts/subscribe', '', array('type' => 'bookstore', 'class' => 'section-m-t section-m-b')); ?>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
