<?php

/**
 * Template name: Bookstore - FAQs
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$faqs = array(
  array(
    'title' => 'What happens after I place my order?',
    'body'  => '<p>After you place an order a receipt is sent to your email address.</p>',
  ),
  array(
    'title' => 'How long will it take to get my order?',
    'body'  => '<p>Delivery time: Canada/USA 2-4 weeks, International 4-6 weeks. Digital purchases are sent immediately.</p>',
  ),
  array(
    'title' => 'How are shipping costs calculated?',
    'body'  => '<p>Shipping is $8 per item. Shipping for journals is $10. Shipping for larger/heavier books and specialty items (posters, shoulder bag, etc.) range between $10 to $20. Shipping for <a href="/bookstore/bulk-discounts/">bulk orders, bundles, packages and collections</a> are higher depending on number of items, size and weight. Free shipping on orders of $300+</p>',
  ),
  array(
    'title' => 'Do prices ever change?',
    'body'  => '<p>Yes. Prices are subject to change.</p>',
  ),
);

$phone = '416-926-7133';
$phone_desc = '(9 am – 5 pm EST)';
$email = 'sheptytsky@utoronto.ca';

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b faqs">
      <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1">
        <div class="faqs__heading faqs__main">
          <div class="faqs__title">
            <h1>FAQs</h1>
          </div>
          <div class="faqs__search">
            <form role="search" method="get" id="faqs-searchform" class="searchform searchform--shadow" action="/">
              <div class="searchform__inner"><label class="screen-reader-text" for="faqs-searchinput">Search FAQs</label>
                <input type="search" value="" placeholder="Search FAQs" name="s" id="faqs-searchinput" />
                <button class="button" type="submit" id="searchsubmit"><?php the_svg('search', 'Search FAQs'); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1">
        <div class="faqs__accordions faqs__main">
          <?php get_template_part('template-parts/accordions', '', array('data' => $faqs)); ?>
        </div>
      </div>
      <div class="col-xs-12 col-md-4 col-lg-3">
        <div class="faqs__contact">
          <div class="faqs__contact__bg xs-hide sm-show">
            <?php echo wp_get_attachment_image(186, 'large'); ?>
          </div>
          <div class="faqs__contact__card card">
            <h2 class="h6 faqs__contact__title">Have further questions?</h2>
            <div class="contact-info-wrapper">
              <div>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('phone'); ?>
                    <span><a href="<?php echo phone_to_url($phone); ?>" target="_blank" rel="noopener noreferrer"><?php echo $phone; ?></a> <?php echo $phone_desc; ?></span>
                  </div>
                </div>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('email'); ?>
                    <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
