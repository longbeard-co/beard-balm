<?php

/**
 * Template name: Academics - Distance Education
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$courses = array(
  array(
    'title' => 'Canon Law in the Church',
    'link'  => 'https://www.tst.edu/academic/course/canon-law-and-church'
  ),
  array(
    'title' => 'History of Christianity I',
    'link'  =>  'https://www.tst.edu/academic/course/history-christianity-i-ad-843-55',
  ),
  array(
    'title' => 'Foundations to Eastern Ethics',
    'link'  => 'https://www.tst.edu/academic/course/foundations-eastern-christian-ethics-2',
  ),
  array(
    'title' => 'The Three-Personed God: Eastern Christian Perspectives',
    'link'  =>  'https://www.tst.edu/academic/course/three-personed-god-eastern-christian-perspectives-3',
  ),
  array(
    'title' => 'Contemporary Issues in Eastern Christian Moral Theology',
    'link'  =>  'https://www.tst.edu/academic/course/contemporary-issues-eastern-christian-moral-theology',
  ),
);
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 col-lg-offset-1">
        <div class="intro__text">
          <h1>Distance Education</h1>
          <p>The Sheptytsky Institute offers a limited number of courses via distance education, which vary from year to year.</p>
          <p><em class="blockquote">These courses are offered over the internet synchronously with on campus participation.</em></p>
          <p>Students in remote locations can earn university credits towards ecclesiastical (pontifical) and civil degree programs, while participating in real time in classes: with full access to visual material, the ability to ask questions and engage in classroom discussion. </p>
          <p>Prospective students are responsible for ensuring they have the appropriate technology to participate in the courses.</p>
        </div>
      </div>
      <div class="col-xs-4 col-sm-3 col-md-4 col-md-offset-1">
        <div class="intro__img">
          <?php echo wp_get_attachment_image(233, 'large'); ?>
        </div>
      </div>
      <?php if (!empty($courses)) { ?>
        <div class="col-xs-12 col-md-7 col-md-offset-1 col-lg-6 col-lg-offset-2">
          <div class="intro__courses">
            <h3 class="intro__courses__title h6">The following courses are available online:</h3>
            <ul>
              <?php foreach ($courses as $course) { ?>
                <li>
                  <a href="<?php echo $course['link'] ?>" target="_blank">
                    <img src="/wp-content/themes/beardbalm/assets/icons/document-gradient.svg" width="16" height="18" alt="" />
                    <h6><?php echo $course['title']; ?></h6>
                  </a>
                </li>
              <?php } ?>
            </ul>
            <p class="align-right"><a href="/academics/degree-programs" class="button"><span></span>Go to Degree Programs</a></p>
          </div>
        </div>
      <?php } ?>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
