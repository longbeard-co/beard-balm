<?php

/**
 * Template name: About - MASIF
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b--sm intro">
      <div class="col-xs-12 col-sm-10 col-md-12 col-lg-10 col-lg-offset-1">
        <h1>MASI Foundation</h1>
        <div class="text-col" data-rm data-rm-xs="4" data-rm-sm="4">
          <p>Established in 1989 by the Ukrainian Catholic Bishops of Canada, the Metropolitan Andrey Sheptytsky Institute Foundation (MASIF) is a federally registered charitable organization.</p>
          <p>The board of directors is comprised of bishops and lay representatives from Canada and the United States.</p>
          <p><em class="blockquote">Its goal is to manage and develop a principal fund to support the teaching and academic activity of the Sheptytsky Institute.</em></p>
          <p>Funds are raised from individuals, organizations and foundations in North America.</p>
          <p>Our donors are caring people, who have a commitment to our Church and its future. For some it is a true sacrifice to give their “Widow’s mite” of a few dollars. Others like Peter and Doris Kule of Edmonton have donated over two million dollars in endowed funds, knowing that their gift will help to fund worthwhile educational programs for future generations. </p>
          <p>Our donors also help us by praying for the Institute, its staff and students, by persuading their friends to support the Institute and by encouraging young people to come and study here. </p>
          <p>All donors are welcomed into the Sheptytsky Institute family and remembered in our prayers and worship at our Chapel of Saints Joachim and Anne.</p>
        </div>
      </div>
    </section>

    <?php
    $mission_texts = array(
      array(
        'lang' => 'English',
        'text'  => 'The mission of the Foundation is to raise and manage capital funds in order to provide long-term financial support to the Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies.',
      ),
      array(
        'lang' => 'French',
        'text'  => "La Fondation a pour mission de recueillir et d’administrer des fonds capitaux dans le but de fournir un soutien financier à long terme à l'Institut Métropolite Andrey Sheptytsky pour l’étude du christianisme oriental.",
      ),
      array(
        'lang' => 'Ukrainian',
        'text'  => "Місія фундації пoлягає в тому, щоб збирати та вміло розпоряджатися коштами з метою забезпечення довготривалої фінансової підтримки Інститутові східно-християнських студій імені Митрополита Андрея Шептицького.",
      )
    );
    ?>

    <section class="container row section-m-b--sm mission">
      <div class="mission__img">
        <?php echo wp_get_attachment_image(301, 'large', '', array('class' => 'xs-hide sm-show')); ?>
        <?php echo wp_get_attachment_image(141, 'large', '', array('class' => 'sm-hide')); ?>
      </div>
      <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 col-lg-offset-1">
        <div class="mission__card card card--with-cross-img">
          <div class="mission__card__img card__img xs-hide sm-show">
            <?php echo wp_get_attachment_image(109, 'large'); ?>
          </div>
          <div class="mission__card__text">
            <h2 class="h1">Mission</h2>
            <div class="lb-tabs">
              <?php
              $tabs = '';
              $tab_panels = '';
              $i = 0;
              foreach ($mission_texts as $mission_text) {
                $lang = $mission_text['lang'];
                $key   = sanitize_title($lang);
                $text  = $mission_text['text'];
                $active = $i == 0 ? 'active' : '';
                $selected = $i == 0 ? 'true' : '';

                $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$lang</span></a>";
                $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'><p>$text</p></div>";

                $i++;
              }
              ?>
              <div class="tabs tabs--alt" role="tablist">
                <?php echo $tabs; ?>
              </div>
              <div class="tab-panels">
                <?php echo $tab_panels; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="container row section-m-b">
      <div class="col-xs-12 col-sm-10 col-md-6 col-lg-5 col-lg-offset-1">
        <h2 class="h1">History</h2>
        <p>The Metropolitan Andrey Sheptytsky Institute Foundation was established in 1989 by the Ukrainian Catholic Bishops of Canada. It’s current endowment is over five million dollars and includes two endowed chairs: the Kule Family Chair in Eastern Christian Liturgy and the Peter and Doris Kule Chair of Eastern Christian Theology and Spirituality.</p>
      </div>
    </section>

    <section class="container row section-m-t section-m-b donate">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div class="donate__inner box-shadow">
          <div class="donate__img xs-hide col-sm-show">
            <div class="donate__img__bg"><?php echo wp_get_attachment_image(121, 'large'); ?></div>
            <div class="donate__img__fg"><?php echo wp_get_attachment_image(282, 'large'); ?></div>
          </div>
          <div class="donate__text">
            <h2 class="h1 color-primary">Donate</h2>
            <p>We appreciate and gratefully acknowledge all donations, small or large. Canadian or U.S. income tax receipts are promptly issued.</p>
            <p>There are so many ways of giving to choose from: cash, property, stocks, bonds, real estate. Let us help you find the method of giving that works best for you.</p>
            <p class="align-right"><a href="/donate/" class="button alt"><span></span>Donate Now</a></p>
          </div>
        </div>
      </div>
    </section>

    <?php
    $executive_committee = array(
      array(
        'image' => 123,
        'name' => 'Most Rev. Bryan Bayda',
        'location' => 'Saskatoon, Saskatchewan',
        'role' => 'Liaison of Ukrainian Catholic Bishops of Canada',
      ),
      array(
        'image' => 124,
        'name' => 'Father Andriy Chirovsky',
        'location' => 'Tucson, AZ',
        'role' => 'Founder',
      ),
      array(
        'image' => 125,
        'name' => 'Dr. Pascal Bastien',
        'location' => 'Toronto, ON',
        'role' => 'President',
      ),
      array(
        'image' => 126,
        'name' => 'Andrew Hladyshevsky',
        'location' => 'Edmonton, AB',
        'role' => 'Immediate Past President',
      ),
      array(
        'image' => 127,
        'name' => 'Dr. Andrew Browar',
        'location' => 'Chicago, IL',
        'role' => 'Vice President',
      ),
      array(
        'image' => 128,
        'name' => 'Mark Stoiko',
        'location' => 'Toronto, ON',
        'role' => 'Secretary',
      ),
    );

    $placeholder_m = lb_get_person_placeholder_id();
    $placeholder_f = lb_get_person_placeholder_id('female');

    $directors = array(
      array(
        'image' => $placeholder_m,
        'name' => 'Rev. Dr. Andrew P.W. Bennett',
        'location' => 'Ottawa, ON',
      ),
      array(
        'image' => $placeholder_m,
        'name' => 'Julian Savaryn',
        'location' => 'Edmonton, AB',
      ),
      array(
        'image' => 129,
        'name' => 'Paul Grod',
        'location' => 'Mississauga, ON',
      ),
      array(
        'image' => $placeholder_m,
        'name' => 'Luke Nicholas Laschuk Miller',
        'location' => 'San Diego, CA',
      ),
      array(
        'image' => $placeholder_m,
        'name' => 'Michale Levy',
        'location' => 'Lakewood, OH',
      ),
      array(
        'image' => $placeholder_m,
        'name' => 'Taras Pidzamecky',
        'location' => 'Toronto, ON',
      ),
      array(
        'image' => $placeholder_m,
        'name' => 'Dr. William Turk',
        'location' => 'Winnipeg, MB',
      ),
    );

    $board = array(
      array(
        'title' => 'Executive Committee',
        'members' => $executive_committee,
      ),
      array(
        'title' => 'Directors',
        'members' => $directors,
      ),
    );
    ?>

    <section class="container row section-m-t section-m-b board">
      <div class="board__bg">
        <?php echo wp_get_attachment_image(133, 'large'); ?>
      </div>
      <div class="col-xs-12 col-lg-8 col-lg-offset-1">
        <h2 class="h1">Current Board of Directors</h2>
        <p class="narrow">The board of directors of the Foundation work as members and as an executive to develop the Foundation and support the Institute.</p>

        <?php
        $tabs = '';
        $tab_panels = '';
        $i = 0;
        foreach ($board as $board_group) {
          $title    = $board_group['title'];
          $key      = sanitize_title($title);
          $members  = $board_group['members'];
          $active   = $i == 0 ? 'active' : '';
          $selected = $i == 0 ? 'true' : '';

          $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$title</span></a>";
          $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";
          $tab_panels .= "<div class='board__list person-list'><ul>";
          foreach ($members as $person) {
            $image = $person['image'] ? wp_get_attachment_image($person['image'], 'large') : null;
            $name   = $person['name'];
            $location   = isset($person['location']) ? $person['location'] : null;
            $role   = isset($person['role']) ? $person['role'] : null;

            $tab_panels .= "<li>";
            $tab_panels .= "<div class='person'>";
            $tab_panels .= "<div class='person__img'>";
            $tab_panels .= "<div class='person__img__inner'>$image</div>";
            $tab_panels .= "</div>";
            $tab_panels .= "<div class='person__text'>";
            $tab_panels .= "<h3 class='h6 person__name'>$name</h3>";
            $tab_panels .= "<h4 class='h6 person__location'>$location</h4>";
            $tab_panels .= $role ? "<p class='small person__meta'>$role</p>" : null;
            $tab_panels .= "</div>";
            $tab_panels .= "</div>";
            $tab_panels .= "</li>";
          }
          $tab_panels .= "</ul></div>"; // board__list
          $tab_panels .= "</div>"; // tab-panel

          $i++;
        }

        ?>

        <div class="lb-tabs">
          <div class="tabs" role="tablist"><?php echo $tabs; ?></div>
          <div class="tab-panels"><?php echo $tab_panels; ?></div>
        </div>
      </div>
    </section>

    <?php
    $phone = '416-926-7133';
    $email = 'masif@utoronto.ca';
    $address_link = 'https://goo.gl/maps/CkhRh3CXgAgSDx2h8';
    $address_title = 'Metropolitan Andrey Sheptytsky Institute Foundation (MASIF)';
    $address_text = 'University of St. Michael’s College in the University of Toronto<br />
      81 St. Mary Street<br />
      Toronto, ON M5S 1J4<br />
      Canada';
    ?>

    <section class="container row section-m-t section-m-b contact">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h2 class="h1">Foundation Contact</h2>
        <div class="contact__inner">
          <div class="contact-info-wrapper">
            <div class="contact__row">
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('phone'); ?>
                    <a href="<?php echo phone_to_url($phone); ?>" target="_blank" rel="noopener noreferrer"><?php echo $phone; ?></a>
                  </div>
                </div>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('email'); ?>
                    <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                  </div>
                </div>
              </div>
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('location'); ?>
                    <div class="contact-info__text">
                      <h3 class="h5"><a href="<?php echo $address_link; ?>" target="_blank" rel="noopener noreferrer"><strong><?php echo $address_title; ?></strong></a></h3>
                      <p><?php echo $address_text; ?></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
