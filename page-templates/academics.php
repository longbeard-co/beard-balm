<?php

/**
 * Template name: Academics - Overview
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 col-lg-offset-1">
        <div class="intro__text">
          <h1>Academics</h1>
          <p>The Sheptytsky Institute offers its courses through the Faculty of Theology at the University of St. Michael’s College in the University of Toronto. This is a member of the Toronto School of Theology.</p>
          <div class="intro__logos">
            <ul>
              <li>
                <?php echo wp_get_attachment_image(148, 'large'); ?>
              </li>
              <li>
                <?php echo wp_get_attachment_image(149, 'large'); ?>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="xs-hide sm-show col-sm-5 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-1">
        <div class="intro__img xs-hide sm-show">
          <div class="intro__img__inner">
            <?php echo wp_get_attachment_image(236, 'large'); ?>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 225 206">
              <path fill="url(#ac-cross)" d="M0 0h224.487v206H0z" />
              <path fill="#fff" d="M103.376 136.877v-32.812H90.561c-.103 1.513 0 3.026-.308 4.434-.769 3.495-3.742 5.79-7.227 5.895-3.537.052-6.613-2.087-7.535-5.478-.36-1.356-.667-2.191-2.358-2.451-5.229-.992-8.868-5.686-8.868-10.955 0-5.216 3.537-9.65 8.714-10.798 1.281-.26 2.05-.626 2.46-2.139 1.077-4.225 4.614-6.416 8.766-5.79 3.844.626 6.458 3.964 6.356 8.19v1.721h12.815v-15.18c-3.332.47-6.562.365-8.715-2.4-1.127-1.408-1.999-3.338-2.204-5.111-.563-4.59 2.102-7.512 7.843-9.234 1.999-7.876 5.844-11.632 11.943-11.632 6.151 0 10.098 3.86 11.944 11.632 6.099 1.983 8.662 5.217 7.894 10.016-.769 4.747-4.306 7.042-10.867 6.886v15.075h12.609c.154-1.512.103-2.92.462-4.225.871-3.495 3.793-5.738 7.278-5.79 3.486-.052 6.562 2.139 7.433 5.581.359 1.461.923 2.14 2.46 2.452 5.178 1.043 8.714 5.425 8.766 10.694.051 5.269-3.588 9.755-8.817 11.059-.871.208-1.948 1.043-2.204 1.826-1.435 4.381-5.075 6.781-9.227 5.894-3.947-.834-6.356-4.59-5.946-9.024.052-.365.052-.678.103-1.2h-12.969v33.02c.821 0 1.589-.052 2.307 0 3.742.313 6.715 3.078 7.176 6.625.513 4.069-1.537 7.564-5.331 8.659-1.537.47-2.153 1.096-2.46 2.713-.974 5.112-5.639 8.816-10.713 8.711-5.126-.052-9.432-3.755-10.611-8.92-.205-.887-1.025-1.982-1.794-2.295-4.306-1.513-6.51-4.799-5.843-9.129.564-3.808 3.742-6.364 8.099-6.468.41.104.82 0 1.384-.052Z" />
              <defs>
                <linearGradient id="ac-cross" x1="112.244" x2="112.244" y1="0" y2="206" gradientUnits="userSpaceOnUse">
                  <stop offset=".406" stop-color="#D0A40F" />
                  <stop offset="1" stop-color="#FFD237" />
                </linearGradient>
              </defs>
            </svg>
          </div>
        </div>
      </div>
    </section>

    <?php get_template_part('template-parts/degree-programs-overview', '', array(
      'img'      => 151,
      'subtitle' => '<p>Programs are offered at the Sheptytsky Institute in both the advanced degree and basic degree levels.</p>'
    )); ?>

    <section class="container row section-m-t section-m-b courses">
      <div class="xs-hide sm-show col-sm-4 col-md-5">
        <div class="courses__img">
          <?php echo wp_get_attachment_image(153, 'large', '', array('class' => 'bg xs-hide md-show')); ?>
          <?php echo wp_get_attachment_image(154, 'large', '', array('class' => 'fg')); ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6">
        <h2 class="h1">Courses</h2>
        <div class="courses__form">
          <p>The Sheptytsky Institute offers a wide array of courses in Eastern Christian theology, history, liturgy and spirituality.</p>
          <?php get_template_part('template-parts/search-course-form', '', array('checkbox' => false)); ?>
        </div>
        <p class="align-right"><a href="/academics/courses/" class="button"><span></span>View All Courses</a></p>
      </div>
    </section>

    <section class="container row section-m-t section-m-b links">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div class="links__inner">
          <ul>
            <li>
              <div class="links__link card">
                <h2 class="links__link__title h3">
                  <span>Distance Education</span>
                  <img src="<?php the_theme_image_src('distance-education.svg'); ?>" alt="" width="83" height="83" />
                </h2>
                <p>The Sheptytsky Institute makes some of its on-campus courses also available to distance students using web-conferencing systems.</p>
                <p class="align-right"><a href="/academics/distance-education/" class="button"><span></span>Learn More</a></p>
              </div>
            </li>
            <li>
              <div class="links__link card">
                <h2 class="links__link__title h3">
                  <span>Bursaries & Scholarships</span>
                  <img src="<?php the_theme_image_src('scholarships.svg'); ?>" alt="" width="83" height="83" />
                </h2>
                <p>The Sheptytsky Institute and its Foundation have scholarships available for qualifying students.</p>
                <p class="align-right"><a href="/academics/bursaries-scholarships/" class="button"><span></span>Learn More</a></p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
