<?php

/**
 * Template name: Academics - Courses
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();


?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b--sm intro">
      <div class="intro__bg xs-hide md-show">
        <?php echo wp_get_attachment_image(349, 'large'); ?>
      </div>
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h1>Courses</h1>
        <div class="text-col text-block">
          <p>The Sheptytsky Institute offers a wide array of courses in Eastern Christian theology, history, liturgy and spirituality. Some courses are offered in alternate years.</p>
          <p>As part of their programs, Sheptytsky Institute students also take USMC courses, as well as courses in other colleges in the Toronto School of Theology (TST) ecumenical consortium.</p>
          <p>All courses taught by Sheptytsky Institute faculty take place at Sheptytsky Institute, <a href="https://goo.gl/maps/CkhRh3CXgAgSDx2h8" target="_blank" rel="noopener noreferrer">Windle House, 5 Elmsley Place</a>, on the campus of the University of St. Michael’s College in Toronto.</p>
          <p>For a listing of courses for the current and upcoming academic year, please visit the <a href="https://www.tst.edu/academic/course/listings" target="_blank" rel="noopener noreferrer">TST website</a>.</p>
        </div>
      </div>
    </section>
    <section class="container row search-course-form">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div id="search-course-form">
          <?php get_template_part('template-parts/search-course-form'); ?>
        </div>
      </div>
    </section>
    <section class="container row section-m-b courses">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <div id="courses-list">
          <?php get_template_part('template-parts/courses-list'); ?>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
