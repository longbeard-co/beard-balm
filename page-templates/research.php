<?php

/**
 * Template name: Research
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$search = isset($_GET['search']) ? $_GET['search'] : '';

$data = array(
  array(
    'title'   => 'Conferences',
    'text'    => '',
    'subsections' => array(
      array(
        'title'     => 'Upcoming Conferences',
        'post_type' => 'tribe_events',
        'category'  => 'conferences',
        'text'      => '<p>We are hoping to offer Study Days again in the summer of 2022. Please check this page for updates in the future.</p>',
      ),
      array(
        'title'     => 'Past Conferences',
        'post_type' => 'post',
        'category'  => 'past-conferences',
        'text'      => '<p>Some content from previous conferences is available on our website; other conferences are available through the Sheptytsky Institute Bookstore.</p>',
      ),
      array(
        'title'     => 'Study Days',
        'post_type' => 'post',
        'category'  => 'study-days',
        'text'      => '',
      ),
    ),
  ),
  array(
    'title'   => 'Lectures',
    'text'    => '<p>Discover the latest scholarly findings and theological research in Eastern Christian studies with the Tuesdays at Sheptytsky lecture series organized by the Sheptytsky Institute at St. Michael’s College in the University of Toronto. Guests include accomplished distinguished professors, researchers, and journalists.</p>',
    'subsections' => array(
      array(
        'title'     => '2019 Lectures',
        'post_type' => 'post',
        'category'  => '2019-lectures',
        'text'      => '',
      ),
      array(
        'title'     => '2018 Lectures',
        'post_type' => 'post',
        'category'  => '2018-lectures',
        'text'      => '',
      ),
    ),
  ),
  array(
    'title'   => 'Seminars',
    'text'    => '',
    'subsections' => array(
      array(
        'title'     => 'Upcoming Seminars',
        'post_type' => 'tribe_events',
        'category'  => 'upcoming-seminars',
        'text'      => '',
      ),
      array(
        'title'     => 'Past Seminars',
        'post_type' => 'post',
        'category'  => 'past-seminars',
        'text'      => '',
      ),
    ),
  ),
  array(
    'title'   => 'Panel / Roundtable',
    'text'    => '<p>The Sheptytsky Institute is proud to collaborate with academic partners to offer roundtable discussions on various topics.</p>',
    'subsections' => array(
      array(
        'title'     => 'Panel / Roundtable',
        'post_type' => 'post',
        'category'  => 'panel-roundtable',
        'text'      => '',
      ),
    ),
  ),
);

if ($search) {
  array_unshift($data, array(
    'title'     => 'All',
    'text'      => '',
    'subsections' => array(
      array(
        'title'     => 'All',
        'post_type' => 'post',
        'category'  => '',
        'text'      => '',
      ),
    ),
  ));
}

$tabs = '';
$tab_panels = '';

$i = 0;
foreach ($data as $section) {
  $section_title = $section['title'];
  $section_text  = $section['text'];
  $subsections   = $section['subsections'];

  $key           = sanitize_key($section_title);
  $active        = $i == 0 ? 'active' : '';
  $selected      = $i == 0 ? 'true' : '';

  $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab' data-tab='$key'><span>$section_title</span></a>";
  $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";

  $tab_panels .= $section_text ? '<div class="section-intro narrow">' . $section_text . '</div>' : null;

  if (isset($subsections)) {

    $sub_tabs = '';
    $sub_tab_panels = '';

    $j = 0;
    foreach ($subsections as $ss) {
      $ss_title    = $ss['title'];
      $ss_text     = $ss['text'];
      $post_type   = $ss['post_type'];
      $category    = $ss['category'];

      $ss_key      = sanitize_key($ss_title);

      $ss_active   = $j == 0 ? 'active' : '';
      $ss_selected = $j == 0 ? 'true' : '';

      $content = '';
      $content .= $ss_text ? '<div class="section-intro narrow">' . $ss_text . '</div>' : null;
      $content .= "<div data-js-id='$ss_key' data-js-category='$category' data-js-post_type='$post_type'>";
      $content .= lb_load_template_part('template-parts/research-posts-list', '', array('post_type' => $post_type, 'category' => $category, 'search' => $search));
      $content .= "</div>"; // data-js-id

      if (count($subsections) > 1) {
        $sub_tabs .= "<a href='#$ss_key' id='tab-$ss_key' class='tab $ss_active' aria-selected='$ss_selected' aria-controls='$ss_key' role='tab' data-parent-tab='$key'><span>$ss_title</span></a>";
        $sub_tab_panels .= "<div id='$ss_key' class='tab-panel $ss_active' aria-expanded='$ss_selected' aria-labelledby='tab-$ss_key' role='tabpanel'>";
        $sub_tab_panels .= $content;
        $sub_tab_panels .= "</div>"; // tab-panel
      } else {
        $tab_panels .= $content;
      }

      $j++;
    }

    if (count($subsections) > 1) {
      $tab_panels .= '<div class="lb-tabs" data-use-hash>';
      $tab_panels .= '<div class="tabs tabs--small" role="tablist">';
      $tab_panels .= $sub_tabs;
      $tab_panels .= '</div>'; // tabs
      $tab_panels .= '<div class="tab-panels">';
      $tab_panels .= $sub_tab_panels;
      $tab_panels .= '</div>'; // tab-panels
      $tab_panels .= '</div>'; // lb-tabs
    }
  }

  $tab_panels .= "</div>";

  $i++;
}
?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">

    <div id="research-posts" class="lb-tabs" data-use-hash>

      <section class="container row page-m-t research">
        <div class="research__bg xs-hide sm-show">
          <?php echo wp_get_attachment_image(84, 'large'); ?>
        </div>
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <h1>Research</h1>
          <div class="boxed-nav">
            <div class="boxed-nav__tabs">
              <div class="tabs-wrapper">
                <div class="tabs tabs--alt" role="tablist">
                  <?php echo $tabs; ?>
                </div>
              </div>
            </div>
            <div class="boxed-nav__search">
              <?php echo lb_get_search_form('research-searchform', '', home_url('/research'), 'Search by Keyword', 'search', $search); ?>
            </div>
          </div>
        </div>
      </section>

      <section class="container row section-m-b">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <div class="tab-panels tab-panels--main">
            <?php echo $tab_panels; ?>
          </div>
        </div>
      </section>

    </div>

  </main><!-- #main -->
</div><!-- #primary -->

<script>
  window._researchPosts = {
    upcomingConferences: {
      category: ''
    },
    pastConferences: {
      category: ''
    },
    panelRoundtable: {
      category: '',
    },
    lectures2019: {
      category: '2019-lectures',
    },
    lectures2018: {
      category: '2018-lectures',
    }
  };
</script>

<?php
get_footer();
