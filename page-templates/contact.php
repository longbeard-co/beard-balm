<?php

/**
 * Template name: Contact
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$phone = '416-926-7133 (9 am – 5 pm EST)';
$email = 'sheptytsky@utoronto.ca';
$address_link = 'https://goo.gl/maps/CkhRh3CXgAgSDx2h8';
$address_text = 'University of St. Michael’s College in the University of Toronto<br />
      81 St. Mary Street<br />
      Toronto, ON M5S 1J4<br />
      Canada';

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b contact">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h1 class="m-b">Contact Us</h1>
      </div>
      <div class="col-xs-12 col-sm-11 col-md-7 col-lg-6 col-lg-offset-1">
        <div class="contact__form">
          <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
        </div>
      </div>
      <div class="col-xs-12 col-md-5 col-lg-5">
        <div class="contact__text">
          <div class="contact-info-wrapper">
            <div class="contact__row">
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('phone'); ?>
                    <a href="<?php echo phone_to_url($phone); ?>" target="_blank" rel="noopener noreferrer"><?php echo $phone; ?></a>
                  </div>
                </div>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('email'); ?>
                    <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                  </div>
                </div>
              </div>
              <div class="contact__col">
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('location'); ?>
                    <div class="contact-info__text">
                      <p><a href="<?php echo $address_link; ?>" target="_blank" rel="noopener noreferrer"><?php echo $address_text; ?></a></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php

    $placeholder_m = lb_get_person_placeholder_id();
    $placeholder_f = lb_get_person_placeholder_id('female');

    $directory = array(
      array(
        'title' => 'Administration',
        'persons' => array(
          array(
            'name' => 'Fr. Alexander Laschuk, JCD, PhD',
            'role' => 'Executive Director',
            'email' => 'alexander.laschuk@utoronto.ca',
            'img' => 314,
          ),
          array(
            'name' => 'Tamara Wajda',
            'role' => 'Administrative Assistant',
            'email' => 'tamara.wajda@utoronto.ca',
            'img' => $placeholder_f,
          ),
        ),
      ),
      array(
        'title' => 'Faculty',
        'persons' => array(
          array(
            'name' => 'Rev. Dr. Andrew Summerson',
            'role' => 'Professor of Greek Patristics',
            'email' => 'andrew.summerson@utoronto.ca',
            'img' => 1200,
          ),
          array(
            'name' => 'Rev. Dr. Andriy Chirovsky',
            'role' => 'Professor',
            'email' => 'andriy.chirovsky@utoronto.ca',
            'img' => 311,
          ),
          array(
            'name' => 'Very Rev. Dr. Peter Galadza',
            'role' => 'Professor Emeritus',
            'email' => 'peter.galadza@utoronto.ca',
            'img' => 312,
          ),
        ),
      ),
      array(
        'title' => 'Online Purchases',
        'description' => 'For online purchases questions, please contact:',
        'persons' => array(
          array(
            'name' => 'Tamara Wajda',
            'phone' => '416-926-7133  ',
            'email' => 'tamara.wajda@utoronto.ca ',
            'img' => $placeholder_f,
          ),
        ),
      ),
    );
    ?>

    <?php
    $tabs = '';
    $tab_panels = '';
    $cards = ''; // Will be shared between tabs and non-tabs layouts

    $i = 0;
    foreach ($directory as $group) {
      $card = '';

      $title = $group['title'];
      $desc = isset($group['description']) ? $group['description'] : '';
      $persons = $group['persons'];

      $key = sanitize_title($title);
      $active = $i == 0 ? 'active' : '';
      $selected = $i == 0 ? 'true' : '';
      $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$title</span></a>";
      $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";

      $card .= "<div class='directory__group box-shadow'>";
      $card .= "<h2 class='h3 directory__group__title'>$title</h2>";
      $card .= $desc ? "<p>$desc</p>" : null;
      $card .= "<div class='directory__group__persons'>";
      $card .= "<ul>";
      foreach ($persons as $person) {
        $card .= "<li>";
        $card .= lb_load_template_part("template-parts/contact-person", '', array('person' => $person));
        $card .= "</li>";
      }
      $card .= "</ul>";
      $card .= "</div>"; // directory__group__persons
      $card .= "</div>"; // directory__group

      $tab_panels .= $card;
      $tab_panels .= "</div>";

      $cards .= "<li>$card</li>";
      $i++;
    }
    ?>

    <section class="container row section-m-t section-m-b directory">
      <div class="col-xs-12 col-sm-8 col-md-12 col-lg-11 col-lg-offset-1 directory__col">
        <div class="directory__list--tabs md-hide">
          <div class="lb-tabs">
            <div class="tabs-wrapper box-shadow">
              <div class="tabs" role="tablist"><?php echo $tabs; ?></div>
            </div>
            <div class="tab-panels">
              <?php echo $tab_panels; ?>
            </div>
          </div>
        </div>
        <div class="directory__list xs-hide md-show">
          <ul>
            <?php echo $cards; ?>
          </ul>
        </div>
      </div>
    </section>

    <?php get_template_part('template-parts/subscribe', '', array('class' => 'section-m-t section-m-b', 'type' => 'contact')); ?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
