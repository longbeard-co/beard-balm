<?php

/**
 * Template name: Students
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();


?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b student-life">
      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 col-lg-offset-1">
        <h1>Student Life</h1>
        <p>Students affiliated with the Sheptytsky Institute participate in events not only at the Institute but also with the wider student life activities with the Faculty of Theology at the University of St. Michael’s College in the University of Toronto.</p>
        <p>Students are welcome to use our student lounge in Windle House and attend any of our ongoing events.</p>
      </div>
      <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-5 col-md-offset-1">
        <div class="student-life__images">
          <div class="student-life__img student-life__img--1">
            <figure>
              <?php echo wp_get_attachment_image(134, 'large'); ?>
              <figcaption>Windle House</figcaption>
            </figure>
          </div>
          <div class="student-life__img student-life__img--2">
            <?php echo wp_get_attachment_image(135, 'large'); ?>
          </div>
        </div>
      </div>
    </section>
    <section class="container row section-m-t section-m-b middle-sm chapel">
      <div class="xs-hide sm-show col-sm-3 col-md-2">
        <div class="chapel__img">
          <?php echo wp_get_attachment_image(137, 'large'); ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6">
        <div class="chapel__text">
          <div class="chapel__text__img xs-hide sm-show">
            <?php echo wp_get_attachment_image(136, 'large'); ?>
          </div>
          <div class="chapel__text__desc text-block">
            <h2 class="h1">Chapel</h2>
            <p>The Chapel of St. Sophia and her three daughters is a central part of the life of the Sheptytsky Institute and is located in the lower level of Elmsley Hall on the north end of the corridor.</p>
            <p><em class="blockquote">All are welcome at our daily services which are celebrated according to the Byzantine liturgical tradition of the Ukrainian Catholic Church. </em></p>
            <p>We also have a smaller chapel located in the upper floor of Windle House which is assembled in the Coptic tradition.</p>
            <p>The Sheptytsky Institute Chapel:</p>
            <div class="chapel__text__address">
              <a href="https://www.google.com/maps/dir//43.6666468,-79.3904212/@43.666647,-79.390421,16z?hl=en-US" target="_blank" rel="noopener noreferrer" class="chapel__text__address__title">
                <?php the_svg('location'); ?>
                <h3 class="h6">Elmsley Hall</h3>
              </a>
              <div class="chapel__text__address__desc">
                <p>81 St. Mary Street<br />
                  First floor; north end of corridor</p>
              </div>
            </div>
            <p>See also <a href="https://stmikes.utoronto.ca/news/worship-at-the-heart-of-theology/" target="_blank" rel="noopener noreferrer">Worship at the Heart of Theology</a>, University of St. Michael’s College.</p>
          </div>
        </div>

      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
