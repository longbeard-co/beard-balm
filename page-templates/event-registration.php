<?php

/**
 * Template name: Event Registration
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$event = isset($_GET['event']) ? $_GET['event'] : '';

// * Important: this page shouldn't be cached as it needs to be able to render GF on per-request basis

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1">
        <div class="intro__text text-block">
          <h1>Registration Request</h1>
          <p>If you would like to register for an event, please reference our Calendar page for a listing of all event opportunities. Following your decision on which event to register for, please fill out the following form and a member of our team will be in touch shortly.</p>
          <h2 class="h3">Have a question?</h2>
          <p>For more information or any queries about an event, please fill out our contact form <a href="/contact">here</a> and make sure to insert the name of the event into the Subject field.</p>
        </div>
      </div>
      <div class="col-xs-12 col-sm-6 col-lg-5">
        <div class="intro__form">
          <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true" field_values="event=' . $event . '"]'); ?>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
