<?php

/**
 * Template name: LOGOS
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$submissions = array(
  array(
    'title' => 'Type and Length of Submission',
    'body' => '<p>In keeping with its dual focus as a journal that is concerned both with scholarly debate and research and the renewal of the Church’s pastoral life, Logos: A Journal of Eastern Christian Studies will accept for possible publication five different types of contributions in three different languages (English, French, Ukrainian):</p>
      <ul>
        <li>
          <h6>Articles</h6>
          <p>These are major, original, and significant contributions by scholars to the fields of knowledge with which the journal is concerned. Articles, which are first reviewed by the editorial staff to determine whether they are worthy of publication, are always sent to a “blind” jury of experts in the field(s) with which an article is concerned. In general, a decision to accept or reject an article takes a minimum of three months, and sometimes six months or, rarely, longer. Articles are normally no fewer than fifteen pages (in 12-point font and double-spaced in Microsoft Word), and no longer than thirty.</p>
        </li>
        <li>
          <h6>Notes, Lectures, Essays</h6>
          <p>These are usually less specialized or more general pieces that are relatively short (generally eight to twelve double-spaced pages) and may have originated as oral presentations rather than written texts per se. Submissions in this category may not necessarily be as original as an article, but are, in the judgment of the editors, nonetheless worthy of publication, especially if doing so will stimulate debate on a neglected person or topic or contribute to the renewal of the Church.</p>
        </li>
        <li>
          <h6>Review Essays</h6>
          <p>A review essay generally critically discusses three or more new books on the same topic at greater length and in greater detail than a standard book review. Alternately, a review essay may review new books vis-à-vis a classic in the field. Footnotes are permitted in a review essay (see below), which should generally be no longer than eighteen pages—though the editors will consider longer submissions if more than three books are being discussed. Review essays must be discussed with the editors in advance in case the books are already under review.</p>
        </li>
        <li>
          <h6>Book Reviews</h6>
          <p>These are standard reviews of new publications, sent by the editors to experts in the field for adjudication and review. A review should be between 800 and 1200 words, but never longer. Reviews do not usually include footnotes. If an author wishes to submit an unsolicited book review, he must confer with the editors via e-mail in advance.</p>
        </li>
        <li>
          <h6>Brief Reviews</h6>
          <p>These are short reviews (400 words) of books for which, for a variety of reasons, it may not be possible to run longer reviews but which are still deserving of notice. If an author wishes to submit an unsolicited brief review, he must confer with the editors via e-mail in advance.</p>
        </li>
      </ul>',
  ),
  array(
    'title' => 'Stylistic Guidelines',
    'body' => '<p>Authors who submit articles to Logos must comply with the following minimum requirements:</p>
      <ul>
        <li>Logos strongly prefers to receive articles by e-mail in the form of Microsoft Word attachments. In your covering e-mail, please include a brief autobiographical sketch (in which you mention institutional affiliations, research interests, and recent publications), and your 200-word abstract of the article.</li>
        <li>Authors are required to use the seventeenth edition (2017) of The Chicago Manual of Style (<a href="https://www.chicagomanualofstyle.org/home.html" target="_blank" rel="noopener noreferrer">available online</a>), a few of whose more pertinent guidelines are summarized below. A failure to observe these guidelines on the part of an author will result in the submission being returned to the author for reformatting, thereby delaying the possible publication of the article.</li>
        <li>LOGOS only uses footnotes, not endnotes. Please adjust your text accordingly.</li>
      </ul>
    ',
  ),
  array(
    'title' => 'Punctuation',
    'body' => '<p>Please note that there are special rules governing the use of punctuation inside and outside quotation marks. As a rule, the approach known as “American style” (n.b. Logos also used “American” spelling) is followed thus:</p>
      <ul>
        <li>Periods: These are always placed inside the quotation marks, even if they do not occur in the original quotation. This allows for cleaner copy and almost never results in confusion. (If in those highly rare occasions where confusion might ensue, the Manual allows for periods outside the quotation marks.)</li>
        <li>Commas: The same as above, i.e., always inside the quotation marks.</li>
        <li>Semi-colons: These should be placed outside the quotation marks where possible.</li>
        <li>Colons: As above, for semi-colons.</li>
        <li>Exclamation Point: These are inside quotation marks only when they are found in the original.</li>
        <li>Question Marks: As above for exclamation points.</li>
      </ul>
    ',
  ),
  array(
    'title' => 'Footnotes',
    'body' => '<p>There are generally five commonly used sources cited in footnotes: articles in journals, monographs/books with a single author, translated works, chapters in edited collections, and websites. In most cases, a typical citation for a direct quotation from a given page would look like this:</p>
      <ol>
        <li>Journal Article: Hans Urs von Balthasar, “The Fathers, the Scholastics, and Ourselves,” Communio 24, no. 2 (Summer 1997): 347.</li>
        <li>Authored Monograph: Joseph Raya, Transfiguration of Our Lord and Saviour Jesus Christ (Combermere, ON: Madonna House, 1992), 115.</li>
        <li>Translated Works: Hans Urs von Balthasar, Presence and Thought: An Essay on the Religious Philosophy of Gregory of Nyssa, trans. Mark Sebanc (San Francisco: Ignatius Press, 1995 ), 13.</li>
        <li>Chapters in Edited Collections: Alexander Schmemann, “Liturgical Theology, Theology of Liturgy, and Liturgical Reform,” in Liturgy and Tradition: Theological Reflections of Alexander Schmemann, ed. Thomas Fisch (Crestwood, NY: St. Vladimir’s Seminary Press, 1990), 42.</li>
        <li>Websites: Alexis Liberovsky, “His Eminence, Metropolitan Leonty (Turkevich),”The Holy Synod: Primates of the OCA, Orthodox Church in America, July 2006, https://www.oca.org/holy-synod/past-primates/leonty-turkevich.</li>
      </ol>
      <p>Authors should further note that, for repeated citations from the same text, Logos continues to accept the older practice of using the Latin short form Ibid. (although not op. cit., etc.), which is not italicized, but is followed by a period, then a comma, then the page number. Instead of this, the CMS also allows for—and actually encourages—the use of what is called a “short title” or “short cite” in footnotes subsequent to a full citation earlier on. Thus, for each of the examples above, a short cite would look like this:</p>
      <ol>
        <li>von Balthasar, “The Fathers, the Scholastics, and Ourselves,” 363.</li>
        <li>Raya, Transfiguration of Our Lord, 42.</li>
        <li>von Balthasar, Presence and Thought, 121.</li>
        <li>Schmemann, “Liturgical Theology,” 44.</li>
        <li>Liberovsky, “Metropolitan Leonty,” Orthodox Church in America.</li>
      </ol>',
  ),
  array(
    'title' => 'Exchanges',
    'body' => '<p>Permit me to take but a moment of your time to introduce or re-introduce a journal that may be of interest to you in your ongoing research and publication thereof.</p>
      <p>For over half a century, LOGOS: A Journal of Eastern Christian Studies has been published in North America as a tri-lingual academic revue. Now the flagship journal of the Metropolitan Andrey Sheptytsky Institute of Eastern Christian Studies, LOGOS is devoted to all aspects of Eastern Christian studies, emphasizing both Orthodox and Catholic Eastern Churches, with a special, but not exclusive, interest in the Churches of Ukraine. The journal publishes articles of original scholarship and book reviews together with occasional historical documents. All the contents pertain to the theological (especially patristic), spiritual, liturgical, and canonical life of the Christian East in its Catholic, Orthodox, Oriental Orthodox and related manifestations. The journal is also very interested in ecumenical relations not only among Eastern Christians but between and among Christians of Western traditions also.</p>
      <p>LOGOS is a refereed journal, and abstracted in Religion Index One. The LOGOS editorial board includes internationally renowned specialists in Eastern Christian studies.</p>
      <p>In 2005, the journal underwent a period of extensive revision and has emerged in a much stronger form, fully committed to regular publication twice yearly on a fixed schedule. It continues to expand its subscriber base across North America and indeed around the world, and is found in major ecclesiastical centers such as Rome, Kyiv and Constantinople as well as distinguished universities and places of academic research around the world. Ongoing promotion this year intends to aggressively expand the subscriber base in both institutional and individual forms.</p>
      <p><strong>In your own research and writing, please bear LOGOS in mind as a place for possible publication, both of full articles as well as shorter essays, lectures, and comments. For details on deadlines, style, and submissions, please see our LOGOS Style Guide.</strong></p>
      <p>In addition, if your library does not carry our journal, please refer them to our website for details on how they may either subscribe or else arrange with us for an “academic exchange” where possible. If you yourself wish to subscribe, please return the enclosed form to us.</p>
      <p>Finally, you will note that one of the changes recently implemented in the journal was an expansion of the book review section. This expansion has allowed us to more than double the number of reviews we run as well as add two new features: a “Briefly Noted” section and a “Books Received” section.</p>
      <p>The “Briefly Noted” section will contain short reviews (c. 400 words) of books for which, for a variety of reasons, it may not be possible to run longer reviews but which are still deserving of notice. The “Books Received” section will be a compiled list of all the books received by LOGOS in the period between issues of the journal, books for which we will not run regular reviews.</p>
      <p>We wish to draw your attention to the our policy whereby if books are sent, either by authors themselves or their publishers, to LOGOS, and if those books fall within the focus of the journal, we will, at a minimum, advertise them for free in our “Books Received” list but, wherever possible, will find a specialist in the field to write a review of the book, which review we will then publish. Our newly expanded book review section, then, is designed to take fuller account of the burgeoning number of publications in the fields which are of concern to our readers, and to promote greater interest in a larger number of those books. This section should therefore be of significant interest to publishers such as yourself, and of equal interest, we trust hopefully, to our many readers.</p>
      <p>Thank you for your time and consideration, and we look forward to hearing from you in due course.</p>
      <p>Yours faithfully,</p>
      <p>The Rt. Rev. Dr. Andriy Chirovsky,<br />
      Editor-in-chief</p>',
  )
);

$articles = array(
  array(
    'title'  => 'The Liturgy in the Life of the Church',
    'author' => 'Robert F. Taft',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-40-nos-1-4-1999/'
  ),
  array(
    'title'  => 'John of Damascus and Theodore Abū Qurrah: Icons, Christ, and Sacred Texts',
    'author' => 'Jaroslav Z. Skira',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-55-3-4-2014/'
  ),
  array(
    'title'  => 'Canonical Reflections on the Recent Pastoral Letter from the Melkite Greco-Catholic Eparchy of Newton',
    'author' => 'Alexander M. Laschuk',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-53-nos-1-2-2012/'
  ),
  array(
    'title'  => 'How to Acquire an Orthodox Phronêma in the West: From Ecclesiastical Enculturation to Theological Competence',
    'author' => 'Augustine Casiday',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-58-nos-1-4-2017/'
  ),
  array(
    'title'  => 'A Report on the Ottawa Colloquium on the Future of the Ukrainian Greco-Catholic Church in North America',
    'author' => 'Andriy Chirovsky',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-56-nos-1-2-2015/'
  ),
  array(
    'title'  => 'Jaroslav Coranič, Z Dejin Greckokatolickej cirkvi na Slovensku (On the History of the Greek Catholic Church in Slovakia)',
    'author' => 'Paul Robert Magocsi',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-57-nos-1-4-2016/'
  ),
  array(
    'title'  => '“Віки вічні” чи “віки віків”?',
    'author' => 'Петро Ґаладза',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-43-45-2002-2004-2/'
  ),
  array(
    'title'  => '“Andrey Septyckyj, Michel d’Herbigny et la Russie,”',
    'author' => 'Léon Tretjakewitsch',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-34-nos-3-4-1993-2/'
  ),
  array(
    'title'  => '“„Правила” Української Католицької Церкви в Канаді 1915 року Єпископа-ісповідника Никити Будки,”',
    'author' => 'Богдан Казимира',
    'link'   => 'https://sheptytskyinstitute.ca/logos-a-journal-of-eastern-christian-studies-vol-34-nos-3-4-1993/'
  ),
  array(
    'title'  => 'Eschatology and Funerary Practices Today: Byzance après Byzance?',
    'author' => 'Adam A.J. DeVille',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-58-nos-1-4-2017/'
  ),
  array(
    'title'  => '“Breathing with Two Lungs” The Importance of Eastern Christian Studies',
    'author' => 'Stephen Wojcichowsky',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-54-3-4-2013/'
  ),
  array(
    'title'  => 'Fractured Orthodoxy in Ukraine and Politics: The Impact of Patriarch Kyrill’s “Russian World”',
    'author' => 'Nicholas E. Denysenko',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-54-1-2-2013/'
  ),
  array(
    'title'  => 'The Value of Scripture Study',
    'author' => 'L.G. Bloomquist',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-54-3-4-2013/'
  ),
  array(
    'title'  => 'Church, State and Holy War: Assessing the Role of Religious Organizations in the War in Ukraine',
    'author' => 'Emily Bayrachny',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-58-nos-1-4-2017/'
  ),
  array(
    'title'  => 'Літургійні тексти і переклади Основна доповідь другого робочого засідання Синоду єпископів УГКЦ, 5-го вересня 2017',
    'author' => 'Петро Ґаладза',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-58-nos-1-4-2017/'
  ),
  array(
    'title'  => 'India’s “Syriac” Churches – 1996-1997',
    'author' => 'Johannes Madey',
    'link'   => 'https://shep.longbeardco.com/wp-content/uploads/2021/12/Indias-Syriac-Churches-–-1996-1997.pdf'
  ),
  array(
    'title'  => 'Reflections on Recent Orthodox Statements Concerning Eastern Catholics',
    'author' => 'Editorial',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-37-nos-1-4-1996-2/'
  ),
  array(
    'title'  => '“Sister Churches”: Ecumenical Terminology in Search of Content',
    'author' => 'Andriy Chirovsky',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-34-nos-3-4-1993-2/'
  ),
  array(
    'title'  => 'The Dramatic Blindness of Western Orthodox Theologians Towards Russia',
    'author' => 'Antoine Arjakovsky',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-56-3-4-2015/'
  ),
  array(
    'title'  => 'Gifts from the Orient: Eastern Textual Influence in the Development of Anglican Liturgy',
    'author' => 'John Gibaut',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-41-42-2000-2001/'
  ),
  array(
    'title'  => 'La Papauté du point de vue russe orthodoxe',
    'author' => 'Andrius Valevičius',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-48-nos-3-4-2007-2/'
  ),
  array(
    'title'  => 'Patriarches d’Orient et d’Occident: similarités et différences. Comment Rome pourrait fonctionner comme l’un d’entre eux?',
    'author' => 'Grigorios III',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-46-nos-1-2-2005-2/'
  ),
  array(
    'title'  => 'La théologie des énergies divines: l’enjeu, les dificultés et les perspectives du dialogue entre catholiques et orthodoxes',
    'author' => 'Jean-Claude Larchet',
    'link'   => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-50-nos-3-4-2009-2/'
  ),
);

$conference = array(
  array(
    'title' => 'Eastern Christian Writings and Early Encounters with Islam',
    'link' => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-58-nos-1-4-2017/',
    'meta' => array(
      'Waterloo, Canada, April 2017',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 58, Nos. 1-4',
    ),
  ),
  array(
    'title' => 'The Life and Thought of Louis Massignon (1883-1962): Comparative political and theological perspectives',
    'link' => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-55-3-4-2014/',
  ),
  array(
    'title' => 'Symbolic Mediation of Wholeness in Western Orthodoxy',
    'link' => 'https://shep.longbeardco.com/product/symbolic-mediation-of-wholeness-in-western-orthodoxy/',
    'meta' => array(
      'Prague, Czech Republic, May 2014',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 56, Nos. 1-2 Part I',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 57, Nos. 1-4 Part II',
    ),
  ),
  array(
    'title' => 'Radical Orthodoxy: A Christian Answer to Postmodern Culture',
    'link' => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-47-nos-1-2-2006-2/',
    'meta' => array(
      'Lviv, Ukraine, April 2005',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 47, Nos. 1-2',
    ),
  ),
  array(
    'title' => 'The Spiritual and Intellectual Legacy of Metropolitan Andrey Sheptytsky',
    'link' => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-43-45-2002-2004-2/',
    'meta' => array(
      'Montréal, Canada, November 2001',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 43-45 (2002-2004)',
    ),
  ),
  array(
    'title' => 'Encounter of the Eastern Catholic Churches of the Americas and Oceania',
    'link' => 'https://shep.longbeardco.com/product/logos-a-journal-of-eastern-christian-studies-vol-40-nos-1-4-1999/',
    'meta' => array(
      'Boston, USA, November 1999',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 40, Nos. 1-4',
    ),
  ),
  array(
    'title' => 'International Symposium on English Translations of Byzantine Liturgical Texts',
    'link' => 'https://shep.longbeardco.com/product/international-symposium-on-english-translations-of-byzantine-liturgical-texts/',
    'meta' => array(
      'Stamford, USA, June 1998',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 39, Nos. 2-4 Part I',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 41-42 Part II',
    ),
  ),
  array(
    'title' => 'Kyivan Church Study Group Consultations',
    'link' => 'https://shep.longbeardco.com/product/kyivan-church-study-group-collection/',
    'meta' => array(
      'Oxford, Stamford, Ottawa, and Rome, 1992 thru 1995',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 34, Nos. 1-2 Oxford Consultation',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 34, Nos. 3-4 Stamford Consultation',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 35, Nos. 1-4 Ottawa & Stamford II Consultation',
      'LOGOS: A Journal of Eastern Christian Studies Vol. 36, Nos. 1-4 Rome Consultation',
    ),
  ),
);

?>

<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t intro">
      <div class="col-xs-12 col-lg-10 col-lg-offset-1">
        <h1 class="intro__title">
          <span>LOGOS</span>
          <span class="intro__title__separator"></span>
          <span class="h4 intro__title__subtitle">A journal of Eastern Christian Studies</span>
        </h1>
      </div>
      <div class="intro__img xs-hide md-show">
        <?php echo wp_get_attachment_image(237, 'large'); ?>
      </div>
    </section>

    <div class="lb-tabs section-m-b">

      <section class="container row section-m-b--sm">
        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
          <div class="tabs-wrapper">
            <div class="tabs tabs--alt" role="tablist">
              <a href="#about" id="tab-about" class="tab active" aria-selected="true" aria-controls="about" role="tab" data-tab="about">About</a>
              <a href="#catalogue" id="tab-catalogue" class="tab" aria-selected="false" aria-controls="catalogue" role="tab" data-tab="catalogue">Catalogue</a>
              <a href="#subscription" id="tab-subscription" class="tab" aria-selected="false" aria-controls="subscription" role="tab" data-tab="subscription">Subscription</a>
              <a href="#articles" id="tab-articles" class="tab" aria-selected="false" aria-controls="articles" role="tab" data-tab="articles">Open Access Articles</a>
              <a href="#conferences" id="tab-conferences" class="tab" aria-selected="false" aria-controls="conferences" role="tab" data-tab="conferences">Int'l. Conference Proceedings</a>
              <a href="#submissions" id="tab-submissions" class="tab" aria-selected="false" aria-controls="submissions" role="tab" data-tab="submissions">Submissions</a>
              <a href="#collection" id="tab-collection" class="tab" aria-selected="false" aria-controls="collection" role="tab" data-tab="collection">The Collection</a>
            </div>
          </div>
        </div>
      </section>

      <div class="tab-panels">

        <div id="about" class="tab-panel active" aria-expanded="true" aria-labelledby="tab-about" role="tabpanel">
          <?php

          $personnel = array(
            array(
              'name'  => 'Adam DeVille',
              'role'  => 'University of Saint Francis',
              'role2' => 'Editor-in-Chief',
              'img'   => 209,
            ),
            array(
              'name'  => 'Fr. Alexander Laschuk, JCD, PhD',
              'role'  => 'Sheptytsky Institute',
              'role2' => 'Managing Editor',
              'img'   => 314,
            ),
            array(
              'name'  => 'Cyril Kennedy',
              'role'  => 'Catholic University of America',
              'role2' => 'Editor',
              'img'   => 185,
            ),
            array(
              'name'  => 'Tamara Wajda',
              'role2' => 'Distribution',
              'img'   => lb_get_person_placeholder_id('female'),
            ),
          );
          ?>
          <section class="container row about section-m-b--sm">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <div class="text-col" data-rm data-rm-xs="2">
                <p>The Sheptytsky Institute publishes a peer-reviewed journal, LOGOS: A Journal of Eastern Christian Studies, which is Canada’s only double-blind peer-reviewed journal in the field.</p>
                <p>This tri-lingual (English, French, Ukrainian) theological review focuses on Eastern Christian Studies, emphasizing both Orthodox and Catholic Eastern Churches with a special, but not exclusive, interest in the Church of Kyiv.</p>
                <p>LOGOS is edited by an editorial committee. Its editorial board includes members of the editorial committee and outside members chosen from various constituencies, together with internationally renowned specialists in Eastern Christian Studies.</p>
                <p>The journal was founded in 1950 by the late Metropolitan Maxim Hermaniuk and was published in Yorkton, Saskatchewan until 1983. The new series of LOGOS was re-established in 1993 by Fr. Andriy Chirovsky, beginning with Vol. 34. It was published in Ottawa to 2016, and from 2017 in Toronto.</p>
                <p><strong>Design and Layout of LOGOS done by Key-Co. Enterprises.</strong></p>
              </div>
            </div>
          </section>
          <section class="container row personnel">
            <div class="col-xs-12 col-md-9 col-lg-7 col-lg-offset-1">
              <h2>Personnel</h2>
              <div class="personnel__inner">
                <ul>
                  <?php foreach ($personnel as $person) { ?>
                    <li>
                      <?php get_template_part("template-parts/contact-person", '', array('person' => $person)); ?>
                    </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
          </section>
        </div>

        <div id="catalogue" class="tab-panel" aria-expanded="false" aria-labelledby="tab-catalogue" role="tabpanel">
          <section class="container row section-m-b--sm catalogue">
            <div class="col-xs-12 col-md-11 col-lg-9 col-lg-offset-1">
              <h2>Catalogue</h2>
              <p class="narrow">The Sheptytsky Institute publishes a peer-reviewed journal, LOGOS: A Journal of Eastern Christian Studies, which is Canada’s only double-blind peer-reviewed journal in the field.</p>
              <h3 class="h4">Most Recent</h3>
              <?php get_template_part('template-parts/featured-products', '', array('category' => 'journals')); ?>
            </div>
          </section>
        </div>

        <div id="subscription" class="tab-panel" aria-expanded="false" aria-labelledby="tab-subscription" role="tabpanel">
          <section class="container row section-m-b--sm subscription">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <h2 class="m-b">Subscription</h2>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 col-lg-offset-1">
              <div class="subscription__info">
                <ul>
                  <li><strong>Languages</strong>: English, French, Ukrainian</li>
                  <li>Published annually</li>
                  <li><strong>ISSN</strong>: 0024-5895</li>
                  <li class="price"><strong>Price</strong>: $45 (1-year subsription)</li>
                </ul>
                <p>Subscription begins with the next issue of LOGOS: A Journal of Eastern Christian Studies.</p>
                <p class="align-right"><a href="/product/logos-a-journal-of-eastern-christian-studies-subscription/" class="button"><span></span>Subscribe Now</a></p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-4 col-lg-offset-1">
              <div class="subscription__exchanges">
                <h4>Academic Exchanges</h4>
                <p>Other institutions or their libraries are invited to contact the Sheptytsky Institute to arrange for an academic exchange of journals.</p>
                <p>For more information please contact:</p>
                <?php
                $email = 'sheptytsky@utoronto.ca';
                ?>
                <div class="contact-info">
                  <div class="contact-info__inner">
                    <?php the_svg('email'); ?>
                    <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>

        <div id="articles" class="tab-panel" aria-expanded="false" aria-labelledby="tab-articles" role="tabpanel">
          <section class="container row section-m-b--sm articles">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <h2>Open Access Articles</h2>
              <div class="articles__intro">
                <div class="articles__intro__text">
                  <p>The Sheptytsky Institute offers a handful of open access articles in PDF format (below) from LOGOS: A Journal of Eastern Christian Studies.</p>
                </div>
                <script>
                  window._logosArticles = <?php echo json_encode($articles); ?>
                </script>
                <div class="articles__intro__search">
                  <?php echo lb_get_search_form('articles-searchform', '', '/logos', 'Search Articles'); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-md-12 col-lg-10 col-lg-offset-1">
              <div class="articles__list" data-js="articles-list" data-form-id="articles-searchform" data-articles="_logosArticles">
                <?php echo get_template_part('template-parts/logos-articles', '', array('articles' => $articles)); ?>
              </div>
            </div>
          </section>
        </div>

        <div id="conferences" class="tab-panel" aria-expanded="false" aria-labelledby="tab-conferences" role="tabpanel">
          <section class="container row section-m-b--sm articles">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <h2>International Conference Proceedings</h2>
              <div class="articles__intro">
                <div class="articles__intro__text">
                  <p>Proceedings and papers from several international theological conferences and meetings, including multi-part essays, have been published in LOGOS: A Journal of Eastern Christian Studies.</p>
                </div>
                <script>
                  window._conferenceProceedings = <?php echo json_encode($conference); ?>
                </script>
                <div class="articles__intro__search">
                  <?php echo lb_get_search_form('conference-searchform', '', '/logos', 'Search Conferences'); ?>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-10 col-md-12 col-lg-10 col-lg-offset-1">
              <div class="articles__list" data-js="articles-list" data-form-id="conference-searchform" data-articles="_conferenceProceedings">
                <?php echo get_template_part('template-parts/logos-articles', '', array('articles' => $conference)); ?>
              </div>
            </div>
          </section>
        </div>

        <div id="submissions" class="tab-panel" aria-expanded="false" aria-labelledby="tab-submissions" role="tabpanel">
          <section class="container row section-m-b--sm submissions">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <h2 class="m-b">Submissions</h2>
            </div>
            <div class="col-xs-12 col-md-8 col-lg-8 col-lg-offset-1">
              <div class="submissions__accordions">
                <?php get_template_part('template-parts/accordions', '', array('data' => $submissions)); ?>
              </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-3">
              <ul class="submissions__info">
                <li class="submissions__info__card card">
                  <h3 class="h4">Deadlines for submissions</h3>
                  <ul class="submissions__info__deadlines">
                    <li>
                      <h4 class="h5">Juried articles</h4>
                      <ul class="submissions__info__deadlines__issues">
                        <li>Spring issue<br />January 7</li>
                        <li>Fall issue<br />June 2</li>
                      </ul>
                    </li>
                    <li>
                      <h4 class="h5">Other materials</h4>
                      <ul class="submissions__info__deadlines__issues">
                        <li>Spring issue<br />February 15</li>
                        <li>Fall issue<br />July 30</li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="submissions__info__card card">
                  <h3 class="h3">Send submissions to:</h3>
                  <h4 class="h5">Logos Editor</h4>
                  <?php
                  $email = 'sheptytsky@utoronto.ca';
                  ?>
                  <div class="contact-info">
                    <div class="contact-info__inner">
                      <?php the_svg('email'); ?>
                      <a href="mailto:<?php echo $email; ?>" target="_blank" rel="noopener noreferrer"><?php echo $email; ?></a>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </section>
        </div>

        <div id="collection" class="tab-panel" aria-expanded="false" aria-labelledby="tab-collection" role="tabpanel">
          <section class="container row section-m-b--sm collection">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
              <h2 class="m-b">The LOGOS Collection</h2>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1">
              <div class="collection__text">
                <p>We are pleased to offer the full in print collection of LOGOS: A Journal of Eastern Christian Studies, Vols. 34 to 60 (1993-2019) in twenty-five volumes (thirty-seven issues)
                  <br />
                  for <span class="h4">$599</span> (reg. $899).
                </p>
                <p>LOGOS Volumes 1 to 33 (1950-1983) are out of print.</p>
                <p class="align-right"><a href="/product/logos-a-journal-of-eastern-christian-studies-the-collection/" class="button"><span></span>Purchase</a></p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
              <div class="collection__card card">
                <h3>Individual Volumes</h3>
                <p>If you would like to purchase indvidual volumes, visit the bookstore.</p>
                <p class="align-right"><a href="/product-category/journals/?_sfm__price=0+400" class="button"><span></span>Bookstore</a></p>
              </div>
            </div>
          </section>
        </div>

      </div>

    </div>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
