<?php

/**
 * Archive template - product_author
 * 
 * @package WordPress
 * @subpackage beardbalm
 * @since Beard Balm 1.0.0
 * @author Longbeard
 * @url https://www.longbeard.com/
 */

get_header();

$term = get_queried_object();

$args = array(
  'post_type'      => 'product',
  'post_status'    => 'publish',
  'posts_per_page' => 20,
  'tax_query'      => array(
    array(
      'taxonomy' => 'product_author',
      'field'    => 'term_id',
      'terms'    => $term->term_id,
    )
  ),
  'fields'       => 'ids'
);
$the_query = new WP_Query($args);
$the_posts = $the_query->get_posts();

$all_tags = array();

$img = wp_get_attachment_image(lb_get_person_placeholder_id(), 'large');
?>
<div id="primary" class="content-area">
  <main id="main" class="site-main">
    <section class="container row page-m-t section-m-b intro">
      <?php echo lb_get_woocommerce_top_header(); ?>
      <div class="col-xs-12 col-md-9 col-lg-8 col-lg-offset-1">
        <?php lb_woocommerce_nav_open(); ?>

        <div class="author">
          <div class="author__img xs-hide sm-show">
            <div class="author__img__inner"><?php echo $img; ?></div>
          </div>
          <div class="author__info">
            <div class="author__info__title">
              <div class="author__img sm-hide"><?php echo $img; ?></div>
              <h3><?php echo $term->name; ?></h3>
            </div>

            <?php
            if ($the_posts) {
              foreach ($the_posts as $post_id) {
                $tags = get_the_terms($post_id, 'product_tag');
                if ($tags && !is_wp_error($tags)) {
                  foreach ($tags as $tag) {
                    if (isset($all_tags[$tag->term_id])) {
                      $all_tags[$tag->term_id]['products'][] = $post_id;
                    } else {
                      $all_tags[$tag->term_id] = array(
                        'tag' => $tag,
                        'products' => array($post_id)
                      );
                    }
                  }
                }
              }

              if ($all_tags) {
                // Show products in tabbed format 

                $tabs = '';
                $tab_panels = '';
                $i = 0;
                foreach ($all_tags as $tag) {
                  $key   = $tag['tag']->term_id;
                  $text  = $tag['tag']->name;
                  $products = $tag['products'];
                  $active = $i == 0 ? 'active' : '';
                  $selected = $i == 0 ? 'true' : '';

                  $tabs .= "<a href='#$key' id='tab-$key' class='tab $active' aria-selected='$selected' aria-controls='$key' role='tab'><span>$text</span></a>";
                  $tab_panels .= "<div id='$key' class='tab-panel $active' aria-expanded='$selected' aria-labelledby='tab-$key' role='tabpanel'>";
                  foreach ($products as $post_id) {
                    setup_postdata($post_id);
                    $tab_panels .= lb_load_template_part('template-parts/post-entry', 'book', array(
                      'show_thumbnail'  => true,
                      'show_meta'       => true,
                      'author_id'       => $term->term_id,
                    ));
                  }
                  $tab_panels .= "</div>";

                  $i++;
                }
                wp_reset_postdata();
            ?>

                <div class="lb-tabs">
                  <div class="tabs tabs--small" role="tablist"><?php echo $tabs; ?></div>
                  <div class="tab-panels"><?php echo $tab_panels; ?></div>
                </div>

              <?php } else {
                // Show products in list format
                foreach ($the_posts as $post_id) {
                  setup_postdata($post_id);
                  get_template_part('template-parts/post-entry', 'book', array(
                    'show_thumbnail'  => true,
                    'show_meta'       => true,
                    'author_id'       => $term->term_id,
                  ));
                }
              ?>
              <?php } ?>
            <?php } ?>

          </div>
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
        <?php // get_sidebar('shop'); 
        ?>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
